__author__ = "Pawel Kowalik"
__copyright__ = "Copyright 2018, 1&1 IONOS SE"
__credits__ = []
__license__ = "MIT"
__maintainer__ = "Pawel Kowalik"
__email__ = "pawel-kow@users.noreply.github.com"
__status__ = "Beta"


# noinspection PyCompatibility
from http.server import BaseHTTPRequestHandler, HTTPServer
from six.moves import urllib


class GetHandler(BaseHTTPRequestHandler):
    callback = None

    # noinspection PyPep8Naming
    def do_GET(self):
        parsed_path = urllib.parse.urlparse(self.path)
        pased_params = urllib.parse.parse_qs(parsed_path.query)
        if GetHandler.callback is not None:
            GetHandler.callback(parsed_path, pased_params)

        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write('OK: {}'.format(pased_params).encode())

        return


class OneCallTestHttpServer:
    def __init__(self, path_to_handle, serverhost='localhost', serverport=8080, max_loops=5):
        self._serveraddr = (serverhost, serverport)
        self.parsed_path = None
        self.pased_params = None
        self.request_handled = False
        self.path_to_handle = path_to_handle
        self.max_loops = max_loops

    def _http_callback(self, parsed_path, pased_params):
        self.parsed_path = parsed_path
        self.pased_params = pased_params
        if self.path_to_handle == parsed_path.path:
            self.request_handled = True

    def start_local_http_and_handle_one_request(self):
        server = HTTPServer(self._serveraddr, GetHandler)
        print('Starting server on {}'.format(self._serveraddr))
        GetHandler.callback = self._http_callback
        while not self.request_handled and self.max_loops > 0:
            server.handle_request()
            self.max_loops -= 1
        server.server_close()
        print('Server stopped: {}, {}'.format(self.parsed_path, self.pased_params))
