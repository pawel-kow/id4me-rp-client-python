__author__ = "Pawel Kowalik"
__copyright__ = "Copyright 2018, 1&1 IONOS SE"
__credits__ = []
__license__ = "MIT"
__maintainer__ = "Pawel Kowalik"
__email__ = "pawel-kow@users.noreply.github.com"
__status__ = "Beta"


import datetime
from unittest.mock import MagicMock

from jwcrypto.jwk import JWKSet
from test_ID4meClientTestCommon import TestID4meClientTestCommon
from unittest2 import TestCase

from id4me_rp_client import *


class TestID4meClient_IDTokenVerification(TestCase):

    def setUp(self):
        '''
        Keys used in this example:

        -----BEGIN PRIVATE KEY-----
        MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDMPWpQ+OJNG786
        ZeAxfRdQXHvY3MDgua8Xxa/CxrLQ3IRU6K7kp85MXyktA7ftU2ygiCdsXDdWj0dV
        oTpIgQvBO41NiNTAauWLl31Erh/3SQTw05d9g0Agy/bpex+pS9G6r1I9K41xfkvc
        aDGjUiQzUhRTctwMfCIP+8zIO1rky5ZSVYDFFw9MCjnSVkJsHsdINOGfWsGN81t4
        DQpUTOu/m0uKup1KA06HiHuKA83xhT5kyZosp50qhS+OCEnGoHz0nOt+1HqipRpH
        vXN2zVErVDyXnD33r6AhBooxVfa+6PoV+XEafmVYkgcNUXrFVf31W0/WDNIykKYK
        +dRUcFEBAgMBAAECggEAM9gzMLK830330N8jdeyHCoMvfPEslN8Nrz8jSpIduh8A
        CH24R5rBJ2nQjfivq7LNlEJ7n2oZMgPU28u9e8ImNbrkN5ZQSzfXZQPjbJe2zZLf
        ijJr9hRnJuwflU0H0HXKm1T9z0FK+lBQ+XYJg6fKQKzJNBWn/hNUojIM3ZcTj5ga
        AaN6avad8VG7s4RS5kvC7dFUb0yVVs86q8GCDobGWfOaxf74iNcIdKyvjTKAKCw9
        5E3p614GuKbAV9NCmmt0YQkfRey+/hCLPcK711KNMyze326e3lbFI+mVkVgub8Fs
        0ztqo4uD2SnsGBTI0/xzHqYY1mMcmGaeNHuJLtoa8QKBgQD1oFP215+ijZ/86Gri
        zjTDIZgFjFr6jINUwlnXERk9ZI5D8HO7E9tr1h5FWRKHt7G1HwVGycGtS1AhGKbl
        7yAz+Wn6/ztz8OgwV6C4KJgRC+B6ID7MSEMQFHko1gIIcfqfNBuLRgJGYQRd4Xop
        ieVjUQqJbuL4n8Vd4p2juOVXlwKBgQDU3Z/aVxNyxuoRf9zfDadlkLoAUcSNpbbp
        rBhN6h8b2QUzhuwdfYCv2sLYSyHxLgB/+DZ5CYYRAgYTd1sygwS0YlbR+5DTUMx3
        eZeBebB42TAkhBVkMLjlkRiXSzxMO1Q3eqNktQ8+Gj391nQEliZSszFURppDBYqc
        YFjHYFbvJwKBgG7HztbHzVHNOlkgogSFKDrRzfTCps3Vze3OciKTraSI07lEm43G
        mPH78k8oqzjW2qlrwj95A4cbLpqeubd+sDGAzhHMR441GyOigfRNKriVeLrcDDaq
        aMS51yNZ0jTo4zYmlD2uZ5xCaWvGJAjUDsdKZ8wwXOHxfVyotpx+6cElAoGAN8+P
        3PBdsgdDUalDoNv0muWSTwkhTr43MhckHDGZTHTfLz5d4/GG7xSdZ5ZzZMfZ67OJ
        nLyJgSYVr77ok1rUvfLWrqGVvEIACgdAWPst8NGJFNnoveckY1RirTBVs+sOTqmB
        86KhR3gb7tTugFFZ5O8ErQMB6TanvA5q2IOC55sCgYAKDPOkRf+r+R79L7di2WUm
        k7uJV+aLZjEnrx5pLw+2cYaTSUZ7J7RfsH5DE4oWbxufyLioEB7zmAT1bL9TukvK
        QdwR4kxlFBYYa7W//J20wXDmFc47b/lcqMPzDyORFWwH+Jalx9B6E/uI4yP0ymjO
        iNDTM6//eYq1gkKWcbllEg==
        -----END PRIVATE KEY-----

        Public keys in PEM format used in this example

        -----BEGIN PUBLIC KEY-----
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzD1qUPjiTRu/OmXgMX0X
        UFx72NzA4LmvF8Wvwsay0NyEVOiu5KfOTF8pLQO37VNsoIgnbFw3Vo9HVaE6SIEL
        wTuNTYjUwGrli5d9RK4f90kE8NOXfYNAIMv26XsfqUvRuq9SPSuNcX5L3Ggxo1Ik
        M1IUU3LcDHwiD/vMyDta5MuWUlWAxRcPTAo50lZCbB7HSDThn1rBjfNbeA0KVEzr
        v5tLirqdSgNOh4h7igPN8YU+ZMmaLKedKoUvjghJxqB89JzrftR6oqUaR71zds1R
        K1Q8l5w996+gIQaKMVX2vuj6FflxGn5lWJIHDVF6xVX99VtP1gzSMpCmCvnUVHBR
        AQIDAQAB
        -----END PUBLIC KEY-----
        '''

        auth_jwks = JWKSet.from_json("{   \"keys\":[      {         \"kid\":\"0d581143-aa56-4130-a371-86776e1cd338\",         \"kty\":\"RSA\",         \"d\":\"M9gzMLK830330N8jdeyHCoMvfPEslN8Nrz8jSpIduh8ACH24R5rBJ2nQjfivq7LNlEJ7n2oZMgPU28u9e8ImNbrkN5ZQSzfXZQPjbJe2zZLfijJr9hRnJuwflU0H0HXKm1T9z0FK-lBQ-XYJg6fKQKzJNBWn_hNUojIM3ZcTj5gaAaN6avad8VG7s4RS5kvC7dFUb0yVVs86q8GCDobGWfOaxf74iNcIdKyvjTKAKCw95E3p614GuKbAV9NCmmt0YQkfRey-_hCLPcK711KNMyze326e3lbFI-mVkVgub8Fs0ztqo4uD2SnsGBTI0_xzHqYY1mMcmGaeNHuJLtoa8Q\",         \"dp\":\"bsfO1sfNUc06WSCiBIUoOtHN9MKmzdXN7c5yIpOtpIjTuUSbjcaY8fvyTyirONbaqWvCP3kDhxsump65t36wMYDOEcxHjjUbI6KB9E0quJV4utwMNqpoxLnXI1nSNOjjNiaUPa5nnEJpa8YkCNQOx0pnzDBc4fF9XKi2nH7pwSU\",         \"dq\":\"N8-P3PBdsgdDUalDoNv0muWSTwkhTr43MhckHDGZTHTfLz5d4_GG7xSdZ5ZzZMfZ67OJnLyJgSYVr77ok1rUvfLWrqGVvEIACgdAWPst8NGJFNnoveckY1RirTBVs-sOTqmB86KhR3gb7tTugFFZ5O8ErQMB6TanvA5q2IOC55s\",         \"e\":\"AQAB\",         \"n\":\"zD1qUPjiTRu_OmXgMX0XUFx72NzA4LmvF8Wvwsay0NyEVOiu5KfOTF8pLQO37VNsoIgnbFw3Vo9HVaE6SIELwTuNTYjUwGrli5d9RK4f90kE8NOXfYNAIMv26XsfqUvRuq9SPSuNcX5L3Ggxo1IkM1IUU3LcDHwiD_vMyDta5MuWUlWAxRcPTAo50lZCbB7HSDThn1rBjfNbeA0KVEzrv5tLirqdSgNOh4h7igPN8YU-ZMmaLKedKoUvjghJxqB89JzrftR6oqUaR71zds1RK1Q8l5w996-gIQaKMVX2vuj6FflxGn5lWJIHDVF6xVX99VtP1gzSMpCmCvnUVHBRAQ\",         \"p\":\"9aBT9tefoo2f_Ohq4s40wyGYBYxa-oyDVMJZ1xEZPWSOQ_BzuxPba9YeRVkSh7extR8FRsnBrUtQIRim5e8gM_lp-v87c_DoMFeguCiYEQvgeiA-zEhDEBR5KNYCCHH6nzQbi0YCRmEEXeF6KYnlY1EKiW7i-J_FXeKdo7jlV5c\",         \"q\":\"1N2f2lcTcsbqEX_c3w2nZZC6AFHEjaW26awYTeofG9kFM4bsHX2Ar9rC2Esh8S4Af_g2eQmGEQIGE3dbMoMEtGJW0fuQ01DMd3mXgXmweNkwJIQVZDC45ZEYl0s8TDtUN3qjZLUPPho9_dZ0BJYmUrMxVEaaQwWKnGBYx2BW7yc\",         \"qi\":\"CgzzpEX_q_ke_S-3YtllJpO7iVfmi2YxJ68eaS8PtnGGk0lGeye0X7B-QxOKFm8bn8i4qBAe85gE9Wy_U7pLykHcEeJMZRQWGGu1v_ydtMFw5hXOO2_5XKjD8w8jkRVsB_iWpcfQehP7iOMj9MpozojQ0zOv_3mKtYJClnG5ZRI\"      }   ]}")

        # noinspection PyPep8
        prv_key_json = '{\"keys\":[{\"d\":\"LiY9nb5v4NW8-iBtU3Z_Bx-PNFzX57qeaEqh2AZuuvFLZpBNvKhELJ0_Re308LweyCYVJHaP6MUrVs9i2PwGTtNxxuZ835K09T_9wc5OcNIjzI5WxLc8j1Wwd65v1QlndmXh4MS9rPjPC3us4modamFcjf5I0Lg9o-0miCt77SYROl_ZuHCX8QVCVrySU4UkU9xpZONpkDn25CpwnrftRhO1hP60HIiXWA6vQvYYHpRX9f5wuqIshcoMzXccSHf_TMY9UEAsxpvIqO98eRCxWNF75RTBynddqoSWVBnP_JwCUWOG3ZtwS7EGYd5HncNKdramrh0XK-JOzS6xb7UyzQ\",\"dp\":\"I6knDqeSJTdbYw-OfXM2KmqgUZ-z6QaVqpLDPyNeTfJJTwklH1i9T5i_6t849fOKBOjrYhZ-Poz3t1tVJ7n77_colyr-i7o3irMoVnMI-m8EalwVGV61jJ7BlAruhnvrINNrGauUJ9E_2kkcFaJtlQy3LPN2r8e0oX2qH9NANkk\",\"dq\":\"DfKB5JouG30jiVnFJljMwMKTJw47o4bSJ5BGqZIxfNjVgtQir2-_Ekzh4F_kVMxFIDhSc-gKBFtDw1SMkbfZUZl8gdhZe9ejvhXuh08NuTKDbdayuWNCv4gGd7kwTIvjjI545uaLsc5fql7-D9Ai6gF40lfzZZp9If0yUve8CAM\",\"e\":\"AQAB\",\"kid\":\"9664a935-c49d-4914-963a-110aba5d6202\",\"kty\":\"RSA\",\"n\":\"saZoYnYQBKfWQY0Uy0stB3tpRVXCJce0YQhRPPpfYizQhueo-2I_UVrdenZhzyWKFg6LfR2z_pKcDPj5A3TepXXSWo4X8HnOKu3V9E1pplwfzE5NEGADd4-Fvfjs6RZtX8JLIhj3teBVO4LXBryHGOW-s24_DIqdItoZbpCvRwZaHskZG0ruWIX27hSCNQyxygLNRmbWRBYo0sHloOFPV2Yl3aM5N2VOTEfhkJr4346GHOGDmrDgirC3DdjpEqBmk1cQXP2T3J4EmMpZOsn-RiuRsjvBHIyRr9mooKYL2ekAmr8CD5PVeSslntf_F-_w4ATv4Ld2MLsXS7mrl41DBQ\",\"p\":\"5FJM0z-LeWKNtz7DZsheYKhViieNxgor2JhnsCJZd8L1IPlmwJ8tueyUjG7wbFrVsdfKp4Mo15CDXAMdMBjmvSa6oY1_hJIjiWvRbAbJ0MgMo60nc6Pv4H7N807Lg9AFhVDUaQIL47ldOYvp9cJBnNGWZoGKop-SllDsB4mN_m8\",\"q\":\"xy-TgNAeqeF8mLPE37KE99-jtcVaMR3wZsfytumwZW-lm8oVgmUcnt_u4157_ni3OidPgdcpItNvGi5L4h9MOmWJI7Hc3aSZnj7ZTFjeQ4OFg7eCmUxENtMXseGK1eRyfzE6e-y7PGtp3eVbz-yD_yED81LwTKSbx6zJD3S0D8s\",\"qi\":\"tKkmN7wcmG63hAQAoq9oD0KYXjv6V5bnrr5NBHHWtO_fqHpofqhHizYemmuQnvad8I7kg6h1iN_abhxMaE5gtUi-kzdRZy1hx_KU3w5XO2F6h60WKfQEuXtIkPFaGjtLPhzGTDDaisOsOYltKQOIuUkjaO3ZBatzGSi9BLLeqT8\"}]}'

        self.registration = {
            'client_id': u'lbgdkmy7lgo4s',
            'client_secret': u'HF2Lfedqhgu0UprrqTxqNrKPeydqei4nd51toWbG0JY',
        }

        self.ctx = ID4meContext(
            id4me='id200.connect.domains',
            identity_authority='authority.acme.com',
            issuer='https://authority.acme.com/',
        )

        self.client = TestID4meClientTestCommon._get_test_client(get_client_registration=None)
        self.client._get_openid_configuration = MagicMock(return_value={ 'jwks_uri': 'https://foo.com/jwks.json'})
        self.client._get_public_keys_set = MagicMock(return_value=auth_jwks)

    def test_Test0_Good_token(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjJ9.trG7rM-fJw-brOzU3oNfENXXb1r9W-eqYsY-pYLH20xm-VnIIMcJ9x7zqKL_bavH3oBOIqLkvErWVg8i4NEld67HfK5fwdlZ9MaA2HJ70bQehqC8WB1T-ybbO6qGI5nLobr-R40t0w39FmstThVmcxhhJlh6k-B0xkIfKsIQ0yG0W4MOibJyRw_WKmhYGvvFPlYI14U0bbvG9IqvgRL8go_rp3Y95kIQItzlibqsswfINRZXzgYmNdw_el3Xef3YucSiKcw0wd9-Vp9fi2xMU44_FwhEiE4QgP0vgm5SmCkmhHUzb8J7NF85cuh7WajWELdV9uWrAkeap_EwBE765Q"
            '''
                {
                    "iss": "https://authority.acme.com/",
                    "aud": "lbgdkmy7lgo4s",
                    "iat": 1526239022,
                    "exp": 1556239022
                }
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
        except ID4meTokenException as e:
            assert False, '[Test0_Good_token] Unexpected exception: {}'.format(e.message)

    def test_Test0_1_Good_token_all_features(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjBkNTgxMTQzLWFhNTYtNDEzMC1hMzcxLTg2Nzc2ZTFjZDMzOCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjIsImF6cCI6ImxiZ2RrbXk3bGdvNHMiLCJpZDRtZS5pZGVudGlmaWVyIjoiaWQyMDAuY29ubmVjdC5kb21haW5zIiwic3ViIjoidGVzdC5zdWIifQ.rij0aC3m8AaraPOTSzFPJRY19ZQgrLTQh_0cz2Bk0OHsuA0QJNpd-eN0g_GZKjhD_cazMtM-1h4WSUmZXgAMO2c2QIwmninT-RbvJQlId22x4d8L8t6d5JFdpvDLLC2L_jaeueDDOdeqaCJye2sbY8UUM8ozGy2F3bt8DWm4213vOZCfslRGJr-oY0BDSu9I8OQRuiDKHx4ZB0Awl9o_CbnwUt2KE9lXNMRYlph_BRT0Sv4c_5qP7U3MQGTL3Sd21Yj3nHEwq7MBSut-oxuARneTz_B5r6qOCmmm1obTNk5QKGNY_eXh7Wf-iWrLsFrRv5vW7Xt5woHuosGS2QX5CQ"
            '''
                {
                  "alg": "RS256",
                  "typ": "JWT",
                  "kid": "0d581143-aa56-4130-a371-86776e1cd338"
                }
                {
                  "iss": "https://authority.acme.com/",
                  "aud": "lbgdkmy7lgo4s",
                  "iat": 1526239022,
                  "exp": 1556239022,
                  "azp": "lbgdkmy7lgo4s",
                  "id4me.identifier": "id200.connect.domains",
                  "sub": "test.sub"
                }
            '''
            ctx_with_sub = ID4meContext(
                id4me='id200.connect.domains',
                identity_authority='authority.acme.com',
                issuer='https://authority.acme.com/',
            )
            ctx_with_sub.sub = 'test.sub'
            self.client._decode_token(token, ctx_with_sub, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
        except ID4meTokenException as e:
            assert False, '[test_Test0_1_Good_token_all_features] Unexpected exception: {}'.format(e.message)

    def test_Test0_2_Good_token_not_signed(self):
        try:
            token = "eyJhbGciOiJub25lIn0.eyJhdWQiOiJsYmdka215N2xnbzRzIiwic3ViIjoiNG5SWWJqeXNxTDVPdm01cElXRjcwakIwZ2hsZmtqRDBfY2V5aF9yNGZ1MCIsImF1dGhfdGltZSI6MTU3NDM1NTUwNCwiaXNzIjoiaHR0cHM6Ly9icm9rZXIubmV0aWQuZGUvIiwiZXhwIjoxNTc0MzU2NDI1LCJpYXQiOjE1NzQzNTU1MjUsIm5vbmNlIjoiYzI0ZTRmYzgtYmM4ZS00OTYxLWJjN2QtYjhiMjIxNGRhMGI2In0."
            '''
                {
                    "alg": "none"
                }
                {
                  "aud": "34c5c94a-af5f-432c-a1ef-b37e40d79629",
                  "sub": "4nRYbjysqL5Ovm5pIWF70jB0ghlfkjD0_ceyh_r4fu0",
                  "auth_time": 1574355504,
                  "iss": "https://broker.netid.de/",
                  "exp": 1574356425,
                  "iat": 1574355525,
                  "nonce": "c24e4fc8-bc8e-4961-bc7d-b8b2214da0b6"
                }
            '''
            ctx = ID4meContext(
                id4me='id200.connect.domains',
                identity_authority='broker.netid.de',
                issuer='https://broker.netid.de/'
            )
            ctx.nonce = 'c24e4fc8-bc8e-4961-bc7d-b8b2214da0b6'
            self.client._decode_token(token, ctx, self.registration, 'https://broker.netid.de/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
        except ID4meTokenException as e:
            assert False, '[test_Test0_2_Good_token_not_signed] Unexpected exception: {}'.format(e.message)


    def test_Test1_Empty_token(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.e30.vvssBSxwPSgjgE3R1oCXBSchRquU6bMesaQ17J7vUIsvGn-O4fDZWgKhG_YGwkJ-eLNAFkhoX6xNLCbmrbJ-b2RPaRRxaMRGvUnKjVCw6rJPaJ5vd-orylGuI2IsM7gZy3kWq0K7hnYmofKDHKhtbQMsZ35GJW9u9kVfxyIdOs-HNaYO7iBWTa92lYnpNgDoGpTY5-BUPqmIzN89NIMUu0Sj4wSy9KLz89bEGN0hMcSNuQi5DvzY61N6WKmHp6grKUBSe0IORd_e5QV1sCFy_EcbrJdrIgnP0bhhj2JypZBqojSw55FgA_7Ki_tPQs9UuJZSueSBaDz_vdhlr80R2w"
            '''
            	{
				}
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test1_Empty_token] No exception'
        except ID4meTokenException as e:
            print("[Test1_Empty_token] Expected exception: {}".format(e.message))

    def test_Test2_iss_not_matching(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL3N1Yi5hdXRob3JpdHkuYWNtZS5jb20vIn0.UkIREX1TWjIhF8m9EKocJ76WBffDj-Gh-gBdn8iZ34XfXwhQa8AIzAsy4LxUkrbjCQMRHWGsCPeA15l1Qv4WmGunrTgzHYfz7SkH8NQAbxrs4N2clX-rOgkptHiMymnm_AT7I3gxJ1CfKsQbq4LedbS_6BGNqSs7qgu8tkWndfq6j6TT7Kitr8JZkK6cuAMVQ2EQa-w-LSjBlt3qnPofhukPJyF0mwRo3owmdyA2Lo14a7avi14GNEZ_0etaT9sijeIazE3okCXpEimBqMMPepzNrR1olatqWY_f3197ttF9oQwtadqX7LMUlGoKDilLLgfEgH_h6B0nwjU3u46OJw"
            '''
				{
				   "iss": "https://sub.authority.acme.com/"
				}
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test2_iss_not_matching] No exception'
        except ID4meTokenException as e:
            print("[Test2_iss_not_matching] Expected exception: {}".format(e.message))

    def test_Test3_aud_not_matching(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJ3cm9uZ2lkIn0.EnmEAmv7W8mZ2D3JVVzwotnFlyv1UC1Q3NbzcRdL29ePCXvdL0tF971FXe0yFycXSs0RgEFjW95W2IAzkSVu4mi1btDPX2ijpfzedEFJEw5fu0FUXVEYS8NV5jmWdLYrYJr_VXA2kosNxXnD0SAAfrLbwQCm3bytxhA_YRW5slF7eQ8SOg4ynnpsvdibWw0-myT4wqvFxT-CgrbyCySnfxwp8xpSu1ejCLmMPWuntGuQhH5bEH0REarTI2vgN3s61kL3ZcYAX5LC1gThBxxxGkCFYe_NK6TCB5SfGjbLsrGgMn19zYTmjg8yrXMIkYUIckG30YqYR18J7Iw5QE_YZg"
            '''
				{
				   "iss": "https://authority.acme.com/",
				   "aud": "wrongid"
				}
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test3_aud_not_matching] No exception'
        except ID4meTokenException as e:
            print("[Test3_aud_not_matching] Expected exception: {}".format(e.message))

    def test_Test4_multi_aud_matching_no_azp(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOlsid3JvbmdpZCIsImxiZ2RrbXk3bGdvNHMiXX0.wy5BAPIzo8of0WwRuNU7zovvXjSUufR-hhsif1WPjw_qQuEuB33zgEYIq-G17Wvg-0sQzOdjZhBOIesHcGrWjyY8_OPKUq6x5y6DfKgbWPooJyB61soRg-erD4Sq4BWFGbQ814WmJUBmVX3i6IBUqyCYm3bTky7BifOhsiFouZ4uycJSE8T_t0ajNo7-Sxm9NiHffMyO6IEppJgRSoxG9ha_FbQgfr2YcE8H5oJCWf4TgF6sJquqoyfb8x71nUBI1P2FFP_2lpPX-5o38YGTzdps0d8DYcd5bH3k_0spl6C-n_EhogrMdlFxpqCLa8zBpnME_kXN2v-gId0DdTbrwg"
            '''
				{
				   "iss": "https://authority.acme.com/",
				   "aud": ["wrongid", "lbgdkmy7lgo4s"]
				}
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test4_multi_aud_matching_no_azp] No exception'
        except ID4meTokenException as e:
            print("[Test4_multi_aud_matching_no_azp] Expected exception: {}".format(e.message))

    def test_Test5_signed_with_other_keys(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTU2MjM5MDIyfQ.S3b-dFbtP2LEhdCNmVTzVwKF4ItG1LXnTr8OyMS4Hsmm-7I-iZEtrK1Iy-zXc_JEC5mUA1T73zoNeDPbSwJAn0OLWWe82tbbohpD0JxbpMuKc6Jv_TBk3l815aNNsHcMFsUgfuI82RiOJ94MeQqzOfvXuyoI6Uwc6JnWu_VDCb4qYa3oTx3CPzDFa9IVjzPwwB7If9h0NDL0bjUS-dyuSl3kffRK6-tr2fK0yDzHYoIIeBoERBowp7iIq5-R8cGgXY_tm4kxKl0w8DBCEHehNWJjV_X4acl1a4Q_xnvKiI_M9dSf-d6g_Yh6euQiLyhFKIJFJRF2ZLLU8XwBX_ZH1g"
            '''
				{
				  "iss": "https://authority.acme.com/",
				  "aud": "lbgdkmy7lgo4s",
				  "iat": 1556239022
				}
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test5_signed_with_other_keys] No exception'
        except ID4meTokenException as e:
            print("[Test5_signed_with_other_keys] Expected exception: {}".format(e.message))

    def test_Test6_signed_with_other_alg(self):
        try:
            token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTU2MjM5MDIyfQ.Woc7qYWGvrTK59ZTrriR1e7pp9aFU6ZT3_gggxgibgw"
            '''
				{
				  "alg": "HS256",
				  "typ": "JWT"
				}
				{
				  "iss": "https://authority.acme.com/",
				  "aud": "lbgdkmy7lgo4s",
				  "iat": 1556239022
				}
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test6_signed_with_other_alg] No exception'
        except ID4meTokenException as e:
            print("[Test6_signed_with_other_alg] Expected exception: {}".format(e.message))

    def test_Test7_token_expired(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1MzYyMzkwMjJ9.t403-tr7wD-9BcrZxpGTLGUojte_aqDVVOHd4pCC0-_JHUsP8GysNs3r-Xjsf8ATbAYkZvYMgzRI9iwT4lMuIIsqWFvq17Odp74WitnMc0da6Vlhsq9S7jEloUNyZchXMU0jQXdp4__moDZJCTs5_ba3ZGKAfXKx_kYAbT5iunc1X2agmKNUL-EPTqK0UBd8TVIGTi4ZwkW9pFlkZnaC6DscIkbNmDEvmZ1K0uzYwwVzARHzS3S7oyKlJiv4Mm7Gi5N1yuKbLwc20bLHCdkTMkL1BJPrK_ygYe3tyocMUeFQ6uRY1mLRNWyOb8xlf98OIoP1OaLWAdmMmNBMA43ZwA"
            '''
				{
					"iss": "https://authority.acme.com/",
					"aud": "lbgdkmy7lgo4s",
					"iat": 1526239022,
					"exp": 1536239022
				}
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(minutes=5), verify_aud=True)
            assert False, '[Test7_token_expired] No exception'
        except ID4meTokenException as e:
            print("[Test7_token_expired] Expected exception: {}".format(e.message))

    def test_Test8_nonce_missing(self):
        try:
            self.ctx.nonce = '33A04060-4A0C-44D3-9B3D-2E0FA0829235'
            try:
                token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjJ9.trG7rM-fJw-brOzU3oNfENXXb1r9W-eqYsY-pYLH20xm-VnIIMcJ9x7zqKL_bavH3oBOIqLkvErWVg8i4NEld67HfK5fwdlZ9MaA2HJ70bQehqC8WB1T-ybbO6qGI5nLobr-R40t0w39FmstThVmcxhhJlh6k-B0xkIfKsIQ0yG0W4MOibJyRw_WKmhYGvvFPlYI14U0bbvG9IqvgRL8go_rp3Y95kIQItzlibqsswfINRZXzgYmNdw_el3Xef3YucSiKcw0wd9-Vp9fi2xMU44_FwhEiE4QgP0vgm5SmCkmhHUzb8J7NF85cuh7WajWELdV9uWrAkeap_EwBE765Q"
                '''
                    {
                        "iss": "https://authority.acme.com/",
                        "aud": "lbgdkmy7lgo4s",
                        "iat": 1526239022,
                        "exp": 1536239022
                    }
                '''
                self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
                assert False, '[Test8_nonce_missing] No exception'
            except ID4meTokenException as e:
                print("[Test8_nonce_missing] Expected exception: {}".format(e.message))
        finally:
            self.ctx.nonce = None

    def test_Test9_nonce_incorrect(self):
        try:
            self.ctx.nonce = '33A04060-4A0C-44D3-9B3D-2E0FA0829235'
            try:
                token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1MzYyMzkwMjIsIm5vbmNlIjoiRjkxOURGNTktQzc3MS00MDFGLTlGQTAtREIwODdCNjU3OUFDIn0.k-utZJpsp6mycHEXCjY_AWG3npI-VOEcMHvU1dewGQE5rdjLR2OzDH60NQpdWlw8p-2MtEMPrG7X8VW24AnY-Sl_cFuczxV9PxXC_kVfPQKmBqmgv15lqvj58KSK6VtREcgtDBWuL0NJshBs_esWaHZSc6HKtMvFOXoByBgWjVK4ulMniADfYcQ6YzWC2biyKNTMvRsJUdj1bOshg-9JpeIJ5Jya6ypGQtfZn7ke1hsNr-dL7dyOwLP-bRMHM4aF-KX9HBBXQfkTKcUpRpXjAmOx5Q7wVNyttn3XEu-4Nv5nnSeVi-e28go7VBsyIvCRAlVj0-immfBBrWTHJAzH-A"
                '''
				{
					"iss": "https://authority.acme.com/",
					"aud": "lbgdkmy7lgo4s",
					"iat": 1526239022,
					"exp": 1536239022,
					"nonce": "F919DF59-C771-401F-9FA0-DB087B6579AC"
				}
                '''
                self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
                assert False, '[Test9_nonce_incorrect] No exception'
            except ID4meTokenException as e:
                print("[Test9_nonce_incorrect] Expected exception: {}".format(e.message))
        finally:
            self.ctx.nonce = None

    def test_Test10_nonce_replay(self):
        try:
            self.ctx.nonce = 'F919DF59-C771-401F-9FA0-DB087B6579AC'
            try:
                token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1MzYyMzkwMjIsIm5vbmNlIjoiRjkxOURGNTktQzc3MS00MDFGLTlGQTAtREIwODdCNjU3OUFDIn0.k-utZJpsp6mycHEXCjY_AWG3npI-VOEcMHvU1dewGQE5rdjLR2OzDH60NQpdWlw8p-2MtEMPrG7X8VW24AnY-Sl_cFuczxV9PxXC_kVfPQKmBqmgv15lqvj58KSK6VtREcgtDBWuL0NJshBs_esWaHZSc6HKtMvFOXoByBgWjVK4ulMniADfYcQ6YzWC2biyKNTMvRsJUdj1bOshg-9JpeIJ5Jya6ypGQtfZn7ke1hsNr-dL7dyOwLP-bRMHM4aF-KX9HBBXQfkTKcUpRpXjAmOx5Q7wVNyttn3XEu-4Nv5nnSeVi-e28go7VBsyIvCRAlVj0-immfBBrWTHJAzH-A"
                '''
				{
					"iss": "https://authority.acme.com/",
					"aud": "lbgdkmy7lgo4s",
					"iat": 1526239022,
					"exp": 1536239022,
					"nonce": "F919DF59-C771-401F-9FA0-DB087B6579AC"
				}
                '''
                self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
                self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
                assert False, '[Test10_nonce_replay] No exception'
            except ID4meTokenException as e:
                print("[Test10_nonce_replay] Expected exception: {}".format(e.message))
        finally:
            self.ctx.nonce = None

    def test_Test11_Good_token_encryption_required_not_fulfilled(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjJ9.trG7rM-fJw-brOzU3oNfENXXb1r9W-eqYsY-pYLH20xm-VnIIMcJ9x7zqKL_bavH3oBOIqLkvErWVg8i4NEld67HfK5fwdlZ9MaA2HJ70bQehqC8WB1T-ybbO6qGI5nLobr-R40t0w39FmstThVmcxhhJlh6k-B0xkIfKsIQ0yG0W4MOibJyRw_WKmhYGvvFPlYI14U0bbvG9IqvgRL8go_rp3Y95kIQItzlibqsswfINRZXzgYmNdw_el3Xef3YucSiKcw0wd9-Vp9fi2xMU44_FwhEiE4QgP0vgm5SmCkmhHUzb8J7NF85cuh7WajWELdV9uWrAkeap_EwBE765Q"
            '''
                {
                    "iss": "https://authority.acme.com/",
                    "aud": "lbgdkmy7lgo4s",
                    "iat": 1526239022,
                    "exp": 1556239022
                }
            '''
            client = TestID4meClientTestCommon._get_test_client(get_client_registration=None,
                                                                params={'requireencryption': True},
                                                                generate_private_keys=True)
            client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/',
                                 TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365),
                                 verify_aud=True)
            assert False, \
                '[Test11_Good_token_encryption_required_not_fulfilled] No expected exception'
        except ID4meTokenException as e:
            print("[Test11_Good_token_encryption_required_not_fulfilled] Expected exception: {}".format(e.message))

    def test_Test12_invalid_token_type(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IlhYWCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjJ9.WqaUPVQUk65bIOAnYcgt8en6IJgqomCQTjrllJMRlDfkQWmdMvAKA3e11sJkTrDrcNgV-9HbFhORHF93PoRnj9Z__dafXn98E6s957YN6eAEOqh66ewfTi5lZO-p8BqsRcil7aqQZjNsAmMnpBV3yelbRAPAJErXQRnBA0_ZyGaHbVVm2HAuaP77szFyYy-bBkYYkWaqEb8U1VqXQPTQgE3j_PaavB2cFNoNtMtNz0TFhjJ8eO1MlCEm5awAa1EdWi0K4RqAi9rwloEGeGeg0wAUbvjNEYUPiVs1L5g_CNJL70FEkxBtuIybhOYw9GqblQk9LN_rZOPLCH2Pxlt5Ag"
            '''
                {
                  "alg": "RS256",
                  "typ": "XXX"
                }
                {
                    "iss": "https://authority.acme.com/",
                    "aud": "lbgdkmy7lgo4s",
                    "iat": 1526239022,
                    "exp": 1556239022
                }
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test12_invalid_token_type] No expected exception'
        except ID4meTokenException as e:
            print("[Test12_invalid_token_type] Expected exception: {}".format(e.message))

    def test_Test13_missing_alg(self):
        try:
            token = "eyJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjJ9.WqaUPVQUk65bIOAnYcgt8en6IJgqomCQTjrllJMRlDfkQWmdMvAKA3e11sJkTrDrcNgV-9HbFhORHF93PoRnj9Z__dafXn98E6s957YN6eAEOqh66ewfTi5lZO-p8BqsRcil7aqQZjNsAmMnpBV3yelbRAPAJErXQRnBA0_ZyGaHbVVm2HAuaP77szFyYy-bBkYYkWaqEb8U1VqXQPTQgE3j_PaavB2cFNoNtMtNz0TFhjJ8eO1MlCEm5awAa1EdWi0K4RqAi9rwloEGeGeg0wAUbvjNEYUPiVs1L5g_CNJL70FEkxBtuIybhOYw9GqblQk9LN_rZOPLCH2Pxlt5Ag"
            '''
                {
                  "typ": "JWT"
                }
                {
                    "iss": "https://authority.acme.com/",
                    "aud": "lbgdkmy7lgo4s",
                    "iat": 1526239022,
                    "exp": 1556239022
                }
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test13_missing_alg] No expected exception'
        except ID4meTokenException as e:
            print("[Test13_missing_alg] Expected exception: {}".format(e.message))

    def test_Test14_id_token_signed_response_alg_not_matching_registration(self):
        try:
            custom_registration = {
                'client_id': u'lbgdkmy7lgo4s',
                'client_secret': u'HF2Lfedqhgu0UprrqTxqNrKPeydqei4nd51toWbG0JY',
                'id_token_signed_response_alg': 'RS256',
            }

            token = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjJ9.Erm4uA9WN7Q4ZvbbWO8z81_AZgDJDD_a0K0s2dLyWENIFTOZJIVEURcMAeYATDwcebEebvDwm_tLm0Ia-I0TnEVB-bXPSViiaH3kfMAdoOHug5J2jrfkqR4QwxDlnFiwFeJ8zq6sTWbpLMxQLc6WCyrLcGPHf6cMHOKNe3MpTaeBcbyIAzbFUIfQl-ZLlXxtKqty04DBvCyTI00z61aGmBsNhpqgDwqaEX-Utmzh4fvqHFh1-_Jx-3tWuPTOI_mWITJXGO9mlQZoIehIFQoMryLUSCovKspL8PJ3QmXHx77dHkFjqn6P5mHazDxcmL8Gm5DAg18NMpLpvhGG8a6kcA"
            '''
                {
                  "alg": "RS512",
                  "typ": "JWT"
                }
                {
                    "iss": "https://authority.acme.com/",
                    "aud": "lbgdkmy7lgo4s",
                    "iat": 1526239022,
                    "exp": 1556239022
                }
            '''
            self.client._decode_token(token, self.ctx, custom_registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test14_id_token_signed_response_alg_not_matching_registration] No expected exception'
        except ID4meTokenException as e:
            print("[Test14_id_token_signed_response_alg_not_matching_registration] Expected exception: {}".format(e.message))

    def test_Test15_userinfo_signed_response_alg_not_matching_registration(self):
        try:
            custom_registration = {
                'client_id': u'lbgdkmy7lgo4s',
                'client_secret': u'HF2Lfedqhgu0UprrqTxqNrKPeydqei4nd51toWbG0JY',
                'userinfo_signed_response_alg': 'RS256',
            }

            token = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjJ9.Erm4uA9WN7Q4ZvbbWO8z81_AZgDJDD_a0K0s2dLyWENIFTOZJIVEURcMAeYATDwcebEebvDwm_tLm0Ia-I0TnEVB-bXPSViiaH3kfMAdoOHug5J2jrfkqR4QwxDlnFiwFeJ8zq6sTWbpLMxQLc6WCyrLcGPHf6cMHOKNe3MpTaeBcbyIAzbFUIfQl-ZLlXxtKqty04DBvCyTI00z61aGmBsNhpqgDwqaEX-Utmzh4fvqHFh1-_Jx-3tWuPTOI_mWITJXGO9mlQZoIehIFQoMryLUSCovKspL8PJ3QmXHx77dHkFjqn6P5mHazDxcmL8Gm5DAg18NMpLpvhGG8a6kcA"
            '''
                {
                  "alg": "RS512",
                  "typ": "JWT"
                }
                {
                    "iss": "https://authority.acme.com/",
                    "aud": "lbgdkmy7lgo4s",
                    "iat": 1526239022,
                    "exp": 1556239022
                }
            '''
            self.client._decode_token(token, self.ctx, custom_registration, 'https://authority.acme.com/', TokenDecodeType.UserInfo, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test15_userinfo_signed_response_alg_not_matching_registration] No expected exception'
        except ID4meTokenException as e:
            print("[Test15_userinfo_signed_response_alg_not_matching_registration] Expected exception: {}".format(e.message))

    def test_Test16_invalid_token_content(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjBkNTgxMTQzLWFhNTYtNDEzMC1hMzcxLTg2Nzc2ZTFjZDMzOCIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjI.b-EatWweCX-cNxQ-hO0iIQkM8X7qQf9tlYCn8XsRFUbuTS2D4HpriJ9L-wrHF1PJkw853BH2xqq0lE2sAiBRLr4xSbNleOOMU0RKzu-5DbgcuauTpX73pDb604oAisKDRAHd7khp34Lt3QWNaIOq1J99Ew6HOeZKsQTHdrWwOfIV3zAr94w6Oddxtp4dhTE9-br8kNZP6-lW_MrUNpd2RdA6bzYVKKUDExhis74v6mMzYdVPkV-t_h6Le0kmxo76u4uDDIH3oT85TdlzR7UORlpHWtet6f_RAzZag6VFRzE1IIOD7NeowG5ApX2vjz77aLFEqIfkBSSZq02Q3n7DxA"
            '''
                {
                    "alg":"RS256",
                    "kid":"0d581143-aa56-4130-a371-86776e1cd338",
                    "typ":"JWT"
                }
                {
                    "iss": "https://authority.acme.com/",
                    "aud": "lbgdkmy7lgo4s",
                    "iat": 1526239022,
                    "exp": 1556239022
                
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/', TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365), verify_aud=True)
            assert False, '[Test16_invalid_token_content] No expected exception'
        except ID4meTokenException as e:
            print("[Test16_invalid_token_content] Expected exception: {}".format(e))

    def test_Test17_Invalid_id4me_identifier(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjIsImlkNG1lLmlkZW50aWZpZXIiOiJ3cm9uZy5pZCJ9.EKkF1pSPV19FBUhkGgHIUFAXyEFQncDd2e89jAVEQdcCkymehn9tW_58k-W7-8s8Uv6oM_K-IjfdyDPbJlD2t7d5EhwVWrDKWtXTGaWO6u-vaJmAOT1ufsVd9f08GJ--X-XFlL6SzJBNOHTKB2wgYvGtjf7AsLpdZA-P7kDuPOpnBJ5mEemEj4xlSDytTjOrAjs_DInqvmEC0mowMg-vuPUAOtBCIpyZ3Hb5l3WYW8dJ893rGuJ4aN97T5y00YIunr1UEmM6jHXnVaaxRF_eNzryPNuxnFm4U0IuwMrMzC46bn1f3iQIgXzjMnS7GH6-9Vfu2nBm6exuFLBpcS-jKw"
            '''
                {
                  "iss": "https://authority.acme.com/",
                  "aud": "lbgdkmy7lgo4s",
                  "iat": 1526239022,
                  "exp": 1556239022,
                  "id4me.identifier": "wrong.id"
                }
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/',
                                      TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365),
                                      verify_aud=True)
            assert False, \
                '[Test17_Invalid_id4me_identifier] No expected exception'
        except ID4meTokenException as e:
            print("[Test17_Invalid_id4me_identifier] Expected exception: {}".format(e.message))

    def test_Test18_Invalid_sub(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjIsInN1YiI6InRva2VuLnN1YiJ9.pzF-AlYPj_Mn_gxiX4JdoXfi3vYxN9gtkqhbqM7zWK9Aa1QiyZ35hx0RHJfcwkXsPQunGzIMcAparU2QQxlQ3U5BZc0MOh_xGTmoKq3YMlG1Ovc1xuRzQ2PqYFXSlscVhNRR__UAkG-HhIwONlV_iZtZqT8QTb9RT1zsG4Zmz1kEU4L3cCcaxPv8C5zo_-hXopZfq5_khPD7ogN9m42ezGum8V343KRL63PuzQhLyCqwIrDGGvDOuV_YPIjwyI49lHL0P1J1dcSTbowpLxvLTywv7sKBazV0ixo5a1H5OsnSzEJyA7LAyAbNEKPTkJfc0K1cpvR1ifrA3MFBwCR01g"
            '''
                {
                  "iss": "https://authority.acme.com/",
                  "aud": "lbgdkmy7lgo4s",
                  "iat": 1526239022,
                  "exp": 1556239022,
                  "sub": "token.sub"
                }
            '''
            ctx_with_sub = ID4meContext(
                id4me='id200.connect.domains',
                identity_authority='authority.acme.com',
                issuer='https://authority.acme.com/',
            )
            ctx_with_sub.sub = 'context.sub'
            self.client._decode_token(token, ctx_with_sub, self.registration, 'https://authority.acme.com/',
                                      TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365),
                                      verify_aud=True)
            assert False, \
                '[Test18_Invalid_sub] No expected exception'
        except ID4meTokenException as e:
            print("[Test18_Invalid_sub] Expected exception: {}".format(e.message))


    def test_Test19_Invalid_azp(self):
        try:
            token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjIsImF6cCI6InRva2VuLmF6cCJ9.onoixdtEpKJlfnUe0gT-EthF0hm9-zrbdA5aTFyUCGKNdLjnFxxIv2GeR5953AaIRoWYS1Xqr1GGoGagJHxcG-zs1zgFCpGhGxGdimwrKF-wkBDMQmYEp5ueqOmXNiv1sWIh9wguazf86w-6z3ABLjrx-yx72aqTfF1BibIPw0znLTrGFgs7AtGgZWaahNf7ynKMJ3WEG332d9ReIEhuZKQ3RO8DN7Z6_fXWUxhY3T3DP3Rdb-OGjxrw3kMtk9Qe2VCn0Is6AtmEJ3u5OU_f_aaNSGfyLgxfnANpr21qw0CubKdyIQeKETDNi6VcuBCJI_FkBDAoqy9cChTA6E41-A"
            '''
                {
                  "iss": "https://authority.acme.com/",
                  "aud": "lbgdkmy7lgo4s",
                  "iat": 1526239022,
                  "exp": 1556239022,
                  "azp": "token.azp"
                }
            '''
            self.client._decode_token(token, self.ctx, self.registration, 'https://authority.acme.com/',
                                      TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365),
                                      verify_aud=True)
            assert False, \
                '[Test19_Invalid_azp] No expected exception'
        except ID4meTokenException as e:
            print("[Test19_Invalid_azp] Expected exception: {}".format(e.message))
