__author__ = "Pawel Kowalik"
__copyright__ = "Copyright 2018, 1&1 IONOS SE"
__credits__ = []
__license__ = "MIT"
__maintainer__ = "Pawel Kowalik"
__email__ = "pawel-kow@users.noreply.github.com"
__status__ = "Beta"

from enum import Enum

from unittest2 import TestCase

from id4me_rp_client.helper.stringify_keys import stringify_keys


class TestEnumStr(Enum):
    vala = 'val1'
    valb = 'val2'
    valc = 'val3'
    vald = 'val4'

    def __str__(self):
        return self.value


class TestEnumRepr(Enum):
    vala = 'val1'
    valb = 'val2'
    valc = 'val3'
    vald = 'val4'

    def __str__(self):
        raise Exception('Just a test')

    def __repr__(self):
        return self.value


class TestEnumNoSupport(Enum):
    vala = 'val1'

    def __str__(self):
        raise Exception('Just a test')

    def __repr__(self):
        raise Exception('Just a test')


class TestRefObject(object):
    def __init__(self, prop1=None, prop2=None):
        if prop1 is not None:
            self.prop1 = prop1
        if prop2 is not None:
            self.prop2 = prop2


class TestStringify_keys(TestCase):
    def test_stringify_keys_str(self):
        enum_dict = {
            TestEnumStr.vala: TestRefObject(
                prop1=True,
                prop2='To call you by name'),
            TestEnumStr.valb: TestRefObject(
                prop1=True,
                prop2='To call you by name'),
            TestEnumStr.valc: TestRefObject(
                prop1=True,
                prop2='To call you by name'),
            TestEnumStr.vald: TestRefObject(
                prop2='To display your picture'),
        }
        try:
            stringify_keys(enum_dict)
        except Exception as e:
            assert False, 'Exception while stringifying: {}'.format(e)

        for key in enum_dict.keys():
            assert isinstance(key, str), 'Key is not str: {}'.format(key)

    def test_stringify_keys_repr(self):
        enum_dict = {
            TestEnumRepr.vala: TestRefObject(
                prop1=True,
                prop2='To call you by name'),
            TestEnumRepr.valb: TestRefObject(
                prop1=True,
                prop2='To call you by name'),
            TestEnumRepr.valc: TestRefObject(
                prop1=True,
                prop2='To call you by name'),
            TestEnumRepr.vald: TestRefObject(
                prop2='To display your picture'),
        }
        try:
            stringify_keys(enum_dict)
        except Exception as e:
            assert False, 'Exception while stringifying: {}'.format(e)

        for key in enum_dict.keys():
            assert isinstance(key, str), 'Key is not str: {}'.format(key)

    def test_stringify_keys_repr(self):
        enum_dict = {
            TestEnumNoSupport.vala: TestRefObject(
                prop1=True,
                prop2='To call you by name'),
        }
        try:
            stringify_keys(enum_dict)
            assert False, 'No exception while stringifying: {}'.format(e)
        except Exception as e:
            pass

    def test_stringify_keys_nested_dict(self):
        enum_dict = {
            TestEnumStr.vala: TestRefObject(
                prop1=True,
                prop2='To call you by name'),
        }

        testdict = {
            "a": enum_dict,
            "b": "test",
        }
        try:
            stringify_keys(testdict)
        except Exception as e:
            assert False, 'Exception while stringifying: {}'.format(e)

        for key in testdict.keys():
            assert isinstance(key, str), 'Key is not str: {}'.format(key)

        for key in testdict["a"].keys():
            assert isinstance(key, str), 'Key is not str: {}'.format(key)
