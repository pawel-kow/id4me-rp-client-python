__author__ = "Pawel Kowalik"
__copyright__ = "Copyright 2018, 1&1 IONOS SE"
__credits__ = []
__license__ = "MIT"
__maintainer__ = "Pawel Kowalik"
__email__ = "pawel-kow@users.noreply.github.com"
__status__ = "Beta"

import datetime
import json
import random
from unittest import mock
from unittest.mock import MagicMock, call, Mock

from dns.resolver import Resolver, NXDOMAIN, Timeout, YXDOMAIN, NoAnswer, NoNameservers
from jwcrypto.jwk import JWKSet
from test_ID4meClientTestCommon import TestID4meClientTestCommon
from unittest2 import TestCase

from id4me_rp_client import *


class TestID4meClient(TestCase):

    _example_jwks_str = '{"keys": [{"e": "AQAB", "kid": "keyId", "kty": "RSA", "n": "rx3-cQZDJeASwj4uinuzBVbRRg2Kmhk8et5eDDmZvEEZcIhdnlFjUrNKNg4ztx41BRNB_NQD-kyroI8R6tGBIBBG5Tlb6Q1hZ67WK5I5eAMKw1BSbAcaOXkTnEcjORnSAj4QLGXlaPZ5sLiRE-_U4OEwWDzB6Wkf_yDjIj1UOypjdCbWk9-lckfyaS-nXrIMzrL6ZaNKvj4mOuwSHj5h47UdY3NmGm7J6QDqcyBpl_2Lgv3fxhn2aw375gY7QopMdkjpDvZJWYbgGlk5IWyH-btKwEeH2KDuAPweGPSMaTGvsu0CkSd9v6QdPf53niku1K-Omuas85i9OXCFeAa-UQ", "use": "sig"}]}'

    _denic_openid_config = \
        {
            "issuer": "https://id.test.denic.de",
            "jwks_uri": "https://id.test.denic.de/jwks.json",
            "authorization_endpoint": "https://id.test.denic.de/login",
            "token_endpoint": "https://id.test.denic.de/token",
            "registration_endpoint": "https://id.test.denic.de/clients",
            "introspection_endpoint": "https://id.test.denic.de/token/introspect",
            "revocation_endpoint": "https://id.test.denic.de/token/revoke",
            "scopes_supported": [
                "openid",
                "profile",
                "email",
                "address",
                "phone"
            ],
            "response_types_supported": [
                "code"
            ],
            "response_modes_supported": [
                "query",
                "fragment"
            ],
            "grant_types_supported": [
                "authorization_code",
                "refresh_token",
                "password",
                "client_credentials",
                "urn:ietf:params:oauth:grant-type:jwt-bearer"
            ],
            "code_challenge_methods_supported": [
                "S256",
                "plain"
            ],
            "token_endpoint_auth_methods_supported": [
                "client_secret_basic",
                "client_secret_post",
                "client_secret_jwt",
                "private_key_jwt"
            ],
            "token_endpoint_auth_signing_alg_values_supported": [
                "HS256",
                "HS384",
                "HS512",
                "RS256",
                "RS384",
                "RS512",
                "PS256",
                "PS384",
                "PS512",
                "ES256",
                "ES384",
                "ES512"
            ],
            "request_object_signing_alg_values_supported": [
                "HS256",
                "HS384",
                "HS512",
                "RS256",
                "RS384",
                "RS512",
                "PS256",
                "PS384",
                "PS512",
                "ES256",
                "ES384",
                "ES512",
                "none"
            ],
            "ui_locales_supported": [
                "en"
            ],
            "request_parameter_supported": True,
            "request_uri_parameter_supported": True,
            "require_request_uri_registration": True,
            "tls_client_certificate_bound_access_tokens": True,
            "request_uri_quota": 10,
            "subject_types_supported": [
                "public",
                "pairwise"
            ],
            "userinfo_endpoint": "https://id.test.denic.de/userinfo",
            "end_session_endpoint": "https://id.test.denic.de/logout",
            "id_token_signing_alg_values_supported": [
                "RS256",
                "RS384",
                "RS512",
                "PS256",
                "PS384",
                "PS512",
                "ES256",
                "ES384",
                "ES512",
                "HS256",
                "HS384",
                "HS512",
                "none"
            ],
            "id_token_encryption_alg_values_supported": [
                "RSA1_5",
                "RSA-OAEP",
                "RSA-OAEP-256",
                "ECDH-ES",
                "ECDH-ES+A128KW",
                "ECDH-ES+A192KW",
                "ECDH-ES+A256KW",
                "dir",
                "A128KW",
                "A192KW",
                "A256KW",
                "A128GCMKW",
                "A192GCMKW",
                "A256GCMKW"
            ],
            "id_token_encryption_enc_values_supported": [
                "A128CBC-HS256",
                "A192CBC-HS384",
                "A256CBC-HS512",
                "A128GCM",
                "A192GCM",
                "A256GCM"
            ],
            "userinfo_signing_alg_values_supported": [
                "RS256",
                "RS384",
                "RS512",
                "PS256",
                "PS384",
                "PS512",
                "ES256",
                "ES384",
                "ES512",
                "HS256",
                "HS384",
                "HS512",
                "none"
            ],
            "userinfo_encryption_alg_values_supported": [
                "RSA1_5",
                "RSA-OAEP",
                "RSA-OAEP-256",
                "ECDH-ES",
                "ECDH-ES+A128KW",
                "ECDH-ES+A192KW",
                "ECDH-ES+A256KW",
                "dir",
                "A128KW",
                "A192KW",
                "A256KW",
                "A128GCMKW",
                "A192GCMKW",
                "A256GCMKW"
            ],
            "userinfo_encryption_enc_values_supported": [
                "A128CBC-HS256",
                "A192CBC-HS384",
                "A256CBC-HS512",
                "A128GCM",
                "A192GCM",
                "A256GCM"
            ],
            "display_values_supported": [
                "page",
                "popup"
            ],
            "claim_types_supported": [
                "normal"
            ],
            "claims_supported": [
                "sub",
                "iss",
                "auth_time",
                "name",
                "given_name",
                "family_name",
                "middle_name",
                "nickname",
                "preferred_username",
                "profile",
                "picture",
                "website",
                "email",
                "email_verified",
                "gender",
                "birthdate",
                "zoneinfo",
                "locale",
                "phone_number",
                "phone_number_verified",
                "address",
                "updated_at"
            ],
            "claims_parameter_supported": True,
            "frontchannel_logout_supported": True,
            "frontchannel_logout_session_supported": True,
            "backchannel_logout_supported": True,
            "backchannel_logout_session_supported": True
        }

    _identityagent_openid_config = \
        {
            "claims_supported": [
                "name",
                "given_name",
                "family_name",
                "middle_name",
                "nickname",
                "preferred_username",
                "profile",
                "picture",
                "website",
                "email",
                "email_verified",
                "gender",
                "birthdate",
                "zoneinfo",
                "locale",
                "phone_number",
                "phone_number_verified",
                "address"
            ],
            "id_token_signing_alg_values_supported": [
                "RS256"
            ],
            "issuer": "https://identityagent.de",
            "jwks_uri": "https://identityagent.de/jwks.json",
            "response_types_supported": [
                "code"
            ],
            "userinfo_endpoint": "https://identityagent.de/userinfo"
        }

    _ionos_auth_openid_config = \
        {
            "issuer": "https://auth.ionos.com/",
            "authorization_endpoint": "https://auth.ionos.com/1.0/oauth/auth/init",
            "token_endpoint": "https://auth.ionos.com/1.0/oauth/token",
            "userinfo_endpoint": "https://auth.ionos.com/1.0/userinfo",
            "jwks_uri": "https://auth.ionos.com/1.0/oauth/.well-known/jwks.json",
            "scopes_supported": ["openid", "profile", "email", "phone", "address"],
            "response_types_supported": ["code"],
            "grant_types_supported": ["authorization_code"],
            "id_token_signing_alg_values_supported": ["RS512"],
            "token_endpoint_auth_methods_supported": ["client_secret_basic", "client_secret_post"],
            "display_values_supported": ["page"],
            "service_documentation": "https://auth.ionos.com/1.0/doc/",
            "ui_locales_supported": ["de-DE", "en-GB", "en-US", "en-CA", "fr-FR", "it-IT", "es-ES", "es-MX"]
        }

    _agentnoschemainurl_openid_config = \
        {
            "claims_supported": [
                "name",
                "given_name",
                "family_name",
                "middle_name",
                "nickname",
                "preferred_username",
                "profile",
                "picture",
                "website",
                "email",
                "email_verified",
                "gender",
                "birthdate",
                "zoneinfo",
                "locale",
                "phone_number",
                "phone_number_verified",
                "address"
            ],
            "id_token_signing_alg_values_supported": [
                "RS256"
            ],
            "issuer": "https://identityagent.de",
            "jwks_uri": "https://agentnoschemainurl.de/jwks.json",
            "response_types_supported": [
                "code"
            ],
            "userinfo_endpoint": "https://identityagent.de/userinfo"
        }


    _openid_configs = \
        {
            'id.test.denic.de': _denic_openid_config,
            'https://id.test.denic.de': _denic_openid_config,
            'https://identityagent.de': _identityagent_openid_config,
            'https://auth.ionos.com': _ionos_auth_openid_config,
            'https://agentnoschemainurl.de': _agentnoschemainurl_openid_config,
        }

    _denic_registration = \
        {"grant_types": ["authorization_code"], "subject_type": "public", "application_type": "native",
         "logo_uri": "http://localhost:8090/logo.png",
         "registration_client_uri": "https://id.test.denic.de/clients/bpwlftz52dw4u",
         "redirect_uris": ["http://localhost:8090/validate"],
         "registration_access_token": "kTCg1JzdfZfVCyZ336nedgi2zTkg4aFd76SYm-a0zq0.YSxpLHI",
         "token_endpoint_auth_method": "client_secret_basic", "userinfo_signed_response_alg": "RS256",
         "client_id": "bpwlftz52dw4u", "client_secret_expires_at": 0, "tos_uri": "http://localhost:8090/documents",
         "client_id_issued_at": 1558991578, "client_secret": "fP4I8yzCkRuMQAEVfwpg0FX8Y5QG5NGgsveUV6UbHZI",
         "tls_client_certificate_bound_access_tokens": False, "client_name": "Test", "response_types": ["code"],
         "policy_uri": "http://localhost:8090/about", "id_token_signed_response_alg": "RS256"}

    _entity_keys = \
        {
            'https://id.test.denic.de/jwks.json': JWKSet.from_json('{"keys":[{"kty":"RSA","e":"AQAB","use":"sig","kid":"FOvy","n":"qJrsb07wfa77Big8kUOB3VdkQC3sH1hdOW5eA2vpMGz0-Ltzfvm6KudThn0QsGbGK5if5mJJIKSZ7C-y-Y7bZp3fvsIjwhWp03C2wAC_QJjzxzT91NsxIemhGJ5p3D-AR5uFtiXsObo2_AM9N0_n6G0XkRtjG57rXQjvbphwLSSYn7aaxUV0Iqo-584BshTmKhjkPHGzuIrJlt3wS44Ow9gLb0mWFBRN1hvcjyi4NJp20Rezp0rMGFB5ly-sy_b0yC6lTkhioFCh26o1x-_JsHPZnhNofuL5yUIBa4X-WmXruJxodlUPr-rwa8uuRYzLIuCnlIUXX33tRdnCpGRFMw"},{"kty":"EC","use":"sig","crv":"P-256","kid":"6QMM","x":"p_k_rQFFo2BDXM97VIiPjXxqvr5EeqFDYn-oCLRvx9c","y":"4xQPoUFwaHzoAJMpOo95xW6Aq9ivZJv0uSr8miXIpdw"},{"kty":"EC","use":"sig","crv":"P-384","kid":"XpWw","x":"-PbE7JcDtHJgnWA5TO5E0CMDNMDaKSlzE1BI7RF4o29UqGHe7fJSQUkutARiEoVR","y":"5nYp4ZCopr-s-JbL6SyuCVQRu8EdvIaZjbVj5lQphV2In6JQzK0j4_3dR3iK9-LG"},{"kty":"EC","use":"sig","crv":"P-521","kid":"JOgJ","x":"AIycl7HpLJkOIkAPR5HHCf8I0RewWBuIsfQN-JK3GQLSeCZYkh2R1XIOQUKf-wizB1lM8T0UEAyVX_PeYTpF9WsQ","y":"AJamNsgDzc2r5h0MWaqziIOOToRyXP6yxPeTPT3X-9oTf59G3ccHumHsiNxvtlyAETtneZ4xr2_pK_U7_FQNRWAN"},{"kty":"RSA","e":"AQAB","use":"sig","kid":"9wzy","n":"jl9O56nWJcjWlMRKroBdX-ZGhllv6a5dlYwhZZdSz9MBhKbNOBt8mXCr0I8YilLoJXHdwFkum8ZoJGsgC5NOPZa_UNj5w-WSkmt3aAgPc4Soqm9o9z459Ph-QsCpIbwGo-8cZ_mvABm5DaLJLlgmE6VMebwhR_BfVXFyQw7Z4T0a-31bfX8h3Ih3eoTfaDJFjJFPyCj_N1HqRyNRAhvOzzNmfDRZwTaG0Z1GZWkRRYrRcQkR52BMoMy5sX7RDYqEob9xqGx2gi5xgQCfYXY3VMr58A7f2NqyxQ9dci4w3bxvbGBGAE6wFOdH4YDrLWO5n3nszu6KfgAtRVVzLqzobQ"},{"kty":"EC","use":"sig","crv":"P-256","kid":"3uVT","x":"XM3HiZAIU-vUh1aLQ8-uoTx5aD3NJm8NFxtk8JkfB7A","y":"20H7gDmnPQKzODB9o9M9-AG19KR17sVWcH405oHIoqo"},{"kty":"EC","use":"sig","crv":"P-384","kid":"Jqkp","x":"tyM3N93RMQzYulRkp_1u_0lLBCHPtj10DOnJ2g4LQACedaxa3t4vf41O4bUkD55T","y":"X_gsjC_tHiQnO2YjEwjKk0B6XX2I3lCpM_ZKKV4Pbjqx70JiAagn7Ne9wvXugWxH"},{"kty":"EC","use":"sig","crv":"P-521","kid":"eHj7","x":"AcinoGwEavdO_Hl2afwUHFmcUTlnRp0pd28YS5uZ7gm6Ct2h7fWUwnym9XO21NVhuRBccCpW9vssut3uKmNaffsS","y":"AeHIUFf8g3aBrP69mMHK5tC8D6HkolJACIoyJzOTzoE_WaMvbiS4d2lBy3xf7d8HAUE_stuhaBO2NOgGhw3CZMzU"},{"kty":"RSA","e":"AQAB","use":"sig","kid":"CXup","n":"hrwD-lc-IwzwidCANmy4qsiZk11yp9kHykOuP0yOnwi36VomYTQVEzZXgh2sDJpGgAutdQudgwLoV8tVSsTG9SQHgJjH9Pd_9V4Ab6PANyZNG6DSeiq1QfiFlEP6Obt0JbRB3W7X2vkxOVaNoWrYskZodxU2V0ogeVL_LkcCGAyNu2jdx3j0DjJatNVk7ystNxb9RfHhJGgpiIkO5S3QiSIVhbBKaJHcZHPF1vq9g0JMGuUCI-OTSVg6XBkTLEGw1C_R73WD_oVEBfdXbXnLukoLHBS11p3OxU7f4rfxA_f_72_UwmWGJnsqS3iahbms3FkvqoL9x_Vj3GhuJSf97Q"},{"kty":"EC","use":"sig","crv":"P-256","kid":"yGvt","x":"pvgdqM3RCshljmuCF1D2Ez1w5ei5k7-bpimWLPNeEHI","y":"JSmUhbUTqiFclVLEdw6dz038F7Whw4URobjXbAReDuM"},{"kty":"EC","use":"sig","crv":"P-384","kid":"9nHY","x":"JPKhjhE0Bj579Mgj3Cn3ERGA8fKVYoGOaV9BPKhtnEobphf8w4GSeigMesL-038W","y":"UbJa1QRX7fo9LxSlh7FOH5ABT5lEtiQeQUcX9BW0bpJFlEVGqwec80tYLdOIl59M"},{"kty":"EC","use":"sig","crv":"P-521","kid":"tVzS","x":"AZgkRHlIyNQJlPIwTWdHqouw41k9dS3GJO04BDEnJnd_Dd1owlCn9SMXA-JuXINn4slwbG4wcECbctXb2cvdGtmn","y":"AdBC6N9lpupzfzcIY3JLIuc8y8MnzV-ItmzHQcC5lYWMTbuM9NU_FlvINeVo8g6i4YZms2xFB-B0VVdaoF9kUswC"}]}'),
            'https://identityagent.de/jwks.json': JWKSet.from_json('{"keys": [{"e": "AQAB", "kid": "keyId", "kty": "RSA", "n": "rx3-cQZDJeASwj4uinuzBVbRRg2Kmhk8et5eDDmZvEEZcIhdnlFjUrNKNg4ztx41BRNB_NQD-kyroI8R6tGBIBBG5Tlb6Q1hZ67WK5I5eAMKw1BSbAcaOXkTnEcjORnSAj4QLGXlaPZ5sLiRE-_U4OEwWDzB6Wkf_yDjIj1UOypjdCbWk9-lckfyaS-nXrIMzrL6ZaNKvj4mOuwSHj5h47UdY3NmGm7J6QDqcyBpl_2Lgv3fxhn2aw375gY7QopMdkjpDvZJWYbgGlk5IWyH-btKwEeH2KDuAPweGPSMaTGvsu0CkSd9v6QdPf53niku1K-Omuas85i9OXCFeAa-UQ", "use": "sig"}]}'),
            'https://agentnoschemainurl.de/jwks.json': JWKSet.from_json("{   \"keys\":[      {         \"kid\":\"0d581143-aa56-4130-a371-86776e1cd338\",         \"kty\":\"RSA\",         \"d\":\"M9gzMLK830330N8jdeyHCoMvfPEslN8Nrz8jSpIduh8ACH24R5rBJ2nQjfivq7LNlEJ7n2oZMgPU28u9e8ImNbrkN5ZQSzfXZQPjbJe2zZLfijJr9hRnJuwflU0H0HXKm1T9z0FK-lBQ-XYJg6fKQKzJNBWn_hNUojIM3ZcTj5gaAaN6avad8VG7s4RS5kvC7dFUb0yVVs86q8GCDobGWfOaxf74iNcIdKyvjTKAKCw95E3p614GuKbAV9NCmmt0YQkfRey-_hCLPcK711KNMyze326e3lbFI-mVkVgub8Fs0ztqo4uD2SnsGBTI0_xzHqYY1mMcmGaeNHuJLtoa8Q\",         \"dp\":\"bsfO1sfNUc06WSCiBIUoOtHN9MKmzdXN7c5yIpOtpIjTuUSbjcaY8fvyTyirONbaqWvCP3kDhxsump65t36wMYDOEcxHjjUbI6KB9E0quJV4utwMNqpoxLnXI1nSNOjjNiaUPa5nnEJpa8YkCNQOx0pnzDBc4fF9XKi2nH7pwSU\",         \"dq\":\"N8-P3PBdsgdDUalDoNv0muWSTwkhTr43MhckHDGZTHTfLz5d4_GG7xSdZ5ZzZMfZ67OJnLyJgSYVr77ok1rUvfLWrqGVvEIACgdAWPst8NGJFNnoveckY1RirTBVs-sOTqmB86KhR3gb7tTugFFZ5O8ErQMB6TanvA5q2IOC55s\",         \"e\":\"AQAB\",         \"n\":\"zD1qUPjiTRu_OmXgMX0XUFx72NzA4LmvF8Wvwsay0NyEVOiu5KfOTF8pLQO37VNsoIgnbFw3Vo9HVaE6SIELwTuNTYjUwGrli5d9RK4f90kE8NOXfYNAIMv26XsfqUvRuq9SPSuNcX5L3Ggxo1IkM1IUU3LcDHwiD_vMyDta5MuWUlWAxRcPTAo50lZCbB7HSDThn1rBjfNbeA0KVEzrv5tLirqdSgNOh4h7igPN8YU-ZMmaLKedKoUvjghJxqB89JzrftR6oqUaR71zds1RK1Q8l5w996-gIQaKMVX2vuj6FflxGn5lWJIHDVF6xVX99VtP1gzSMpCmCvnUVHBRAQ\",         \"p\":\"9aBT9tefoo2f_Ohq4s40wyGYBYxa-oyDVMJZ1xEZPWSOQ_BzuxPba9YeRVkSh7extR8FRsnBrUtQIRim5e8gM_lp-v87c_DoMFeguCiYEQvgeiA-zEhDEBR5KNYCCHH6nzQbi0YCRmEEXeF6KYnlY1EKiW7i-J_FXeKdo7jlV5c\",         \"q\":\"1N2f2lcTcsbqEX_c3w2nZZC6AFHEjaW26awYTeofG9kFM4bsHX2Ar9rC2Esh8S4Af_g2eQmGEQIGE3dbMoMEtGJW0fuQ01DMd3mXgXmweNkwJIQVZDC45ZEYl0s8TDtUN3qjZLUPPho9_dZ0BJYmUrMxVEaaQwWKnGBYx2BW7yc\",         \"qi\":\"CgzzpEX_q_ke_S-3YtllJpO7iVfmi2YxJ68eaS8PtnGGk0lGeye0X7B-QxOKFm8bn8i4qBAe85gE9Wy_U7pLykHcEeJMZRQWGGu1v_ydtMFw5hXOO2_5XKjD8w8jkRVsB_iWpcfQehP7iOMj9MpozojQ0zOv_3mKtYJClnG5ZRI\"      }   ]}")
        }

    def test__get_domain_name_to_lookup(self):
        encoded = ID4meClient._get_domain_name_to_lookup('john.doe.example.com')
        expected = 'john.doe.example.com'
        assert encoded == expected, \
            'No match for E-mail address alike ID, was: {}, expected: {}'.format(encoded, expected)

        encoded = ID4meClient._get_domain_name_to_lookup('hugh@example.com')
        expected = 'c93f1e400f26708f98cb19d936620da35eec8f72e57f9eec01c1afd6.example.com'
        assert encoded == expected,\
            'No match for E-mail address alike ID, was: {}, expected: {}'.format(encoded, expected)

        try:
            ID4meClient._get_domain_name_to_lookup('john@doe@example.com')
            assert False, 'No exception with invalid identifier'
        except ID4meInvalidIDException:
            pass

    def test__decode_token(self):
        # noinspection PyPep8
        access_token = 'eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJyc0xYM1hwb0RIbkd4WHQrekZsNVZTOWMwdmE1RUJkdTQ4b3BHY2FITVI0dmFGZHBMdnE0Yys3XC9aY0FIdENzWSIsImlkNG1lLmlkZW50aWZpZXIiOiJqb2huLmRvZS5kb21haW5pZC5jb21tdW5pdHkiLCJpZGVudGlmaWVyIjoiam9obi5kb2UuZG9tYWluaWQuY29tbXVuaXR5IiwiaWQ0bWUiOiJqb2huLmRvZS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiZW1haWxfdmVyaWZpZWQiLCJuaWNrbmFtZSIsImdpdmVuX25hbWUiLCJlbWFpbCIsInBpY3R1cmUiXSwic2NvcGUiOlsib3BlbmlkIl0sImlzcyI6Imh0dHBzOlwvXC9pZC50ZXN0LmRlbmljLmRlIiwiZXhwIjoxNTUyMzE0ODY2LCJpYXQiOjE1NTIzMTQyNjYsImp0aSI6IjJveS1EVGNJNjZJIiwiY2xpZW50X2lkIjoiM3ptY2U1YmNzYnh2bSJ9.cETdnolV8fV5ovyZvAj51Oho7zcqXn7RDy3AZTSmDscNsdSlwY_6czKz7hmQJtLFH-T8S3Jj2Acz_3FPGhcH8fWOOtNf5femWK-4BMuCz7KewtKy2yCKza1vubgk6-nMrIo-46zKLiwFfroBMhMr-KgEkg2RJSJOoSoN9Nt-OiIMjekYQaK9U_XamFQXcQ6tsSeCyhtrpImTXU985T-uCRCiurUNR6pDcq5agcofuAMggZ_ZSUigf01WNGPh59HUXAUO2efcPBGug9AXmYpPkWz4A0lELd4hbEZuNTZVqwJy3ptoLSycaMiLM5OO0wG19Bbtr3yZ5K0VHA_Xoyqbzg'
        # noinspection PyPep8
        id_token = 'eyJraWQiOiI5NjY0YTkzNS1jNDlkLTQ5MTQtOTYzYS0xMTBhYmE1ZDYyMDIiLCJjdHkiOiJKV1QiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiUlNBLU9BRVAtMjU2In0.PmOUU57YXm_qWGFMYmnQ-bnK50aD7bnYPlQAOLodvrLvoLbZA6O02PM8PKBo-tFmLGR6KAUvHw_7gUqm-ynWfcerqlzM6Z0D1eQColUxMmk1F3rnW9Jw9iZx_B11YivTASrU7IaJ_5p6_U3y041K36HDPEg4ma-95LF6TNj7FYPH0_NrxsBO7gMXGMKY67KUuvkFEu-X2seV-xrp8bBQ9E9sgKMpRLKVQCkZmiznMXx2TLam354tnh7c7Q6mZnmmV-Nue1F0RbN7YS1QbDGErH17NVVEHft27SwCq_kLOzgsRNgRR5fyHGwFV1beSpk3iQDNoR6lam2lwl0ndj76Jg.FSahbkHluSfyD-ALtD3g4g.NsfVoQfHYTPsXtd1FrEzmm05uyqp0UbfBUao88s2maaf6t3t_uHaL3SoVFS8rs4UCk9VqSCAYGgUJPEhwpJkVkywCEHgBalCLS6lgxvv9wLESL4jMeWm9sMdUrCVl7S8TBwpU9HFiBdvOmw2iOeG1AZ8NzP5ohCPkdNhbrgfmv5C0sHArJjx5Gi0hMshWrYOG6rHsch2t_b0QNmDFOtrPyMzbEPFD7W9GbONUaCzv1K-YADxzhDHH_uUGajlj5qTSb5kIPXOhzmMI4FbRvqBO-76UYmcgZTyw71d6vRNG6S6uHuwOWfCCQSTNi3EeBeimRjN_tX1J2Tw_WOAZW8o_UgdcPYunEjfnnqo8yOeYY75XGzHHDiMj5zhXuWe_ZITRTDt8OoUUkA91Zhr1OLyVwg1-vk9E0J4NhuXFCT7ND4_r21CCiZ46g6ofQHr9Z2UR6j-oR7dS75d0Z6iju57zHbJScXSkVgb9AgtmIFGaXdP3xk7Osc1f34_A3gnsmJDfsakXsMgHh335G-OLvpoIn7eeWFMxdh-yODp81OX6NLFbtmZG0DweG1o8EIkvtm1vYcLDQCAH4TUQeqQHkVA18_5IiyIarG8XMX8Oh1QURpgRFXJjleRebg5ULlOzBhWtqH_UT6kh9jN5WJi7aaV82q7bCK8cj2sEqDsQua-aaejL_MhNAjEXStD7eJjtJJt_1aZRVRV4jV-IxjvRsrVxAheMW4nAed91BebP1EOh2Gr_6JzKyaYl0GQZISoDses0f3bHknc7Ahq3NlvWv9jD8uGBHGatM73ibOjrcTSVyq57XUp8l1JbNEFqU2K_WTcxlG-OKthKexUirlQXPxg9T7I6I_2jJfiVXW5e6Nh8pcv0nV6VIjOIqnGbWZSjeQthL7aRWnh1b4zPCrNnqp9ZoUO7_Mmb8WkV0qIupgR8j3FNrdDTK8SpE36ghOoTjGL.eN9PXvRIYhqcZA1tCS5sqg'
        # noinspection PyPep8
        user_info_auth = 'eyJraWQiOiI5NjY0YTkzNS1jNDlkLTQ5MTQtOTYzYS0xMTBhYmE1ZDYyMDIiLCJjdHkiOiJKV1QiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiUlNBLU9BRVAtMjU2In0.R8FW2ENyg-YEC1eY1fXJfQCQQoPvqTzKDtV1WOJEQo-mLuYvbleYrNRx6DamC8cV1YPRvGQ9UwK_as8ZpYUKYAJ4fgulojisKwaeAWcjpIuiM4FPt3yrYp2ufmqZnqdSDlu62--g_UYIH1CLYc73YGARAkiGbxCT9mw6PhwglH3ppZfuBEHrUZkYa8-McW-gbcih59ef2aagh_auUjoLIBAUCfWIzqZjIO74MUM1CgpCJVigw8fiEIqIWltYxupgpmC7AjeIBNlwZf8s370Sb0IrwmXG8d9ufA1mVc4Vb03FglVDtazu0KSl_R9iurQtdYEmcyUgzVDM1M2UOFPGlQ.mlHvD2zl3SmCP3zgsAbROg._DQoFrv2ds4s0QWwnPLZs-0g3vTRXhrZbZbR4otBLyuedUCRkxkaOtdNxT3exll2izbkdajP0mGgXPFc4a7K8F99oKNZQGAmXwmDxT8aiahRaKBF636xu8uOPzXUDRlGfCLuLre58ISzMBTANXY08GFXbveLZ0wnl6H4y8rMPbw8NesdjPDbwVJZzRLhJKsPyDHFn1i5Wf8RC---2w_1DJtQijRmShU1F2EZiSyRTl9v2VxYz0fk4J6JbyXmcSmNJztISiTrMi2SOM40rjra7MoasasVGbiNfoh9OKSi2OZs2gGHPkbMC7UXC9f6udpI4yXD5xIK9WSnmy1WXtt8VuYwC0A6TX9_g9Pu9_riP_kWGxUa7tnPAJk3AjtSXVES6t-PofnCsi-FSUZhdH1OGHcBQvVkhLYnmQ3URFMLMsN9HiWMgBRZ8-zspe8hAnRmQ-t5kgdrYP7o6f4pnDp5ITjgALu4q4uRQa7cunEYl1MJWtuc5MzIL12-IIc9w0yPEfN7p5tIih1eAFY90TBwJ9dtwJctJix2gYCf-zf1enWbsJrWaRtXc7saZ5cTVQiGbKL-hG5I_rNHlNJoskiRa2xYx-_oDco-of_TjkJAYSocCvTdahw1Yh3esWckAjIvxMexjRLX2_JOFfbHBmG6yq_e55vxN3RJIfAsz73OiGU9PbSm_REio-ZxuizPc5MWqFUGE4PopJgcVAz_pJeYTfdSmRYiakTNtPcvJ211cJgurhMOYhPXZ-2tVVtda6cUUaAaI4IOLjGHOC_k0NbZjz6d_3O6Qvc27Iquej3KEs_85jK3hGFnzUOqorx1JEkr75VwuauQQ6nBbHp7Pg3Z9NrXsuyJBbRw3zoXNMplWokaxxXcycKYpmKLxlq8rqhQ29BixVk2hECk3hF39xMF64twQFojqCM4CLT_k2Q9l-Nbp8Mqyk5uRzjNzHe2-dg7jjbFqx2IwJSgcPUj9vsfB2MpgEQ8gUD9_xe2_diMlPUfWg1XvW_B7o4bVHqAwlRRUar5fwuD7A2fvwZDfc5i3YtdUZQ82TIyKxiVN_cjql2pGSWVrY5FRNePprQWEkFq8_6AaFaC6Q8oWzSvbycgTd4iy64o_bFrrTlkne82ucDWWFyA2wOtqou7XKiDS9y55anVRY4dDoECGRB7O8yYuOSVGeaJqVbWwV2chM55KNQ5diyuB-4DlL7el8f07ru4ig2m5hLb6-zX58m1wHtS9s3QICUrmzOMkr5nK93hXF1zcF5lEplRwiALNQ8njwNi6m6I64hkX2fzM3v7RCYwLXbemj69gdejYDCIADu4YHYd9q7aIQ-v5QdlA7FdUm7WCYETWXERQYkpaAwvVn25CNmuHFRYPdb-O2CGxPsl_oIX_BNBcedgl7TKallCmCqwiOBrD4obsmIB1JF1IvaQVXeU0aBNn79-TnUL-9Wl4FfjpFCMRiRXgCxiR8-Lbc-smzUsB9egaPN3AvkmSZ0JIcVbOJbybBEpqmxZkBFHwO7VmlMsc8V8fPiK50UuWsvBczagQlvjteNyBAN6NLwF_xgOs4wEGzbqiPeSJr8o_fBw8At9yNuvKN8oeD9eHlzQo_Kqt-8XupnaTJO4UpQj8FjhQvDv7eJI0DuMiBg3NPENwygsK0FvotfacUAZBuzKAFJifRMMlGY91MWdyYTCDPC1MMmWOPUSx6G9aKMDB87uNJNwMvB6X_fk0hLebixUAY2HtwuAB04Q-8BlUCz453oMqn6WkVykXVAkl6k61cCRWZHPj-BGaSHVFKmixdt8ozfVsZEv1arn9kU-wCSmTpuM68IWFINkU4P1MAenlsuK1-ar6x6G4aSfidUjozB-qgptEIOjduHf6oUAmJmKqqsNB-BxREZxbUvaOa56JfrZ3SneMzOakB4LgEdvkc97l_kt3oud3GxlfTyIKYwWCJ-VPRbHzNhSuc1eG7xMzMyTiLXRnh3BXCgFjI34rX0zGcOQkSIAPUeyOu_G8p4eeDU5YZ82Y23hYM94AaoofVnGUm-D7q5Bi-exx-CI3xLFQxLD1YAX4YInEHxkwtqRJCGHTsQqAL0tWSa8tyd4TJJjmRi6rmt3lElkXyRtdsspPA4_QPzvVJ62E-_pJodhNIiimASC2xxqF-hOSmQMN1AsiTKhWa4ZW165juBuAdo1UVhT2LwhlKJdJ0RA2hPBzSdMlkNUDg2spm2ltgD4ysR2ttDK0eVcv_bRyufNxt-n-MRic_ihE_7ecKpDPW91dwVpVz0baYYbxTQgD4xJCOcqfZ7Ehwc6uiDxCjR9m0s5AuL2bvHOsq9rfJFv_JGOVsQh3n-4ZPsh53-Et0-RFQutLQrZAl5wIB_brLytGEVtY2fzq3X942D458CXs5RAy2uBIy-zPiLxQRkNP24jF-BeSanhvW05tCRyopsB4DQS-pLUsMCGwgnsH4o0wHupUWRcF5f9bxDa_xrGWWaHL082Raxf-BvYHO-UCW8ZVRxI4KrB-QrtDL7dySt00xqk1OrCU9MBWERvD3UuZDMD5vUJIvzHj0XHn8xmIsSFpH-lvB0keDwMQvF2ZpXJMkeFC_s_6RnXCZjiKRAo1fqNIVfgTYuna5H3I2nag-BzW94Mqb8xbtCIyIW6eXa4siCaSp3Xkt_bjPwG_U-885HaFGZ7jTppSRuI0NHJXIeBq4isjrQieYjvOlBV1Am2t9Br7CO9Bul2PkZ52VLDE0abcCjQqkTW_nNALZNMBt-Akzp5eSugnLsidcBRQfb2nEIjAhQ5AKELCHcBfd0mJznn1fwQX3jPLpA96lEHp7FX68B55gmITbf18VtlCtmHcRG2Nx4DFNnSSbcyaV8KZDQqHiuC29HRE_Qu0D6m1btV4ccEc_MpDFEFI0kzJsRTIiFl8TFa770K4NiL9jTGpmtKypwa2rv_GnuU-jaL-0iH7L-mpZo1SCb83j0DVXGi437s_ivea723Ij-cfZAcQ99cTnPT-S3ioKGX4G8ChsflHYsUK_RulHFBBgko2g4j3XiSg-ev1Xjp-iPuxkn4V6hfytm4n-HbKsRzgLGkgEJmX1ohxA-d_3lqfbgdOKcvpxdaWPsRbz7qHLPZwMJc_bLRPpU7y6fJdtVaA-k_Blv_qcPZ.7uw13sPnKQEOImajhsLZwg'
        # noinspection PyPep8
        user_info_agent = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJyc0xYM1hwb0RIbkd4WHQrekZsNVZTOWMwdmE1RUJkdTQ4b3BHY2FITVI0dmFGZHBMdnE0Yys3L1pjQUh0Q3NZIiwiaWQ0bWUuaWRlbnRpZmllciI6ImpvaG4uZG9lLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lLmlkZW50aXR5Ijoiam9obi5kb2UuZG9tYWluaWQuY29tbXVuaXR5IiwiZXhwIjoxNTUyMzE2NTk5LCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaWF0IjoxNTUyMzE2Mjk5LCJnaXZlbl9uYW1lIjoiR2l2ZW5OYW1lIiwidXBkYXRlZF9hdCI6MTU1MjMxNjI5MCwiaXNzIjoiaHR0cHM6Ly9pZGVudGl0eWFnZW50LmRlIiwibmJmIjoxNTUyMzE2Mjk5LCJlbWFpbCI6Im1haWxAdGVzdC5kZSIsIm5pY2tuYW1lIjoiTmlja25hbWUiLCJhdWQiOiIzem1jZTViY3NieHZtIiwicGljdHVyZSI6Imh0dHA6Ly9waWN0dXJlLmNvbS9waWN0dXJlIn0.G6WaEjieFb8xndfHYMpZ8cXqT5eAUATPm4drwlNolsav7Va-Jto-KUnmmNG4glJUcwHyK8-HkY7MbjKwvtBPQbj3y8r4GcSNlE3l1tE6WGuf1SUu0S8AOhQ-p_cvutxIjju0VHg3XvtfPG5yFnb8GXmuoC89Y6SmhXaI_gb5PUufLA0fJcNlO8bQqsK74mUxAh1aI_OlZ4TMOLNTo6a80w6xGvqFSPIZs5ok7ErxLb8Ku8lLBKfdwCS_hXpii0IoDwT9OJUmAuL9iB88y-O0omIv7qqoHOFXuh1n8N84c1qFwK3YApzSQObmsN88s9_m2pFj4EsYNZIOGelQ4JEx9A'

        # noinspection PyPep8
        private_jwks_json = '{"keys":[{"d":"LiY9nb5v4NW8-iBtU3Z_Bx-PNFzX57qeaEqh2AZuuvFLZpBNvKhELJ0_Re308LweyCYVJHaP6MUrVs9i2PwGTtNxxuZ835K09T_9wc5OcNIjzI5WxLc8j1Wwd65v1QlndmXh4MS9rPjPC3us4modamFcjf5I0Lg9o-0miCt77SYROl_ZuHCX8QVCVrySU4UkU9xpZONpkDn25CpwnrftRhO1hP60HIiXWA6vQvYYHpRX9f5wuqIshcoMzXccSHf_TMY9UEAsxpvIqO98eRCxWNF75RTBynddqoSWVBnP_JwCUWOG3ZtwS7EGYd5HncNKdramrh0XK-JOzS6xb7UyzQ","dp":"I6knDqeSJTdbYw-OfXM2KmqgUZ-z6QaVqpLDPyNeTfJJTwklH1i9T5i_6t849fOKBOjrYhZ-Poz3t1tVJ7n77_colyr-i7o3irMoVnMI-m8EalwVGV61jJ7BlAruhnvrINNrGauUJ9E_2kkcFaJtlQy3LPN2r8e0oX2qH9NANkk","dq":"DfKB5JouG30jiVnFJljMwMKTJw47o4bSJ5BGqZIxfNjVgtQir2-_Ekzh4F_kVMxFIDhSc-gKBFtDw1SMkbfZUZl8gdhZe9ejvhXuh08NuTKDbdayuWNCv4gGd7kwTIvjjI545uaLsc5fql7-D9Ai6gF40lfzZZp9If0yUve8CAM","e":"AQAB","kid":"9664a935-c49d-4914-963a-110aba5d6202","kty":"RSA","n":"saZoYnYQBKfWQY0Uy0stB3tpRVXCJce0YQhRPPpfYizQhueo-2I_UVrdenZhzyWKFg6LfR2z_pKcDPj5A3TepXXSWo4X8HnOKu3V9E1pplwfzE5NEGADd4-Fvfjs6RZtX8JLIhj3teBVO4LXBryHGOW-s24_DIqdItoZbpCvRwZaHskZG0ruWIX27hSCNQyxygLNRmbWRBYo0sHloOFPV2Yl3aM5N2VOTEfhkJr4346GHOGDmrDgirC3DdjpEqBmk1cQXP2T3J4EmMpZOsn-RiuRsjvBHIyRr9mooKYL2ekAmr8CD5PVeSslntf_F-_w4ATv4Ld2MLsXS7mrl41DBQ","p":"5FJM0z-LeWKNtz7DZsheYKhViieNxgor2JhnsCJZd8L1IPlmwJ8tueyUjG7wbFrVsdfKp4Mo15CDXAMdMBjmvSa6oY1_hJIjiWvRbAbJ0MgMo60nc6Pv4H7N807Lg9AFhVDUaQIL47ldOYvp9cJBnNGWZoGKop-SllDsB4mN_m8","q":"xy-TgNAeqeF8mLPE37KE99-jtcVaMR3wZsfytumwZW-lm8oVgmUcnt_u4157_ni3OidPgdcpItNvGi5L4h9MOmWJI7Hc3aSZnj7ZTFjeQ4OFg7eCmUxENtMXseGK1eRyfzE6e-y7PGtp3eVbz-yD_yED81LwTKSbx6zJD3S0D8s","qi":"tKkmN7wcmG63hAQAoq9oD0KYXjv6V5bnrr5NBHHWtO_fqHpofqhHizYemmuQnvad8I7kg6h1iN_abhxMaE5gtUi-kzdRZy1hx_KU3w5XO2F6h60WKfQEuXtIkPFaGjtLPhzGTDDaisOsOYltKQOIuUkjaO3ZBatzGSi9BLLeqT8"}]}'

        registration = {
            'client_id': u'3zmce5bcsbxvm',
            'client_secret': u'9KpB_rg-VZp7_s1di4zABocV22hCdfzKJxGuMpvfolk',
        }

        ctx = ID4meContext(
            id4me='john.doe.domainid.community',
            identity_authority='id.test.denic.de',
            issuer='https://id.test.denic.de'
        )
        ctx.access_token = access_token

        client = TestID4meClientTestCommon._get_test_client(private_jwks_json=private_jwks_json, get_client_registration=None)
        client._get_openid_configuration = MagicMock(return_value={ 'jwks_uri': 'https://id.test.denic.de/jwks.json'})
        client._get_public_keys_set = MagicMock(return_value=JWKSet.from_json('{"keys":[{"kty":"RSA","e":"AQAB","use":"sig","kid":"FOvy","n":"qJrsb07wfa77Big8kUOB3VdkQC3sH1hdOW5eA2vpMGz0-Ltzfvm6KudThn0QsGbGK5if5mJJIKSZ7C-y-Y7bZp3fvsIjwhWp03C2wAC_QJjzxzT91NsxIemhGJ5p3D-AR5uFtiXsObo2_AM9N0_n6G0XkRtjG57rXQjvbphwLSSYn7aaxUV0Iqo-584BshTmKhjkPHGzuIrJlt3wS44Ow9gLb0mWFBRN1hvcjyi4NJp20Rezp0rMGFB5ly-sy_b0yC6lTkhioFCh26o1x-_JsHPZnhNofuL5yUIBa4X-WmXruJxodlUPr-rwa8uuRYzLIuCnlIUXX33tRdnCpGRFMw"},{"kty":"EC","use":"sig","crv":"P-256","kid":"6QMM","x":"p_k_rQFFo2BDXM97VIiPjXxqvr5EeqFDYn-oCLRvx9c","y":"4xQPoUFwaHzoAJMpOo95xW6Aq9ivZJv0uSr8miXIpdw"},{"kty":"EC","use":"sig","crv":"P-384","kid":"XpWw","x":"-PbE7JcDtHJgnWA5TO5E0CMDNMDaKSlzE1BI7RF4o29UqGHe7fJSQUkutARiEoVR","y":"5nYp4ZCopr-s-JbL6SyuCVQRu8EdvIaZjbVj5lQphV2In6JQzK0j4_3dR3iK9-LG"},{"kty":"EC","use":"sig","crv":"P-521","kid":"JOgJ","x":"AIycl7HpLJkOIkAPR5HHCf8I0RewWBuIsfQN-JK3GQLSeCZYkh2R1XIOQUKf-wizB1lM8T0UEAyVX_PeYTpF9WsQ","y":"AJamNsgDzc2r5h0MWaqziIOOToRyXP6yxPeTPT3X-9oTf59G3ccHumHsiNxvtlyAETtneZ4xr2_pK_U7_FQNRWAN"},{"kty":"RSA","e":"AQAB","use":"sig","kid":"9wzy","n":"jl9O56nWJcjWlMRKroBdX-ZGhllv6a5dlYwhZZdSz9MBhKbNOBt8mXCr0I8YilLoJXHdwFkum8ZoJGsgC5NOPZa_UNj5w-WSkmt3aAgPc4Soqm9o9z459Ph-QsCpIbwGo-8cZ_mvABm5DaLJLlgmE6VMebwhR_BfVXFyQw7Z4T0a-31bfX8h3Ih3eoTfaDJFjJFPyCj_N1HqRyNRAhvOzzNmfDRZwTaG0Z1GZWkRRYrRcQkR52BMoMy5sX7RDYqEob9xqGx2gi5xgQCfYXY3VMr58A7f2NqyxQ9dci4w3bxvbGBGAE6wFOdH4YDrLWO5n3nszu6KfgAtRVVzLqzobQ"},{"kty":"EC","use":"sig","crv":"P-256","kid":"3uVT","x":"XM3HiZAIU-vUh1aLQ8-uoTx5aD3NJm8NFxtk8JkfB7A","y":"20H7gDmnPQKzODB9o9M9-AG19KR17sVWcH405oHIoqo"},{"kty":"EC","use":"sig","crv":"P-384","kid":"Jqkp","x":"tyM3N93RMQzYulRkp_1u_0lLBCHPtj10DOnJ2g4LQACedaxa3t4vf41O4bUkD55T","y":"X_gsjC_tHiQnO2YjEwjKk0B6XX2I3lCpM_ZKKV4Pbjqx70JiAagn7Ne9wvXugWxH"},{"kty":"EC","use":"sig","crv":"P-521","kid":"eHj7","x":"AcinoGwEavdO_Hl2afwUHFmcUTlnRp0pd28YS5uZ7gm6Ct2h7fWUwnym9XO21NVhuRBccCpW9vssut3uKmNaffsS","y":"AeHIUFf8g3aBrP69mMHK5tC8D6HkolJACIoyJzOTzoE_WaMvbiS4d2lBy3xf7d8HAUE_stuhaBO2NOgGhw3CZMzU"},{"kty":"RSA","e":"AQAB","use":"sig","kid":"CXup","n":"hrwD-lc-IwzwidCANmy4qsiZk11yp9kHykOuP0yOnwi36VomYTQVEzZXgh2sDJpGgAutdQudgwLoV8tVSsTG9SQHgJjH9Pd_9V4Ab6PANyZNG6DSeiq1QfiFlEP6Obt0JbRB3W7X2vkxOVaNoWrYskZodxU2V0ogeVL_LkcCGAyNu2jdx3j0DjJatNVk7ystNxb9RfHhJGgpiIkO5S3QiSIVhbBKaJHcZHPF1vq9g0JMGuUCI-OTSVg6XBkTLEGw1C_R73WD_oVEBfdXbXnLukoLHBS11p3OxU7f4rfxA_f_72_UwmWGJnsqS3iahbms3FkvqoL9x_Vj3GhuJSf97Q"},{"kty":"EC","use":"sig","crv":"P-256","kid":"yGvt","x":"pvgdqM3RCshljmuCF1D2Ez1w5ei5k7-bpimWLPNeEHI","y":"JSmUhbUTqiFclVLEdw6dz038F7Whw4URobjXbAReDuM"},{"kty":"EC","use":"sig","crv":"P-384","kid":"9nHY","x":"JPKhjhE0Bj579Mgj3Cn3ERGA8fKVYoGOaV9BPKhtnEobphf8w4GSeigMesL-038W","y":"UbJa1QRX7fo9LxSlh7FOH5ABT5lEtiQeQUcX9BW0bpJFlEVGqwec80tYLdOIl59M"},{"kty":"EC","use":"sig","crv":"P-521","kid":"tVzS","x":"AZgkRHlIyNQJlPIwTWdHqouw41k9dS3GJO04BDEnJnd_Dd1owlCn9SMXA-JuXINn4slwbG4wcECbctXb2cvdGtmn","y":"AdBC6N9lpupzfzcIY3JLIuc8y8MnzV-ItmzHQcC5lYWMTbuM9NU_FlvINeVo8g6i4YZms2xFB-B0VVdaoF9kUswC"}]}'))

        claims = client._decode_token(access_token, ctx, registration, ctx.issuer,
                                      TokenDecodeType.Other,
                                      leeway=datetime.timedelta(days=100 * 365),
                                      verify_aud=False)

        # noinspection PyPep8
        claims_ref = {'sub': 'rsLX3XpoDHnGxXt+zFl5VS9c0va5EBdu48opGcaHMR4vaFdpLvq4c+7/ZcAHtCsY',
                      'id4me.identifier': 'john.doe.domainid.community', 'identifier': 'john.doe.domainid.community',
                      'id4me': 'john.doe.domainid.community',
                      'clm': ['email_verified', 'nickname', 'given_name', 'email', 'picture'],
                      'scope': ['openid'], 'iss': 'https://id.test.denic.de', 'exp': 1552314866,
                      'iat': 1552314266, 'jti': '2oy-DTcI66I', 'client_id': '3zmce5bcsbxvm'}

        assert claims is not None, 'Claims empty'
        assert claims == claims_ref, 'Claims content differs'

        claims_id = client._decode_token(id_token, ctx, registration, ctx.issuer, TokenDecodeType.IDToken, leeway=datetime.timedelta(days=100 * 365))

        claims_id_ref = {'sub': 'rsLX3XpoDHnGxXt+zFl5VS9c0va5EBdu48opGcaHMR4vaFdpLvq4c+7/ZcAHtCsY',
                         'aud': '3zmce5bcsbxvm', 'id4me.identifier': 'john.doe.domainid.community',
                         'amr': ['pwd'], 'auth_time': 1552313972, 'iss': 'https://id.test.denic.de',
                         'exp': 1552314886, 'iat': 1552313986}
        assert claims_id is not None, 'Claims empty'
        assert claims_id == claims_id_ref, 'Claims content differs'

        user_info_auth_dec = client._decode_token(user_info_auth, ctx, registration, ctx.issuer, TokenDecodeType.UserInfo, leeway=datetime.timedelta(days=100 * 365))

        user_info_auth_ref = {
            'sub': 'rsLX3XpoDHnGxXt+zFl5VS9c0va5EBdu48opGcaHMR4vaFdpLvq4c+7/ZcAHtCsY',
            'aud': '3zmce5bcsbxvm',
            '_claim_names': {
                'email_verified': '99b10238-33d1-41aa-b6eb-f417cc82277f',
                'nickname': '99b10238-33d1-41aa-b6eb-f417cc82277f',
                'given_name': '99b10238-33d1-41aa-b6eb-f417cc82277f',
                'email': '99b10238-33d1-41aa-b6eb-f417cc82277f',
                'picture': '99b10238-33d1-41aa-b6eb-f417cc82277f'
            },
            '_claim_sources': {
                '99b10238-33d1-41aa-b6eb-f417cc82277f': {
                'access_token': 'eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJyc0xYM1hwb0RIbkd4WHQrekZsNVZTOWMwdmE1RUJkdTQ4b3BHY2FITVI0dmFGZHBMdnE0Yys3XC9aY0FIdENzWSIsImlkNG1lLmlkZW50aWZpZXIiOiJqb2huLmRvZS5kb21haW5pZC5jb21tdW5pdHkiLCJpZGVudGlmaWVyIjoiam9obi5kb2UuZG9tYWluaWQuY29tbXVuaXR5IiwiaWQ0bWUiOiJqb2huLmRvZS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiZW1haWxfdmVyaWZpZWQiLCJuaWNrbmFtZSIsImdpdmVuX25hbWUiLCJlbWFpbCIsInBpY3R1cmUiXSwic2NvcGUiOlsib3BlbmlkIl0sImlzcyI6Imh0dHBzOlwvXC9pZC50ZXN0LmRlbmljLmRlIiwiZXhwIjoxNTUyMzE2MTU3LCJpYXQiOjE1NTIzMTU1NTcsImp0aSI6IklYc0FLX2Vwa21JIiwiY2xpZW50X2lkIjoiM3ptY2U1YmNzYnh2bSJ9.mbQWbsYl__JweiO_ew9xxYMZxOb7f0IAUTp7EBITbEfGTokH5nBPNN0OhWcZr2_LQx7rELR1SUITkAYBtvrT_zyD4tsp1ivC7q5G0Doxt1epxrvPNnFXYqliZGQKlIjpGDiQTg-1DKyQVpfjqLI-K_pHr9ChMILyOQPbDUihKQdBW-aUXY9XLRItxE7pzEDq4Kppx2LTe7-R9e-m2FJuTBFMKHQjzIjh3u0xceWeHZleoTrdU500p5DqMsq53hBLsChfGhK4VJi45oPaamsGr2OS_USi63Zzdz9qyvMGkP0lsTjwdBIpMTknnsk0EeAq9G57mie09yBS6hc9CQXclQ',
                'endpoint': 'https://identityagent.de/userinfo'
                }
            },
            'iss': 'https://id.test.denic.de',
            'iat': 1552315617
        }
        assert user_info_auth_dec is not None, 'Claims empty'
        assert user_info_auth_dec == user_info_auth_ref, 'Claims content differs'


        # noinspection PyPep8
        claims_agent_ref = {
            'sub': 'rsLX3XpoDHnGxXt+zFl5VS9c0va5EBdu48opGcaHMR4vaFdpLvq4c+7/ZcAHtCsY',
            'id4me.identifier': 'john.doe.domainid.community',
            'id4me.identity': 'john.doe.domainid.community',
            'exp': 1552316599,
            'email_verified': True,
            'iat': 1552316299,
            'given_name': 'GivenName',
            'updated_at': 1552316290,
            'iss': 'https://identityagent.de',
            'nbf': 1552316299,
            'email': 'mail@test.de',
            'nickname': 'Nickname',
            'aud': '3zmce5bcsbxvm',
            'picture': 'http://picture.com/picture'
        }

        client._get_openid_configuration = MagicMock(return_value={ 'jwks_uri': 'https://identityagent.de/jwks.json'})
        client._get_public_keys_set = MagicMock(return_value=JWKSet.from_json('{"keys": [{"e": "AQAB", "kid": "keyId", "kty": "RSA", "n": "rx3-cQZDJeASwj4uinuzBVbRRg2Kmhk8et5eDDmZvEEZcIhdnlFjUrNKNg4ztx41BRNB_NQD-kyroI8R6tGBIBBG5Tlb6Q1hZ67WK5I5eAMKw1BSbAcaOXkTnEcjORnSAj4QLGXlaPZ5sLiRE-_U4OEwWDzB6Wkf_yDjIj1UOypjdCbWk9-lckfyaS-nXrIMzrL6ZaNKvj4mOuwSHj5h47UdY3NmGm7J6QDqcyBpl_2Lgv3fxhn2aw375gY7QopMdkjpDvZJWYbgGlk5IWyH-btKwEeH2KDuAPweGPSMaTGvsu0CkSd9v6QdPf53niku1K-Omuas85i9OXCFeAa-UQ", "use": "sig"}]}'))

        claims_agent = client._decode_token(user_info_agent, ctx, registration,
                                            'https://identityagent.de',
                                            TokenDecodeType.UserInfo,
                                            leeway=datetime.timedelta(days=100 * 365))
        assert claims_agent is not None, 'Claims empty'
        assert claims_agent == claims_agent_ref, 'Claims content differs'

        try:
            client._decode_token(id_token, ctx, registration,
                                            'https://id.test.denic.de',
                                            TokenDecodeType.IDToken,
                                            leeway=datetime.timedelta(days=0 * 365))
            assert False, 'No exception on decoding of expired token'
        except ID4meException:
            pass

        try:
            tmpid = registration['client_id']
            registration['client_id'] = 'wrong_id'
            try:
                client._decode_token(id_token, ctx, registration,
                                                'https://id.test.denic.de',
                                                id4me_rp_client.TokenDecodeType.IDToken,
                                                leeway=datetime.timedelta(days=100 * 365))
            finally:
                ctx.client_id = tmpid
            assert False, 'No exception on decoding token with different audience'
        except ID4meException:
            pass

    def test___init__(self):
        netctx = NetworkContext(
            nameservers=['ns1', 'ns2']
        )
        auth_store = dict()

        client = ID4meClient(
            validate_url='https://foo.com/validate',
            client_name='Test client',
            get_client_registration=lambda authname: auth_store[authname]
        )
        assert client is not None
        assert client.validateUrl == 'https://foo.com/validate'
        assert client.jwksUrl is None
        assert client.client_name == 'Test client'
        assert client.logoUrl is None
        assert client.policyUrl is None
        assert client.tosUrl is None
        assert client.private_jwks is None
        assert client.requireencryption is None
        assert client.networkContext is not None
        assert client.app_type == OIDCApplicationType.native
        assert client.get_client_registration is not None
        assert client.save_client_registration is None
        assert client.register_client_dynamically == True

        client = ID4meClient(
            validate_url='https://foo.com/validate',
            jwks_url='https://foo.com/jwks.json',
            client_name='Test client',
            logo_url='https://foo.com/logo.png',
            policy_url='https://foo.com/policy',
            tos_url='https://foo.com/tos',
            private_jwks_json=self._example_jwks_str,
            requireencryption=False,
            network_context=netctx,
            app_type=OIDCApplicationType.web,
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            register_client_dynamically=True
        )

        assert client is not None
        assert client.validateUrl == 'https://foo.com/validate'
        assert client.jwksUrl == 'https://foo.com/jwks.json'
        assert client.client_name == 'Test client'
        assert client.logoUrl == 'https://foo.com/logo.png'
        assert client.policyUrl == 'https://foo.com/policy'
        assert client.tosUrl == 'https://foo.com/tos'
        assert isinstance(client.private_jwks, JWKSet)
        assert client.requireencryption is False
        assert client.networkContext == netctx
        assert client.app_type == OIDCApplicationType.web
        assert client.get_client_registration is not None
        assert client.save_client_registration is not None
        assert client.register_client_dynamically is True

        try:
            client = ID4meClient(
                validate_url='https://foo.com/validate',
                client_name='Test client',
                get_client_registration=lambda authname: auth_store[authname],
                requireencryption=True
            )
            assert False, 'Client created for encryption but without keys'
        except ID4meException:
            pass

    @mock.patch('id4me_rp_client.network._http_request_raw')
    def test__get_openid_configuration(self, network_mock):
        network_mock.return_value = json.dumps(self._denic_openid_config).encode(), 200, 'application/json'
        client = TestID4meClientTestCommon._get_test_client(get_client_registration=None)

        # test issuer with full URL
        client._get_openid_configuration('https://foo.com')
        network_mock.assert_called_with(
            client.networkContext, 'GET',
            'https://foo.com/.well-known/openid-configuration',
            None, None, None, None, None, None, None)

        # test issuer with just a hostname
        client._get_openid_configuration('foo.com')
        network_mock.assert_called_with(
            client.networkContext,  'GET',
            'https://foo.com/.well-known/openid-configuration',
            None, None, None, None, None, None, None)

        # test error case - exception raised by network call
        network_mock.return_value = ''.encode(), 404
        try:
            client._get_openid_configuration('foo.com')
            assert False, "No exception in case: test error case - exception raised by network call"
        except ID4meAuthorityConfigurationException:
            pass


    def test__get_public_keys_set(self):
        with mock.patch('id4me_rp_client.network._http_request_raw') as network_mock:
            network_mock.return_value = self._example_jwks_str.encode(), 200, 'application/json'
            client = TestID4meClientTestCommon._get_test_client(get_client_registration=None)
            keys = client._get_public_keys_set('https://identityagent.de/jwks.json')
            network_mock.assert_called_with(
                client.networkContext,
                'GET',
                'https://identityagent.de/jwks.json',
                None, None, None, None, None, None, None)
            assert isinstance(keys, JWKSet)
            assert len(keys['keys']) == 1, 'Keys array length mismatch'
            assert keys.get_key('keyId') is not None, 'Key ID not there'

        # error case: fetch error
        with mock.patch('id4me_rp_client.network._http_request_raw') as network_mock:
            network_mock.return_value = 'error'.encode(), 500
            client = TestID4meClientTestCommon._get_test_client(get_client_registration=None)
            try:
                client._get_public_keys_set('https://identityagent.de/jwks.json')
                assert False, "Missing exception: error case: fetch error"
            except ID4meAuthorityConfigurationException:
                pass

    def test__get_identity_authority(self):
        client = TestID4meClientTestCommon._get_test_client(get_client_registration=None)
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.return_value = ['v=OID1;iss=test1.auth;clp=api-identity.ionos.com']
        auth = client._get_identity_authority('this-is-sparta.test.adwords.google.com')
        client.resolver.query.assert_called_with('_openid.this-is-sparta.test.adwords.google.com.', 'TXT')
        assert auth == 'test1.auth', 'Wrong Authority discovered'

        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.side_effect = [NXDOMAIN, ['v=OID1;iss=test2.auth;clp=api-identity.ionos.com']]

        auth = client._get_identity_authority('testionos.domainid.community')

        client.resolver.query.assert_has_calls([call('_openid.testionos.domainid.community.', 'TXT'),
                                                call('_openid.domainid.community.', 'TXT')])
        assert auth == 'test2.auth', 'Wrong Authority discovered'

        # error case _openid record not found
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.side_effect = [NXDOMAIN, NXDOMAIN, NXDOMAIN]
        try:
            client._get_identity_authority('testionos.domainid.community')
            assert False, 'Expected exception:  _openid record not found'
        except ID4meDNSResolverException:
            pass
        client.resolver.query.assert_has_calls([call('_openid.testionos.domainid.community.', 'TXT'),
                                                call('_openid.domainid.community.', 'TXT'),
                                                call('_openid.community.', 'TXT')])
        assert client.resolver.query.call_count == 3, 'Too many DNS resolves'

        # error case: no calls at all
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.side_effect = ['v=OID1;iss=test1.auth;clp=api-identity.ionos.com']
        try:
            client._get_identity_authority('')
            assert False, 'Expected exception: no calls at all'
        except ID4meDNSResolverException:
            pass

    def test__get_identity_authority_with_override(self):
        client = TestID4meClientTestCommon._get_test_client(get_client_registration=None)
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.return_value = ['v=OID1;iss=test1.auth;clp=api-identity.ionos.com']
        client.authority_lookup_override = \
            lambda identifier: 'overruled.auth' if identifier.endswith('.google.com') else None

        #with dot identifier
        auth = client._get_identity_authority('this-is-sparta.test.adwords.google.com')
        client.resolver.query.assert_not_called()
        assert auth == 'overruled.auth', 'Wrong Authority discovered'


        #with dot identifier assure not matching IDs still resolve over DNS
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.side_effect = [NXDOMAIN, ['v=OID1;iss=test2.auth;clp=api-identity.ionos.com']]

        auth = client._get_identity_authority('testionos.domainid.community')

        client.resolver.query.assert_has_calls([call('_openid.testionos.domainid.community.', 'TXT'),
                                                call('_openid.domainid.community.', 'TXT')])
        assert auth == 'test2.auth', 'Wrong Authority discovered'


    def test__get_identity_authority_once(self):
        client = TestID4meClientTestCommon._get_test_client(get_client_registration=None)

        # happy case
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.return_value = ['v=OID1;iss=test1.auth;clp=api-identity.ionos.com']
        auth = client._get_identity_authority_once('testionos.domainid.community')
        client.resolver.query.assert_called_with('_openid.testionos.domainid.community.', 'TXT')
        assert auth == 'test1.auth', 'Wrong Authority discovered'

        # happy case with record mix, no clp
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.return_value = \
            ['v=OID2;iss=test2.auth;clp=api-identity.ionos.com',
             'v=OID1;iss=test1.auth',
             'v=spf1 include:spf.acme.com -all']
        auth = client._get_identity_authority_once('testionos.domainid.community')
        client.resolver.query.assert_called_with('_openid.testionos.domainid.community.', 'TXT')
        assert auth == 'test1.auth', 'Wrong Authority discovered'

        # error case - no valid answer
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.return_value = \
            ['v=OID2;iss=test2.auth;clp=api-identity.ionos.com',
             'v=spf1 include:spf.acme.com -all']
        try:
            client._get_identity_authority_once('testionos.domainid.community')
            assert False, "No exception thrown: no valid answer"
        except ID4meDNSResolverException:
            pass

        # error case - empty answer
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.return_value = []
        try:
            client._get_identity_authority_once('testionos.domainid.community')
            assert False, "No exception thrown: empty answer"
        except ID4meDNSResolverException:
            pass

        # error case: Timeout
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.side_effect = [Timeout]
        try:
            client._get_identity_authority_once('testionos.domainid.community')
            assert False, "No exception thrown: Timeout"
        except ID4meDNSResolverException:
            pass

        # error case: NXDOMAIN
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.side_effect = [NXDOMAIN]
        try:
            client._get_identity_authority_once('testionos.domainid.community')
            assert False, "No exception thrown: NXDOMAIN"
        except ID4meDNSResolverException:
            pass

        # error case: YXDOMAIN
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.side_effect = [YXDOMAIN]
        try:
            client._get_identity_authority_once('testionos.domainid.community')
            assert False, "No exception thrown: YXDOMAIN"
        except ID4meDNSResolverException:
            pass

        # error case: NoAnswer
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.side_effect = [NoAnswer]
        try:
            client._get_identity_authority_once('testionos.domainid.community')
            assert False, "No exception thrown: NoAnswer"
        except ID4meDNSResolverException:
            pass

        # error case: NoNameservers
        client.resolver = mock.create_autospec(Resolver)
        client.resolver.query.side_effect = [NoNameservers]
        try:
            client._get_identity_authority_once('testionos.domainid.community')
            assert False, "No exception thrown: NoNameservers"
        except ID4meDNSResolverException:
            pass

    def test__register_identity_authority(self):
        auth_store = dict()
        privkey = ID4meClient.generate_new_private_keys_set();
        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False, private_jwks_json=privkey)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)

        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.return_value = json.dumps(self._denic_registration).encode(), 200, 'application/json'
            registration = client._register_identity_authority('id.test.denic.de')
            assert registration == self._denic_registration
            http_request_mock.assert_called_with(
                client.networkContext,
                'POST',
                'https://id.test.denic.de/clients',
                '{"redirect_uris": ["http://localhost:8090/validate"], '
                     '"id_token_signed_response_alg": "RS512", '
                     '"jwks": ' + json.dumps(json.loads(JWKSet.from_json(privkey).export(private_keys=False))) + ', '
                     '"client_name": "Test", '
                     '"logo_uri": "http://localhost:8090/logo.png", '
                     '"policy_uri": "http://localhost:8090/about", '
                     '"tos_uri": "http://localhost:8090/documents", '
                     '"application_type": "native"}',
                None, None, 'application/json', None, None, None)

    def test__register_identity_authority_no_signing(self):
        auth_store = dict()
        privkey = ID4meClient.generate_new_private_keys_set();
        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False, private_jwks_json=privkey,
            require_id_token_signing=False,
            require_user_info_signing=False)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)

        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.return_value = json.dumps(self._denic_registration).encode(), 200, 'application/json'
            registration = client._register_identity_authority('id.test.denic.de')
            assert registration == self._denic_registration
            http_request_mock.assert_called_with(
                client.networkContext,
                'POST',
                'https://id.test.denic.de/clients',
                '{"redirect_uris": ["http://localhost:8090/validate"], '
                     '"id_token_signed_response_alg": "none", '
                     '"userinfo_signed_response_alg": "none", '
                     '"jwks": ' + json.dumps(json.loads(JWKSet.from_json(privkey).export(private_keys=False))) + ', '
                     '"client_name": "Test", '
                     '"logo_uri": "http://localhost:8090/logo.png", '
                     '"policy_uri": "http://localhost:8090/about", '
                     '"tos_uri": "http://localhost:8090/documents", '
                     '"application_type": "native"}',
                None, None, 'application/json', None, None, None)

    def test__register_identity_authority_enforced_signing(self):
        auth_store = dict()
        privkey = ID4meClient.generate_new_private_keys_set();
        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False, private_jwks_json=privkey,
            require_id_token_signing=True,
            require_user_info_signing=True)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)

        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.return_value = json.dumps(self._denic_registration).encode(), 200, 'application/json'
            registration = client._register_identity_authority('id.test.denic.de')
            assert registration == self._denic_registration
            http_request_mock.assert_called_with(
                client.networkContext,
                'POST',
                'https://id.test.denic.de/clients',
                '{"redirect_uris": ["http://localhost:8090/validate"], '
                     '"id_token_signed_response_alg": "RS512", '
                     '"userinfo_signed_response_alg": "RS512", '
                     '"jwks": ' + json.dumps(json.loads(JWKSet.from_json(privkey).export(private_keys=False))) + ', '
                     '"client_name": "Test", '
                     '"logo_uri": "http://localhost:8090/logo.png", '
                     '"policy_uri": "http://localhost:8090/about", '
                     '"tos_uri": "http://localhost:8090/documents", '
                     '"application_type": "native"}',
                None, None, 'application/json', None, None, None)


    def test__register_identity_authority_default_signing(self):
        auth_store = dict()
        privkey = ID4meClient.generate_new_private_keys_set();
        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False, private_jwks_json=privkey,
            require_id_token_signing=None,
            require_user_info_signing=None)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)

        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.return_value = json.dumps(self._denic_registration).encode(), 200, 'application/json'
            registration = client._register_identity_authority('id.test.denic.de')
            assert registration == self._denic_registration
            http_request_mock.assert_called_with(
                client.networkContext,
                'POST',
                'https://id.test.denic.de/clients',
                '{"redirect_uris": ["http://localhost:8090/validate"], '
                     '"jwks": ' + json.dumps(json.loads(JWKSet.from_json(privkey).export(private_keys=False))) + ', '
                     '"client_name": "Test", '
                     '"logo_uri": "http://localhost:8090/logo.png", '
                     '"policy_uri": "http://localhost:8090/about", '
                     '"tos_uri": "http://localhost:8090/documents", '
                     '"application_type": "native"}',
                None, None, 'application/json', None, None, None)

    def test__register_identity_authority_with_encryption(self):
        auth_store = dict()
        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=True, params={'requireencryption': True})
        client.jwksUrl = 'https://foo.com/jwks.json'

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)

        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.return_value = json.dumps(self._denic_registration).encode(), 200, 'application/json'
            registration = client._register_identity_authority('id.test.denic.de')
            assert registration == self._denic_registration
            http_request_mock.assert_called_once_with(
                client.networkContext, 'POST', 'https://id.test.denic.de/clients',
                '{"redirect_uris": ["http://localhost:8090/validate"], '
                '"id_token_signed_response_alg": "RS512", '
                '"jwks_uri": "https://foo.com/jwks.json", '
                '"id_token_encrypted_response_alg": "RSA-OAEP-256", '
                '"userinfo_encrypted_response_alg": "RSA-OAEP-256", '
                '"client_name": "Test", '
                '"logo_uri": "http://localhost:8090/logo.png", '
                '"policy_uri": "http://localhost:8090/about", '
                '"tos_uri": "http://localhost:8090/documents", '
                '"application_type": "native"}',
                None, None, 'application/json', None, None, None
            )

    def test__register_identity_authority__error_response(self):
        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=None,
            save_client_registration=None)
        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)

        # error case: http error code 401 but client_id and client_secret returned
        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.return_value = json.dumps(
                {
                    'client_id': 'ClId',
                    'client_secret': 'ClSec'
                }
            ).encode(), 401, 'application/json'
            try:
                client._register_identity_authority('id.test.denic.de')
                assert False, "No exception raised: http error code 401 but client_id and client_secret returned"
            except ID4meRelyingPartyRegistrationException:
                pass

        # error case: http error code 200 but error values returned
        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.return_value = json.dumps(
                {
                    'error': 'Some error',
                    'error_description': 'Some description'
                }
            ).encode(), 200, 'application/json'
            try:
                client._register_identity_authority('id.test.denic.de')
                assert False, "No exception raised: http error code 200 but error values returned"
            except ID4meRelyingPartyRegistrationException:
                pass

        # error case: http error code 200 no client_id and client_secret returned
        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.return_value = json.dumps(
                {
                }
            ).encode(), 200, 'application/json'
            try:
                client._register_identity_authority('id.test.denic.de')
                assert False, "No exception raised: http error code 200 no client_id and client_secret returned"
            except ID4meRelyingPartyRegistrationException:
                pass


    @mock.patch('id4me_rp_client.network._http_request_raw')
    def test__register_identity_authority__expired_secret(self, http_request_mock):
        test_id = 'id200.connect.domains'
        ref_timestamp = datetime.datetime.fromtimestamp(1538748630)


        with mock.patch('datetime.datetime', Mock(wraps=datetime.datetime)) as time_mock:
            time_mock.utcnow.return_value = ref_timestamp

            # test with expired registration
            # noinspection PyPep8
            authstore = {
                'id.test.denic.de': '{"client_secret_expires_at": 1538748630, "client_id": "iue5k7yuqgskg", "client_secret": "inUXSzOZDEtG0TsMqBOuMIyHODHRmML24_QdjCoxaWE"}'}
            client = TestID4meClientTestCommon._get_test_client(
                get_client_registration=lambda authname: authstore[authname],
                save_client_registration=lambda authname, authval: authstore.__setitem__(authname, authval),
                generate_private_keys=False, params={'requireencryption': False})

            client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
            client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)

            http_request_mock.return_value = json.dumps(
                {
                    'client_id': 'new_client_id',
                    'client_secret': 'new_client_secret'
                }
            ).encode(), 200, 'application/json'

            ctx = client.get_rp_context(id4me=test_id)
            http_request_mock.assert_called_once_with(
                client.networkContext, 'POST', 'https://id.test.denic.de/clients',
                '{"redirect_uris": ["http://localhost:8090/validate"], '
                '"id_token_signed_response_alg": "RS512", '
                '"client_name": "Test", "logo_uri": "http://localhost:8090/logo.png", '
                '"policy_uri": "http://localhost:8090/about", '
                '"tos_uri": "http://localhost:8090/documents", "application_type": "native"}',
                None, None, 'application/json', None, None, None
            )
            assert ctx is not None
            assert json.loads(authstore['id.test.denic.de'])[
                       'client_id'] == 'new_client_id', "Expected new client ID as the old expired"

            # test with not expired registration (expire in the future)
            # noinspection PyPep8
            authstore = {
                'id.test.denic.de': '{"client_secret_expires_at": 1539748631, "client_id": "iue5k7yuqgskg", "client_secret": "inUXSzOZDEtG0TsMqBOuMIyHODHRmML24_QdjCoxaWE"}'}
            http_request_mock.reset_mock()
            ctx = client.get_rp_context(id4me=test_id)

            http_request_mock.assert_not_called()
            assert ctx is not None
            assert json.loads(authstore['id.test.denic.de'])[
                       'client_id'] == 'iue5k7yuqgskg', "Expected old client ID as the old does not expire"

            # test with not expired registration (expire equal 0)
            # noinspection PyPep8
            authstore = {
                'id.test.denic.de': '{"client_secret_expires_at": 0, "client_id": "iue5k7yuqgskg", "client_secret": "inUXSzOZDEtG0TsMqBOuMIyHODHRmML24_QdjCoxaWE"}'}
            http_request_mock.reset_mock()
            ctx = client.get_rp_context(id4me=test_id)

            http_request_mock.assert_not_called()
            assert ctx is not None
            assert json.loads(authstore['id.test.denic.de'])[
                       'client_id'] == 'iue5k7yuqgskg', "Expected old client ID as the old does not expire"

            # test with not expired registration (property not specified)
            # noinspection PyPep8
            authstore = {
                'id.test.denic.de': '{"client_id": "iue5k7yuqgskg", "client_secret": "inUXSzOZDEtG0TsMqBOuMIyHODHRmML24_QdjCoxaWE"}'}
            http_request_mock.reset_mock()
            ctx = client.get_rp_context(id4me=test_id)

            http_request_mock.assert_not_called()
            assert ctx is not None
            assert json.loads(authstore['id.test.denic.de'])[
                       'client_id'] == 'iue5k7yuqgskg', "Expected old client ID as the old does not expire"

    @mock.patch('id4me_rp_client.network._http_request_raw')
    def test__register_identity_authority__auth_config_error_cases(self, http_request_mock):
        http_request_mock.return_value = json.dumps(self._denic_registration).encode(), 200, 'application/json'

        auth_store = dict()
        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__('dummy', authval),
            generate_private_keys=True)

        # All values valid
        client.requireencryption = True
        client.require_user_info_signing = None
        client.require_id_token_signing = True
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'id_token_signing_alg_values_supported': ['RS256'],
                                                         'userinfo_signing_alg_values_supported': ['RS256'],
                                                         'userinfo_encryption_alg_values_supported': ['RSA-OAEP-256'],
                                                         'id_token_encryption_alg_values_supported': ['RSA-OAEP-256'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        client._register_identity_authority('id.test.denic.de')

        # Missing id_token_signing_alg_values_supported in config
        client.requireencryption = False
        client.require_user_info_signing = None
        client.require_id_token_signing = True
        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'userinfo_signing_alg_values_supported': ['RS256'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        registered_exception = None
        try:
            client._register_identity_authority('id.test.denic.de')
        except ID4meRelyingPartyRegistrationException as e:
            registered_exception = e

        self.assertIsNotNone(registered_exception)
        self.assertEqual(registered_exception.message, "Required signature algorithm for id_token RS256, RS384 or RS512"
                                                       " not supported by Authority")


        # No valid value for id_token_signing_alg_values_supported in config
        client.requireencryption = False
        client.require_user_info_signing = None
        client.require_id_token_signing = True
        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'userinfo_signing_alg_values_supported': ['RS256'],
                                                         'id_token_signing_alg_values_supported': ['foo', 'bar'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        try:
            client._register_identity_authority('id.test.denic.de')
            assert False, 'Exception expected: No valid value for id_token_signing_alg_values_supported in config'
        except ID4meRelyingPartyRegistrationException:
            pass

        # Missing userinfo_signing_alg_values_supported in config
        client.requireencryption = False
        client.require_user_info_signing = True
        client.require_id_token_signing = True
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'id_token_signing_alg_values_supported': ['RS256'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        try:
            client._register_identity_authority('id.test.denic.de')
            assert False, 'Exception expected: Missing userinfo_signing_alg_values_supported in config'
        except ID4meRelyingPartyRegistrationException:
            pass

        # No valid value for userinfo_signing_alg_values_supported in config
        client.requireencryption = False
        client.require_user_info_signing = True
        client.require_id_token_signing = True
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'userinfo_signing_alg_values_supported': ['foo', 'bar'],
                                                         'id_token_signing_alg_values_supported': ['RS256'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        try:
            client._register_identity_authority('id.test.denic.de')
            assert False, 'Exception expected: No valid value for  userinfo_signing_alg_values_supported in config'
        except ID4meRelyingPartyRegistrationException:
            pass

        # None algorithm not supported and requested by the client config for userinfo
        client.requireencryption = False
        client.require_user_info_signing = False
        client.require_id_token_signing = True
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'userinfo_signing_alg_values_supported': ['RS512'],
                                                         'id_token_signing_alg_values_supported': ['RS256'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        registered_exception = None
        try:
            client._register_identity_authority('id.test.denic.de')
        except ID4meRelyingPartyRegistrationException as e:
            registered_exception = e
        self.assertIsNotNone(registered_exception, "Exception expected")
        self.assertEqual(registered_exception.message, "Required signature algorithm for user_info none not supported by Authority")

        # None algorithm not supported and requested by the client config for id_token
        client.requireencryption = False
        client.require_user_info_signing = None
        client.require_id_token_signing = False
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'userinfo_signing_alg_values_supported': ['RS512'],
                                                         'id_token_signing_alg_values_supported': ['RS256'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        registered_exception = None
        try:
            client._register_identity_authority('id.test.denic.de')
        except ID4meRelyingPartyRegistrationException as e:
            registered_exception = e
        self.assertIsNotNone(registered_exception)
        self.assertEqual(registered_exception.message, "Required signature algorithm for id_token none not supported by Authority")

        # Missing userinfo_encryption_alg_values_supported in config
        client.requireencryption = True
        client.require_user_info_signing = None
        client.require_id_token_signing = True
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'id_token_signing_alg_values_supported': ['RS256'],
                                                         'userinfo_signing_alg_values_supported': ['RS256'],
                                                         'id_token_encryption_alg_values_supported': ['RSA-OAEP-256'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        try:
            client._register_identity_authority('id.test.denic.de')
            assert False, 'Exception expected: Missing userinfo_encryption_alg_values_supported in config'
        except ID4meRelyingPartyRegistrationException:
            pass

        # No valid value for userinfo_encryption_alg_values_supported in config
        client.requireencryption = True
        client.require_user_info_signing = None
        client.require_id_token_signing = True
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'id_token_signing_alg_values_supported': ['RS256'],
                                                         'userinfo_signing_alg_values_supported': ['RS256'],
                                                         'id_token_encryption_alg_values_supported': ['RSA-OAEP-256'],
                                                         'userinfo_encryption_alg_values_supported': ['foo', 'bar'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        try:
            client._register_identity_authority('id.test.denic.de')
            assert False, 'Exception expected: No valid value for userinfo_encryption_alg_values_supported in config'
        except ID4meRelyingPartyRegistrationException:
            pass

        # Missing id_token_encryption_alg_values_supported in config
        client.requireencryption = True
        client.require_user_info_signing = None
        client.require_id_token_signing = True
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'id_token_signing_alg_values_supported': ['RS256'],
                                                         'userinfo_signing_alg_values_supported': ['RS256'],
                                                         'userinfo_encryption_alg_values_supported': ['RSA-OAEP-256'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        try:
            client._register_identity_authority('id.test.denic.de')
            assert False, 'Exception expected: Missing id_token_encryption_alg_values_supported in config'
        except ID4meRelyingPartyRegistrationException:
            pass

        # No valid value for id_token_encryption_alg_values_supported in config
        client.requireencryption = True
        client.require_user_info_signing = None
        client.require_id_token_signing = True
        client._get_openid_configuration = MagicMock(return_value=
                                                     {
                                                         'id_token_signing_alg_values_supported': ['RS256'],
                                                         'userinfo_signing_alg_values_supported': ['RS256'],
                                                         'userinfo_encryption_alg_values_supported': ['RSA-OAEP-256'],
                                                         'id_token_encryption_alg_values_supported': ['foo', 'bar'],
                                                         'registration_endpoint': 'https://foo.com'
                                                     })
        try:
            client._register_identity_authority('id.test.denic.de')
            assert False, 'Exception expected: No valid value for id_token_encryption_alg_values_supported in config'
        except ID4meRelyingPartyRegistrationException:
            pass


    def test_get_rp_context(self):
        test_id = 'idtest1.domainid.community'

        auth_store = dict()

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)
        client._register_identity_authority = MagicMock(return_value=self._denic_registration)

        # case: hapy path
        ctx = client.get_rp_context(id4me=test_id)
        client._register_identity_authority.assert_called_once_with('id.test.denic.de')

        assert ctx is not None
        assert ctx.id == 'idtest1.domainid.community'
        assert ctx.issuer == 'https://id.test.denic.de'
        assert ctx.iau == 'id.test.denic.de'
        assert ctx.sub is None

        # case: test that exception is thrown if new registration needed but save disabled
        client.save_client_registration = None

        auth_store = dict()
        client._register_identity_authority.mock_reset()
        try:
            client.get_rp_context(id4me=test_id)
            assert False, "No exception is thrown if new registration needed but save disabled"
        except ID4meRelyingPartyRegistrationException:
            pass
        client._register_identity_authority.assert_called_once_with('id.test.denic.de')

    def test_get_rp_context__no_dynamic_registration(self):
        test_id = 'idtest1.domainid.community'

        auth_store = {
            'id.test.denic.de': json.dumps({
                'client_id': 'ClientId',
                'client_secret': 'ClientSecret',
            })
        }

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=None,
            generate_private_keys=False
        )

        client.register_client_dynamically = False
        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)
        client._register_identity_authority = MagicMock(return_value=self._denic_registration)

        ctx = client.get_rp_context(id4me=test_id)
        client._register_identity_authority.assert_not_called()

        assert ctx is not None
        assert ctx.id == 'idtest1.domainid.community'
        assert ctx.issuer == 'https://id.test.denic.de'
        assert ctx.iau == 'id.test.denic.de'
        assert ctx.sub is None

        auth_store = dict()
        try:
            client.get_rp_context(id4me=test_id)
            assert False, 'No exception when registration disabled and auth config not in store'
        except ID4meRelyingPartyRegistrationException:
            pass
        client._register_identity_authority.assert_not_called()


    def test_get_consent_url(self):
        test_id = 'id200.connect.domains'
        test_state = str(random.randint(1000000, 10000000))

        auth_store = {'id.test.denic.de': json.dumps(self._denic_registration)}

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)

        ctx = mock.create_autospec(ID4meContext)
        ctx.id = 'idtest1.domainid.community'
        ctx.issuer = 'https://id.test.denic.de'
        ctx.iau = 'id.test.denic.de'
        ctx.sub = None

        # get link with nonce
        link = client.get_consent_url(
            ctx, state=test_state,
            prompt=OIDCPrompt.login,
            usenonce=True,
            claimsrequest=ID4meClaimsRequest(
                id_token_claims={
                    OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='Test reason'),
                    OIDCClaim.email_verified: None,
                },
                userinfo_claims={
                    OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='Test other confusing reason'),
                    OIDCClaim.address: ID4meClaimRequestProperties(essential=True, reason='Yet another reason'),
                    OIDCClaim.name: ID4meClaimRequestProperties(essential=True, reason='Yet another reason'),
                    OIDCClaim.birthdate: ID4meClaimRequestProperties(essential=True)
                })
        )
        assert link is not None, "Login URL is empty (get link with nonce)"
        assert link == 'https://id.test.denic.de/login?response_type=code&client_id=bpwlftz52dw4u&redirect_uri=http%3A//localhost%3A8090/validate&login_hint=idtest1.domainid.community' \
                       '&state=' + test_state + '&prompt=login&nonce=' + ctx.nonce + \
                       '&claims=%7B%22id_token%22%3A%20%7B%22email%22%3A%20%7B%22essential%22%3A%20true%2C%20%22reason%22%3A%20%22Test%20reason%22%7D%2C%20%22email_verified%22%3A%20null%7D%2C%20%22userinfo%22%3A%20%7B%22email%22%3A%20%7B%22essential%22%3A%20true%2C%20%22reason%22%3A%20%22Test%20other%20confusing%20reason%22%7D%2C%20%22address%22%3A%20%7B%22essential%22%3A%20true%2C%20%22reason%22%3A%20%22Yet%20another%20reason%22%7D%2C%20%22name%22%3A%20%7B%22essential%22%3A%20true%2C%20%22reason%22%3A%20%22Yet%20another%20reason%22%7D%2C%20%22birthdate%22%3A%20%7B%22essential%22%3A%20true%7D%7D%7D&scope=openid', \
            'Auth link not matching (get link with nonce)'

        # get link without nonce and with scopes, openid scope missing, no claims
        link = client.get_consent_url(
            ctx,
            usenonce=False,
            scopes=[
                OIDCScope.profile,
                OIDCScope.email,
                'custom'
            ]
        )
        assert link is not None, "Login URL is empty (get link without nonce and with scopes, openid scope missing, no claims)"
        self.assertEqual(link, 'https://id.test.denic.de/login?response_type=code&client_id=bpwlftz52dw4u'
                               '&redirect_uri=http%3A//localhost%3A8090/validate'
                               '&login_hint=idtest1.domainid.community'
                               '&scope=custom%20email%20openid%20profile', \
            'Auth link not matching (get link without nonce and with scopes, openid scope missing, no claims)')

    def test_get_consent_url__claims_not_supported(self):
        test_id = 'id200.connect.domains'
        test_state = str(random.randint(1000000, 10000000))

        auth_store = {'auth.ionos.com': json.dumps(self._denic_registration)}

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False)

        client._get_identity_authority = MagicMock(return_value='auth.ionos.com')
        client._get_openid_configuration = MagicMock(return_value=self._ionos_auth_openid_config)

        ctx = mock.create_autospec(ID4meContext)
        ctx.id = 'idtest1.domainid.community'
        ctx.issuer = 'https://auth.ionos.com'
        ctx.iau = 'auth.ionos.com'
        ctx.sub = None

        # get link with nonce
        link = client.get_consent_url(
            ctx, state=test_state,
            prompt=OIDCPrompt.login,
            usenonce=True,
            claimsrequest=ID4meClaimsRequest(
                id_token_claims={
                    OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='Test reason'),
                    OIDCClaim.email_verified: None,
                },
                userinfo_claims={
                    OIDCClaim.address: ID4meClaimRequestProperties(essential=True, reason='Yet another reason'),
                    OIDCClaim.name: ID4meClaimRequestProperties(essential=True, reason='Yet another reason'),
                    OIDCClaim.birthdate: ID4meClaimRequestProperties(essential=True)
                })
        )
        assert link is not None, "Login URL is empty (get link with nonce)"
        self.assertEqual(link, 'https://auth.ionos.com/1.0/oauth/auth/init?response_type=code'
                               '&client_id=bpwlftz52dw4u&redirect_uri=http%3A//localhost%3A8090/validate'
                               '&login_hint=idtest1.domainid.community' \
                               '&state=' + test_state + '&prompt=login&nonce=' + ctx.nonce + \
                               '&scope=address%20email%20openid%20profile', \
            'Auth link not matching (get link with claims replaced by scope)')

        # get link without nonce and with scopes, openid scope missing, no claims
        link = client.get_consent_url(
            ctx,
            usenonce=False,
            claimsrequest=ID4meClaimsRequest(
                userinfo_claims={
                    OIDCClaim.name: ID4meClaimRequestProperties(essential=True, reason='Yet another reason'),
                    OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='Test reason'),
                }),
            scopes=[
                OIDCScope.profile,
                'custom'
            ]
        )
        assert link is not None, "Login URL is empty (get link without nonce and with scopes, openid scope missing, no claims)"
        self.assertEqual(link, 'https://auth.ionos.com/1.0/oauth/auth/init?response_type=code'
                               '&client_id=bpwlftz52dw4u&redirect_uri=http%3A//localhost%3A8090/validate'
                               '&login_hint=idtest1.domainid.community'
                               '&scope=custom%20email%20openid%20profile', \
            'Auth link not matching (get link without nonce and with scopes, and unsupported claims, openid scope missing)')

    def test_get_consent_url__claims_not_supported_and_no_fallback(self):
        test_id = 'id200.connect.domains'
        test_state = str(random.randint(1000000, 10000000))

        auth_store = {'auth.ionos.com': json.dumps(self._denic_registration)}

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False,
            use_scope_if_claims_not_supported=False)

        client._get_identity_authority = MagicMock(return_value='auth.ionos.com')
        client._get_openid_configuration = MagicMock(return_value=self._ionos_auth_openid_config)

        ctx = mock.create_autospec(ID4meContext)
        ctx.id = 'idtest1.domainid.community'
        ctx.issuer = 'https://auth.ionos.com'
        ctx.iau = 'auth.ionos.com'
        ctx.sub = None

        exceptionCaught = None
        try:
            client.get_consent_url(
                ctx, state=test_state,
                prompt=OIDCPrompt.login,
                usenonce=True,
                claimsrequest=ID4meClaimsRequest(
                    id_token_claims={
                        OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='Test reason'),
                        OIDCClaim.email_verified: None,
                    },
                    userinfo_claims={
                        OIDCClaim.address: ID4meClaimRequestProperties(essential=True, reason='Yet another reason'),
                        OIDCClaim.name: ID4meClaimRequestProperties(essential=True, reason='Yet another reason'),
                        OIDCClaim.birthdate: ID4meClaimRequestProperties(essential=True)
                    })
            )
        except Exception as e:
            exceptionCaught = e

        self.assertIsInstance(exceptionCaught, ID4meClaimsParameterUnsupportedException, 'Expected exception on claims not supported')
        self.assertEqual(exceptionCaught.message, 'Claims parameter not supported by IdP and downgrade to profiles '
                                                      'not configured with use_scope_if_claims_not_supported')


    @mock.patch('time.time')
    def test_get_idtoken(self, time_mock):
        time_mock.return_value = 1558984505

        auth_store = {'id.test.denic.de': json.dumps(self._denic_registration)}

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(return_value=self._denic_openid_config)
        client._get_public_keys_set = MagicMock(return_value=JWKSet.from_json('{"keys":[{"kty":"RSA","e":"AQAB","use":"sig","kid":"FOvy","n":"qJrsb07wfa77Big8kUOB3VdkQC3sH1hdOW5eA2vpMGz0-Ltzfvm6KudThn0QsGbGK5if5mJJIKSZ7C-y-Y7bZp3fvsIjwhWp03C2wAC_QJjzxzT91NsxIemhGJ5p3D-AR5uFtiXsObo2_AM9N0_n6G0XkRtjG57rXQjvbphwLSSYn7aaxUV0Iqo-584BshTmKhjkPHGzuIrJlt3wS44Ow9gLb0mWFBRN1hvcjyi4NJp20Rezp0rMGFB5ly-sy_b0yC6lTkhioFCh26o1x-_JsHPZnhNofuL5yUIBa4X-WmXruJxodlUPr-rwa8uuRYzLIuCnlIUXX33tRdnCpGRFMw"},{"kty":"EC","use":"sig","crv":"P-256","kid":"6QMM","x":"p_k_rQFFo2BDXM97VIiPjXxqvr5EeqFDYn-oCLRvx9c","y":"4xQPoUFwaHzoAJMpOo95xW6Aq9ivZJv0uSr8miXIpdw"},{"kty":"EC","use":"sig","crv":"P-384","kid":"XpWw","x":"-PbE7JcDtHJgnWA5TO5E0CMDNMDaKSlzE1BI7RF4o29UqGHe7fJSQUkutARiEoVR","y":"5nYp4ZCopr-s-JbL6SyuCVQRu8EdvIaZjbVj5lQphV2In6JQzK0j4_3dR3iK9-LG"},{"kty":"EC","use":"sig","crv":"P-521","kid":"JOgJ","x":"AIycl7HpLJkOIkAPR5HHCf8I0RewWBuIsfQN-JK3GQLSeCZYkh2R1XIOQUKf-wizB1lM8T0UEAyVX_PeYTpF9WsQ","y":"AJamNsgDzc2r5h0MWaqziIOOToRyXP6yxPeTPT3X-9oTf59G3ccHumHsiNxvtlyAETtneZ4xr2_pK_U7_FQNRWAN"},{"kty":"RSA","e":"AQAB","use":"sig","kid":"9wzy","n":"jl9O56nWJcjWlMRKroBdX-ZGhllv6a5dlYwhZZdSz9MBhKbNOBt8mXCr0I8YilLoJXHdwFkum8ZoJGsgC5NOPZa_UNj5w-WSkmt3aAgPc4Soqm9o9z459Ph-QsCpIbwGo-8cZ_mvABm5DaLJLlgmE6VMebwhR_BfVXFyQw7Z4T0a-31bfX8h3Ih3eoTfaDJFjJFPyCj_N1HqRyNRAhvOzzNmfDRZwTaG0Z1GZWkRRYrRcQkR52BMoMy5sX7RDYqEob9xqGx2gi5xgQCfYXY3VMr58A7f2NqyxQ9dci4w3bxvbGBGAE6wFOdH4YDrLWO5n3nszu6KfgAtRVVzLqzobQ"},{"kty":"EC","use":"sig","crv":"P-256","kid":"3uVT","x":"XM3HiZAIU-vUh1aLQ8-uoTx5aD3NJm8NFxtk8JkfB7A","y":"20H7gDmnPQKzODB9o9M9-AG19KR17sVWcH405oHIoqo"},{"kty":"EC","use":"sig","crv":"P-384","kid":"Jqkp","x":"tyM3N93RMQzYulRkp_1u_0lLBCHPtj10DOnJ2g4LQACedaxa3t4vf41O4bUkD55T","y":"X_gsjC_tHiQnO2YjEwjKk0B6XX2I3lCpM_ZKKV4Pbjqx70JiAagn7Ne9wvXugWxH"},{"kty":"EC","use":"sig","crv":"P-521","kid":"eHj7","x":"AcinoGwEavdO_Hl2afwUHFmcUTlnRp0pd28YS5uZ7gm6Ct2h7fWUwnym9XO21NVhuRBccCpW9vssut3uKmNaffsS","y":"AeHIUFf8g3aBrP69mMHK5tC8D6HkolJACIoyJzOTzoE_WaMvbiS4d2lBy3xf7d8HAUE_stuhaBO2NOgGhw3CZMzU"},{"kty":"RSA","e":"AQAB","use":"sig","kid":"CXup","n":"hrwD-lc-IwzwidCANmy4qsiZk11yp9kHykOuP0yOnwi36VomYTQVEzZXgh2sDJpGgAutdQudgwLoV8tVSsTG9SQHgJjH9Pd_9V4Ab6PANyZNG6DSeiq1QfiFlEP6Obt0JbRB3W7X2vkxOVaNoWrYskZodxU2V0ogeVL_LkcCGAyNu2jdx3j0DjJatNVk7ystNxb9RfHhJGgpiIkO5S3QiSIVhbBKaJHcZHPF1vq9g0JMGuUCI-OTSVg6XBkTLEGw1C_R73WD_oVEBfdXbXnLukoLHBS11p3OxU7f4rfxA_f_72_UwmWGJnsqS3iahbms3FkvqoL9x_Vj3GhuJSf97Q"},{"kty":"EC","use":"sig","crv":"P-256","kid":"yGvt","x":"pvgdqM3RCshljmuCF1D2Ez1w5ei5k7-bpimWLPNeEHI","y":"JSmUhbUTqiFclVLEdw6dz038F7Whw4URobjXbAReDuM"},{"kty":"EC","use":"sig","crv":"P-384","kid":"9nHY","x":"JPKhjhE0Bj579Mgj3Cn3ERGA8fKVYoGOaV9BPKhtnEobphf8w4GSeigMesL-038W","y":"UbJa1QRX7fo9LxSlh7FOH5ABT5lEtiQeQUcX9BW0bpJFlEVGqwec80tYLdOIl59M"},{"kty":"EC","use":"sig","crv":"P-521","kid":"tVzS","x":"AZgkRHlIyNQJlPIwTWdHqouw41k9dS3GJO04BDEnJnd_Dd1owlCn9SMXA-JuXINn4slwbG4wcECbctXb2cvdGtmn","y":"AdBC6N9lpupzfzcIY3JLIuc8y8MnzV-ItmzHQcC5lYWMTbuM9NU_FlvINeVo8g6i4YZms2xFB-B0VVdaoF9kUswC"}]}'))

        _test_nonce = '44e759dc-9b77-445f-b22b-33d22ed8384b'
        ctx = mock.create_autospec(ID4meContext)
        ctx.id = 'idtest1.domainid.community'
        ctx.issuer = 'https://id.test.denic.de'
        ctx.iau = 'id.test.denic.de'
        ctx.nonce = _test_nonce
        ctx.sub = None

        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:

            # happy path, token_type "Bearer"
            http_request_mock.return_value = json.dumps(
                {
                    "access_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImlkNG1lLmlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiYWRkcmVzcyIsImJpcnRoZGF0ZSIsIm5hbWUiLCJlbWFpbCJdLCJzY29wZSI6WyJvcGVuaWQiXSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJleHAiOjE1NTg5OTIyOTUsImlhdCI6MTU1ODk5MTY5NSwianRpIjoiS1ROX29RWVd2V0UiLCJjbGllbnRfaWQiOiJicHdsZnR6NTJkdzR1In0.itdKbW8BJmQU7fEEWxW3kgE5IHQeb_7PUcQhp-PILgqWD5xnNL81ZKjU8Qfzjs5XVNJYTbBShAqAmiQkukCVV0kmWyYWLL5wYbL7Vst84h9oog0Gdlt-RXDEGDgIeKQ4uVMvykoYdbj06rvf7u7h4CUsnmpuJUOG30d5Zq0mX4FaDU01ZtxQMhUHpidm07TTodbKoAk2BADqjDZprc_Dhl00bOaR82i8YKZZYT0x_jhuMAPc9pXIS-KfqIMWdzmyKwuIUiVe_18GV1a0q9cZTiu8x_xsgo3UGuhGDJ6BVL4JAeqMYft3W0QJo2B1-nBw1uAO1f6SMvwB_gqKXc6RIg", "scope": "openid",
                    "id_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImF1ZCI6ImJwd2xmdHo1MmR3NHUiLCJpZDRtZS5pZGVudGlmaWVyIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJhbXIiOlsicHdkIl0sImlzcyI6Imh0dHBzOlwvXC9pZC50ZXN0LmRlbmljLmRlIiwiZXhwIjoxNTU4OTkyNTQ3LCJpYXQiOjE1NTg5OTE2NDcsIm5vbmNlIjoiNDRlNzU5ZGMtOWI3Ny00NDVmLWIyMmItMzNkMjJlZDgzODRiIn0.jBf8fooszNNBL1PL5oGCRVARj2GwAqvmdOqUc3fMIdrL-qZvqIb4O4CPKGy_bpdIJDLe3qrMlWjqRmFw9T3UNTinVsQQlA0hVIU2ikf0aOUJQEpYK4w6o-cDHr04YqVEzkW6SbgzSAdZvu-SZ8bJUkrKW-jfdxaxDHYVM1HOwZMnQhA1CH8TZLSBJ0BRmPBpizsM_Jm6QhWpliw-eEbvQzO7B6qUtknyMg905d5lyTfHa2dw3m3D8kAs4uk7Hc--bZcu9v3JsmHurYPu-ejOiZOVn4yLzrmOe9b8ZB_sP5tD-cDiML3ho5HsVVL7mKTkzQo9NtEyvpn3M4tx0Xsdxw",
                    "token_type": "Bearer",
                    "expires_in": 600
                }
            ).encode(), 200, 'application/json'
            id_token = client.get_idtoken(ctx, code='dummycode')
            assert id_token is not None, 'ID token empty'
            print ('** ID token: {}'.format(id_token))
            assert 'iss' in id_token, 'No iss in id token'
            assert id_token['iss'] == 'https://id.test.denic.de'
            assert 'sub' in id_token, 'No sub in id token'
            assert id_token['sub'] == '7kn6ENyaLST+IKpqthXSJq217qyMU4y8PwCSeNsxP+9L5xkcNMKEHS4DsE/w+nXQ'
            http_request_mock.assert_called_once_with(
                client.networkContext, 'POST', 'https://id.test.denic.de/token',
                'grant_type=authorization_code&code=dummycode&redirect_uri=http%3A//localhost%3A8090/validate',
                ('bpwlftz52dw4u', 'fP4I8yzCkRuMQAEVfwpg0FX8Y5QG5NGgsveUV6UbHZI'), None,
                'application/x-www-form-urlencoded', None, None, None)

            # happy path, token_type "bearer", refresh_token
            ctx.nonce = _test_nonce
            http_request_mock.reset_mock()
            http_request_mock.return_value = json.dumps(
                {
                    "access_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImlkNG1lLmlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiYWRkcmVzcyIsImJpcnRoZGF0ZSIsIm5hbWUiLCJlbWFpbCJdLCJzY29wZSI6WyJvcGVuaWQiXSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJleHAiOjE1NTg5OTIyOTUsImlhdCI6MTU1ODk5MTY5NSwianRpIjoiS1ROX29RWVd2V0UiLCJjbGllbnRfaWQiOiJicHdsZnR6NTJkdzR1In0.itdKbW8BJmQU7fEEWxW3kgE5IHQeb_7PUcQhp-PILgqWD5xnNL81ZKjU8Qfzjs5XVNJYTbBShAqAmiQkukCVV0kmWyYWLL5wYbL7Vst84h9oog0Gdlt-RXDEGDgIeKQ4uVMvykoYdbj06rvf7u7h4CUsnmpuJUOG30d5Zq0mX4FaDU01ZtxQMhUHpidm07TTodbKoAk2BADqjDZprc_Dhl00bOaR82i8YKZZYT0x_jhuMAPc9pXIS-KfqIMWdzmyKwuIUiVe_18GV1a0q9cZTiu8x_xsgo3UGuhGDJ6BVL4JAeqMYft3W0QJo2B1-nBw1uAO1f6SMvwB_gqKXc6RIg", "scope": "openid",
                    "id_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImF1ZCI6ImJwd2xmdHo1MmR3NHUiLCJpZDRtZS5pZGVudGlmaWVyIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJhbXIiOlsicHdkIl0sImlzcyI6Imh0dHBzOlwvXC9pZC50ZXN0LmRlbmljLmRlIiwiZXhwIjoxNTU4OTkyNTQ3LCJpYXQiOjE1NTg5OTE2NDcsIm5vbmNlIjoiNDRlNzU5ZGMtOWI3Ny00NDVmLWIyMmItMzNkMjJlZDgzODRiIn0.jBf8fooszNNBL1PL5oGCRVARj2GwAqvmdOqUc3fMIdrL-qZvqIb4O4CPKGy_bpdIJDLe3qrMlWjqRmFw9T3UNTinVsQQlA0hVIU2ikf0aOUJQEpYK4w6o-cDHr04YqVEzkW6SbgzSAdZvu-SZ8bJUkrKW-jfdxaxDHYVM1HOwZMnQhA1CH8TZLSBJ0BRmPBpizsM_Jm6QhWpliw-eEbvQzO7B6qUtknyMg905d5lyTfHa2dw3m3D8kAs4uk7Hc--bZcu9v3JsmHurYPu-ejOiZOVn4yLzrmOe9b8ZB_sP5tD-cDiML3ho5HsVVL7mKTkzQo9NtEyvpn3M4tx0Xsdxw",
                    "token_type": "bearer",
                    "refresh_token": "Rtoken",
                    "expires_in": 600
                }
            ).encode(), 200, 'application/json'
            id_token = client.get_idtoken(ctx, code='dummycode')
            assert id_token is not None
            assert ctx.refresh_token is not None
            http_request_mock.assert_called_once()

            # case: exception in token request
            http_request_mock.reset_mock()
            http_request_mock.return_value = '{"error": "access denied"}'.encode(), 401
            try:
                client.get_idtoken(ctx, code='dummycode2')
                assert False, "No expected exception: exception in token request"
            except ID4meTokenRequestException:
                pass
            http_request_mock.assert_called_once()

            # case: missing id_token
            http_request_mock.reset_mock()
            http_request_mock.return_value = json.dumps(
                {
                    "access_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImlkNG1lLmlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiYWRkcmVzcyIsImJpcnRoZGF0ZSIsIm5hbWUiLCJlbWFpbCJdLCJzY29wZSI6WyJvcGVuaWQiXSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJleHAiOjE1NTg5OTIyOTUsImlhdCI6MTU1ODk5MTY5NSwianRpIjoiS1ROX29RWVd2V0UiLCJjbGllbnRfaWQiOiJicHdsZnR6NTJkdzR1In0.itdKbW8BJmQU7fEEWxW3kgE5IHQeb_7PUcQhp-PILgqWD5xnNL81ZKjU8Qfzjs5XVNJYTbBShAqAmiQkukCVV0kmWyYWLL5wYbL7Vst84h9oog0Gdlt-RXDEGDgIeKQ4uVMvykoYdbj06rvf7u7h4CUsnmpuJUOG30d5Zq0mX4FaDU01ZtxQMhUHpidm07TTodbKoAk2BADqjDZprc_Dhl00bOaR82i8YKZZYT0x_jhuMAPc9pXIS-KfqIMWdzmyKwuIUiVe_18GV1a0q9cZTiu8x_xsgo3UGuhGDJ6BVL4JAeqMYft3W0QJo2B1-nBw1uAO1f6SMvwB_gqKXc6RIg", "scope": "openid",
                    "token_type": "Bearer",
                    "expires_in": 600
                }
            ).encode(), 200, 'application/json'
            try:
                client.get_idtoken(ctx, code='dummycode2')
                assert False, "No expected exception: missing id_token"
            except ID4meTokenRequestException:
                pass
            http_request_mock.assert_called_once()

            # case: missing access_token
            http_request_mock.reset_mock()
            http_request_mock.return_value = json.dumps(
                {
                    "id_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImF1ZCI6ImJwd2xmdHo1MmR3NHUiLCJpZDRtZS5pZGVudGlmaWVyIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJhbXIiOlsicHdkIl0sImlzcyI6Imh0dHBzOlwvXC9pZC50ZXN0LmRlbmljLmRlIiwiZXhwIjoxNTU4OTkyNTQ3LCJpYXQiOjE1NTg5OTE2NDcsIm5vbmNlIjoiNDRlNzU5ZGMtOWI3Ny00NDVmLWIyMmItMzNkMjJlZDgzODRiIn0.jBf8fooszNNBL1PL5oGCRVARj2GwAqvmdOqUc3fMIdrL-qZvqIb4O4CPKGy_bpdIJDLe3qrMlWjqRmFw9T3UNTinVsQQlA0hVIU2ikf0aOUJQEpYK4w6o-cDHr04YqVEzkW6SbgzSAdZvu-SZ8bJUkrKW-jfdxaxDHYVM1HOwZMnQhA1CH8TZLSBJ0BRmPBpizsM_Jm6QhWpliw-eEbvQzO7B6qUtknyMg905d5lyTfHa2dw3m3D8kAs4uk7Hc--bZcu9v3JsmHurYPu-ejOiZOVn4yLzrmOe9b8ZB_sP5tD-cDiML3ho5HsVVL7mKTkzQo9NtEyvpn3M4tx0Xsdxw",
                    "token_type": "Bearer",
                    "expires_in": 600
                }
            ).encode(), 200, 'application/json'
            try:
                client.get_idtoken(ctx, code='dummycode2')
                assert False, "No expected exception: missing access_token"
            except ID4meTokenRequestException:
                pass
            http_request_mock.assert_called_once()

            # case: missing token_type
            http_request_mock.reset_mock()
            http_request_mock.return_value = json.dumps(
                {
                    "access_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImlkNG1lLmlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiYWRkcmVzcyIsImJpcnRoZGF0ZSIsIm5hbWUiLCJlbWFpbCJdLCJzY29wZSI6WyJvcGVuaWQiXSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJleHAiOjE1NTg5OTIyOTUsImlhdCI6MTU1ODk5MTY5NSwianRpIjoiS1ROX29RWVd2V0UiLCJjbGllbnRfaWQiOiJicHdsZnR6NTJkdzR1In0.itdKbW8BJmQU7fEEWxW3kgE5IHQeb_7PUcQhp-PILgqWD5xnNL81ZKjU8Qfzjs5XVNJYTbBShAqAmiQkukCVV0kmWyYWLL5wYbL7Vst84h9oog0Gdlt-RXDEGDgIeKQ4uVMvykoYdbj06rvf7u7h4CUsnmpuJUOG30d5Zq0mX4FaDU01ZtxQMhUHpidm07TTodbKoAk2BADqjDZprc_Dhl00bOaR82i8YKZZYT0x_jhuMAPc9pXIS-KfqIMWdzmyKwuIUiVe_18GV1a0q9cZTiu8x_xsgo3UGuhGDJ6BVL4JAeqMYft3W0QJo2B1-nBw1uAO1f6SMvwB_gqKXc6RIg", "scope": "openid",
                    "id_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImF1ZCI6ImJwd2xmdHo1MmR3NHUiLCJpZDRtZS5pZGVudGlmaWVyIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJhbXIiOlsicHdkIl0sImlzcyI6Imh0dHBzOlwvXC9pZC50ZXN0LmRlbmljLmRlIiwiZXhwIjoxNTU4OTkyNTQ3LCJpYXQiOjE1NTg5OTE2NDcsIm5vbmNlIjoiNDRlNzU5ZGMtOWI3Ny00NDVmLWIyMmItMzNkMjJlZDgzODRiIn0.jBf8fooszNNBL1PL5oGCRVARj2GwAqvmdOqUc3fMIdrL-qZvqIb4O4CPKGy_bpdIJDLe3qrMlWjqRmFw9T3UNTinVsQQlA0hVIU2ikf0aOUJQEpYK4w6o-cDHr04YqVEzkW6SbgzSAdZvu-SZ8bJUkrKW-jfdxaxDHYVM1HOwZMnQhA1CH8TZLSBJ0BRmPBpizsM_Jm6QhWpliw-eEbvQzO7B6qUtknyMg905d5lyTfHa2dw3m3D8kAs4uk7Hc--bZcu9v3JsmHurYPu-ejOiZOVn4yLzrmOe9b8ZB_sP5tD-cDiML3ho5HsVVL7mKTkzQo9NtEyvpn3M4tx0Xsdxw",
                    "expires_in": 600
                }
            ).encode(), 200, 'application/json'
            try:
                client.get_idtoken(ctx, code='dummycode2')
                assert False, "No expected exception: missing token_type"
            except ID4meTokenRequestException:
                pass
            http_request_mock.assert_called_once()

            # case: invalid token_type
            http_request_mock.reset_mock()
            http_request_mock.return_value = json.dumps(
                {
                    "access_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImlkNG1lLmlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiYWRkcmVzcyIsImJpcnRoZGF0ZSIsIm5hbWUiLCJlbWFpbCJdLCJzY29wZSI6WyJvcGVuaWQiXSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJleHAiOjE1NTg5OTIyOTUsImlhdCI6MTU1ODk5MTY5NSwianRpIjoiS1ROX29RWVd2V0UiLCJjbGllbnRfaWQiOiJicHdsZnR6NTJkdzR1In0.itdKbW8BJmQU7fEEWxW3kgE5IHQeb_7PUcQhp-PILgqWD5xnNL81ZKjU8Qfzjs5XVNJYTbBShAqAmiQkukCVV0kmWyYWLL5wYbL7Vst84h9oog0Gdlt-RXDEGDgIeKQ4uVMvykoYdbj06rvf7u7h4CUsnmpuJUOG30d5Zq0mX4FaDU01ZtxQMhUHpidm07TTodbKoAk2BADqjDZprc_Dhl00bOaR82i8YKZZYT0x_jhuMAPc9pXIS-KfqIMWdzmyKwuIUiVe_18GV1a0q9cZTiu8x_xsgo3UGuhGDJ6BVL4JAeqMYft3W0QJo2B1-nBw1uAO1f6SMvwB_gqKXc6RIg", "scope": "openid",
                    "id_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImF1ZCI6ImJwd2xmdHo1MmR3NHUiLCJpZDRtZS5pZGVudGlmaWVyIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJhbXIiOlsicHdkIl0sImlzcyI6Imh0dHBzOlwvXC9pZC50ZXN0LmRlbmljLmRlIiwiZXhwIjoxNTU4OTkyNTQ3LCJpYXQiOjE1NTg5OTE2NDcsIm5vbmNlIjoiNDRlNzU5ZGMtOWI3Ny00NDVmLWIyMmItMzNkMjJlZDgzODRiIn0.jBf8fooszNNBL1PL5oGCRVARj2GwAqvmdOqUc3fMIdrL-qZvqIb4O4CPKGy_bpdIJDLe3qrMlWjqRmFw9T3UNTinVsQQlA0hVIU2ikf0aOUJQEpYK4w6o-cDHr04YqVEzkW6SbgzSAdZvu-SZ8bJUkrKW-jfdxaxDHYVM1HOwZMnQhA1CH8TZLSBJ0BRmPBpizsM_Jm6QhWpliw-eEbvQzO7B6qUtknyMg905d5lyTfHa2dw3m3D8kAs4uk7Hc--bZcu9v3JsmHurYPu-ejOiZOVn4yLzrmOe9b8ZB_sP5tD-cDiML3ho5HsVVL7mKTkzQo9NtEyvpn3M4tx0Xsdxw",
                    "token_type": "Dummy",
                    "expires_in": 600
                }
            ).encode(), 200, 'application/json'
            try:
                client.get_idtoken(ctx, code='dummycode2')
                assert False, "No expected exception: invalid token_type"
            except ID4meTokenRequestException:
                pass
            http_request_mock.assert_called_once()


    @mock.patch('time.time')
    def test_get_user_info(self, time_mock):
        time_mock.return_value = 1558991940

        auth_store = {'id.test.denic.de': json.dumps(self._denic_registration)}

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(side_effect=lambda x: self._openid_configs[x])
        client._get_public_keys_set = MagicMock(side_effect=lambda x: self._entity_keys[x])

        ctx = mock.create_autospec(ID4meContext)
        ctx.id = 'idtest1.domainid.community'
        ctx.issuer = 'https://id.test.denic.de'
        ctx.iau = 'id.test.denic.de'
        ctx.nonce = '996f37f0-41ac-439f-a147-961656e43697'
        ctx.sub = '7kn6ENyaLST+IKpqthXSJq217qyMU4y8PwCSeNsxP+9L5xkcNMKEHS4DsE/w+nXQ'
        ctx.iss = 'https://id.test.denic.de'
        ctx.access_token = 'eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImlkNG1lLmlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiYWRkcmVzcyIsImJpcnRoZGF0ZSIsIm5hbWUiLCJlbWFpbCJdLCJzY29wZSI6WyJvcGVuaWQiXSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJleHAiOjE1NTg5OTIyOTUsImlhdCI6MTU1ODk5MTY5NSwianRpIjoiS1ROX29RWVd2V0UiLCJjbGllbnRfaWQiOiJicHdsZnR6NTJkdzR1In0.itdKbW8BJmQU7fEEWxW3kgE5IHQeb_7PUcQhp-PILgqWD5xnNL81ZKjU8Qfzjs5XVNJYTbBShAqAmiQkukCVV0kmWyYWLL5wYbL7Vst84h9oog0Gdlt-RXDEGDgIeKQ4uVMvykoYdbj06rvf7u7h4CUsnmpuJUOG30d5Zq0mX4FaDU01ZtxQMhUHpidm07TTodbKoAk2BADqjDZprc_Dhl00bOaR82i8YKZZYT0x_jhuMAPc9pXIS-KfqIMWdzmyKwuIUiVe_18GV1a0q9cZTiu8x_xsgo3UGuhGDJ6BVL4JAeqMYft3W0QJo2B1-nBw1uAO1f6SMvwB_gqKXc6RIg'

        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.side_effect = \
                [
                    ('eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImF1ZCI6ImJwd2xmdHo1MmR3NHUiLCJfY2xhaW1fbmFtZXMiOnsiYWRkcmVzcyI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiIsImJpcnRoZGF0ZSI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiIsIm5hbWUiOiIwYTg4N2IwYS0yM2VkLTRhNmQtYTcwZi0wOTAxOTlkMGU3N2IiLCJlbWFpbCI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiJ9LCJfY2xhaW1fc291cmNlcyI6eyIwYTg4N2IwYS0yM2VkLTRhNmQtYTcwZi0wOTAxOTlkMGU3N2IiOnsiYWNjZXNzX3Rva2VuIjoiZXlKcmFXUWlPaUpHVDNaNUlpd2lZV3huSWpvaVVsTXlOVFlpZlEuZXlKemRXSWlPaUkzYTI0MlJVNTVZVXhUVkN0SlMzQnhkR2hZVTBweE1qRTNjWGxOVlRSNU9GQjNRMU5sVG5ONFVDczVURFY0YTJOT1RVdEZTRk0wUkhORlhDOTNLMjVZVVNJc0ltbGtORzFsTG1sa1pXNTBhV1pwWlhJaU9pSnBaSFJsYzNReExtUnZiV0ZwYm1sa0xtTnZiVzExYm1sMGVTSXNJbWxrWlc1MGFXWnBaWElpT2lKcFpIUmxjM1F4TG1SdmJXRnBibWxrTG1OdmJXMTFibWwwZVNJc0ltbGtORzFsSWpvaWFXUjBaWE4wTVM1a2IyMWhhVzVwWkM1amIyMXRkVzVwZEhraUxDSmpiRzBpT2xzaVlXUmtjbVZ6Y3lJc0ltSnBjblJvWkdGMFpTSXNJbTVoYldVaUxDSmxiV0ZwYkNKZExDSnpZMjl3WlNJNld5SnZjR1Z1YVdRaVhTd2lhWE56SWpvaWFIUjBjSE02WEM5Y0wybGtMblJsYzNRdVpHVnVhV011WkdVaUxDSmxlSEFpT2pFMU5UZzVPVEl5T1RVc0ltbGhkQ0k2TVRVMU9EazVNVFk1TlN3aWFuUnBJam9pUzFST1gyOVJXVmQyVjBVaUxDSmpiR2xsYm5SZmFXUWlPaUppY0hkc1puUjZOVEprZHpSMUluMC5pdGRLYlc4QkptUVU3ZkVFV3hXM2tnRTVJSFFlYl83UFVjUWhwLVBJTGdxV0Q1eG5OTDgxWktqVThRZnpqczVYVk5KWVRiQlNoQXFBbWlRa3VrQ1ZWMGttV3lZV0xMNXdZYkw3VnN0ODRoOW9vZzBHZGx0LVJYREVHRGdJZUtRNHVWTXZ5a29ZZGJqMDZydmY3dTdoNENVc25tcHVKVU9HMzBkNVpxMG1YNEZhRFUwMVp0eFFNaFVIcGlkbTA3VFRvZGJLb0FrMkJBRHFqRFpwcmNfRGhsMDBiT2FSODJpOFlLWlpZVDB4X2podU1BUGM5cFhJUy1LZnFJTVdkem15S3d1SVVpVmVfMThHVjFhMHE5Y1pUaXU4eF94c2dvM1VHdWhHREo2QlZMNEpBZXFNWWZ0M1cwUUpvMkIxLW5CdzF1QU8xZjZTTXZ3Ql9ncUtYYzZSSWciLCJlbmRwb2ludCI6Imh0dHBzOlwvXC9pZGVudGl0eWFnZW50LmRlXC91c2VyaW5mbyJ9fSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJpYXQiOjE1NTg5OTE3NzJ9.Ui1msk7Sy3l2FRZjsBIaHFeJKJYFnRESgyiW3kadv_vnhWYMAt1LtulKn-PaOndfXQbvrA-mTDb_-cvaT8V91BblSndaPt8b1DN-yRPpsHBevGHsmuJ1IB2zKI6z_7YupH9BdHQh133w7M--hSExWVw18fBnpwCApAm21GfAlDeS9tYstpgUFNRqlOxdi0_rwDToztucdDWImy5VvP2ZJZ_ShISjnlbAz7UBgWmS-Qlq48a1xJZhc6wNBROU66ITmFOseDvu-YJKEuJhVRwlLwb6PfQiZdpWOGbpvOAcYhJo-_Wa1IKXS9xx8zZQK0RJwY9IgtHNXutmGfC1w1JeGA'.encode(), 200, 'application/jwt;charset=UTF-8'),
                    ('eyJ0eXAiOiJKV1QiLCJraWQiOiJrZXlJZCIsImFsZyI6IlJTMjU2In0.eyJpZDRtZS5pZGVudGl0eSI6ImlkdGVzdDEuZG9tYWluaWQuY29tbXVuaXR5IiwiZW1haWwiOiJpZHRlc3QxQGRvbWFpbmlkLmNvbW11bml0eSIsImV4cCI6MTU1ODk5MjIzNywiYWRkcmVzcyI6eyJyZWdpb24iOiIiLCJjb3VudHJ5IjoiTmV2ZXJsYW5kIiwic3RyZWV0X2FkZHJlc3MiOiIiLCJmb3JtYXR0ZWQiOiIiLCJwb3N0YWxfY29kZSI6IiIsImxvY2FsaXR5IjoiIn0sIm5iZiI6MTU1ODk5MTkzNywic3ViIjoiN2tuNkVOeWFMU1QrSUtwcXRoWFNKcTIxN3F5TVU0eThQd0NTZU5zeFArOUw1eGtjTk1LRUhTNERzRS93K25YUSIsIm5hbWUiOiJUZXN0IFVzZXIgMSIsImJpcnRoZGF0ZSI6IjIwMTktMDMtMTEiLCJhdWQiOiJicHdsZnR6NTJkdzR1IiwiaWF0IjoxNTU4OTkxOTM3LCJ1cGRhdGVkX2F0IjoxNTUzNDQ2NTEyLCJpc3MiOiJodHRwczovL2lkZW50aXR5YWdlbnQuZGUiLCJpZDRtZS5pZGVudGlmaWVyIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkifQ.Pl4-gLFqkTeJUZe3hR8CSm2Et-OWTYKYJy4416z1U482DTWCN9QAXbRt1zB92I5bm2R_pDwOe73_cb69lsuG14JfazHXJvutQ_j8mlUngyJ4Cgye_iploy1SfeBYr31D8WUugamlRfe0Q-dx7cC68P58FFQ8TItlnMP8ALuN-R_-cBUns09Ld88QeupEUoWaYqTKw_m1w67kP858IXuCRvRfGGymKWOACWDn-yBgLFzyXA3-vmc9-BtzDlJeJ75gPltoYqlEC0RhHX3AfTtZwT7hmAR7LNocESCY4fv5KWJluudVKogEYTmeZjM9s_-1K703Q1DNsqYJqRE2xlczjA'.encode(), 200, 'application/json+jwt'),
                    ('eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImF1ZCI6ImJwd2xmdHo1MmR3NHUiLCJfY2xhaW1fbmFtZXMiOnsiYWRkcmVzcyI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiIsImJpcnRoZGF0ZSI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiIsIm5hbWUiOiIwYTg4N2IwYS0yM2VkLTRhNmQtYTcwZi0wOTAxOTlkMGU3N2IiLCJlbWFpbCI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiJ9LCJfY2xhaW1fc291cmNlcyI6eyIwYTg4N2IwYS0yM2VkLTRhNmQtYTcwZi0wOTAxOTlkMGU3N2IiOnsiYWNjZXNzX3Rva2VuIjoiZXlKcmFXUWlPaUpHVDNaNUlpd2lZV3huSWpvaVVsTXlOVFlpZlEuZXlKemRXSWlPaUkzYTI0MlJVNTVZVXhUVkN0SlMzQnhkR2hZVTBweE1qRTNjWGxOVlRSNU9GQjNRMU5sVG5ONFVDczVURFY0YTJOT1RVdEZTRk0wUkhORlhDOTNLMjVZVVNJc0ltbGtORzFsTG1sa1pXNTBhV1pwWlhJaU9pSnBaSFJsYzNReExtUnZiV0ZwYm1sa0xtTnZiVzExYm1sMGVTSXNJbWxrWlc1MGFXWnBaWElpT2lKcFpIUmxjM1F4TG1SdmJXRnBibWxrTG1OdmJXMTFibWwwZVNJc0ltbGtORzFsSWpvaWFXUjBaWE4wTVM1a2IyMWhhVzVwWkM1amIyMXRkVzVwZEhraUxDSmpiRzBpT2xzaVlXUmtjbVZ6Y3lJc0ltSnBjblJvWkdGMFpTSXNJbTVoYldVaUxDSmxiV0ZwYkNKZExDSnpZMjl3WlNJNld5SnZjR1Z1YVdRaVhTd2lhWE56SWpvaWFIUjBjSE02WEM5Y0wybGtMblJsYzNRdVpHVnVhV011WkdVaUxDSmxlSEFpT2pFMU5UZzVPVEl5T1RVc0ltbGhkQ0k2TVRVMU9EazVNVFk1TlN3aWFuUnBJam9pUzFST1gyOVJXVmQyVjBVaUxDSmpiR2xsYm5SZmFXUWlPaUppY0hkc1puUjZOVEprZHpSMUluMC5pdGRLYlc4QkptUVU3ZkVFV3hXM2tnRTVJSFFlYl83UFVjUWhwLVBJTGdxV0Q1eG5OTDgxWktqVThRZnpqczVYVk5KWVRiQlNoQXFBbWlRa3VrQ1ZWMGttV3lZV0xMNXdZYkw3VnN0ODRoOW9vZzBHZGx0LVJYREVHRGdJZUtRNHVWTXZ5a29ZZGJqMDZydmY3dTdoNENVc25tcHVKVU9HMzBkNVpxMG1YNEZhRFUwMVp0eFFNaFVIcGlkbTA3VFRvZGJLb0FrMkJBRHFqRFpwcmNfRGhsMDBiT2FSODJpOFlLWlpZVDB4X2podU1BUGM5cFhJUy1LZnFJTVdkem15S3d1SVVpVmVfMThHVjFhMHE5Y1pUaXU4eF94c2dvM1VHdWhHREo2QlZMNEpBZXFNWWZ0M1cwUUpvMkIxLW5CdzF1QU8xZjZTTXZ3Ql9ncUtYYzZSSWciLCJlbmRwb2ludCI6Imh0dHBzOlwvXC9pZGVudGl0eWFnZW50LmRlXC91c2VyaW5mbyJ9fSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJpYXQiOjE1NTg5OTE3NzJ9.Ui1msk7Sy3l2FRZjsBIaHFeJKJYFnRESgyiW3kadv_vnhWYMAt1LtulKn-PaOndfXQbvrA-mTDb_-cvaT8V91BblSndaPt8b1DN-yRPpsHBevGHsmuJ1IB2zKI6z_7YupH9BdHQh133w7M--hSExWVw18fBnpwCApAm21GfAlDeS9tYstpgUFNRqlOxdi0_rwDToztucdDWImy5VvP2ZJZ_ShISjnlbAz7UBgWmS-Qlq48a1xJZhc6wNBROU66ITmFOseDvu-YJKEuJhVRwlLwb6PfQiZdpWOGbpvOAcYhJo-_Wa1IKXS9xx8zZQK0RJwY9IgtHNXutmGfC1w1JeGA'.encode(), 200, 'application/jwt;charset=UTF-8'),
                    (json.dumps(
                            {
                                "sub": "7kn6ENyaLST+IKpqthXSJq217qyMU4y8PwCSeNsxP+9L5xkcNMKEHS4DsE/w+nXQ",
                                "aud": "bpwlftz52dw4u",
                                "_claim_names": {
                                    "address": "0a887b0a-23ed-4a6d-a70f-090199d0e77b",
                                    "birthdate": "0a887b0a-23ed-4a6d-a70f-090199d0e77b",
                                    "name": "0a887b0a-23ed-4a6d-a70f-090199d0e77b",
                                    "email": "0a887b0a-23ed-4a6d-a70f-090199d0e77b"
                                },
                                "_claim_sources": {
                                    "0a887b0a-23ed-4a6d-a70f-090199d0e77b": {
                                        "access_token": "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImlkNG1lLmlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiYWRkcmVzcyIsImJpcnRoZGF0ZSIsIm5hbWUiLCJlbWFpbCJdLCJzY29wZSI6WyJvcGVuaWQiXSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJleHAiOjE1NTg5OTIyOTUsImlhdCI6MTU1ODk5MTY5NSwianRpIjoiS1ROX29RWVd2V0UiLCJjbGllbnRfaWQiOiJicHdsZnR6NTJkdzR1In0.itdKbW8BJmQU7fEEWxW3kgE5IHQeb_7PUcQhp-PILgqWD5xnNL81ZKjU8Qfzjs5XVNJYTbBShAqAmiQkukCVV0kmWyYWLL5wYbL7Vst84h9oog0Gdlt-RXDEGDgIeKQ4uVMvykoYdbj06rvf7u7h4CUsnmpuJUOG30d5Zq0mX4FaDU01ZtxQMhUHpidm07TTodbKoAk2BADqjDZprc_Dhl00bOaR82i8YKZZYT0x_jhuMAPc9pXIS-KfqIMWdzmyKwuIUiVe_18GV1a0q9cZTiu8x_xsgo3UGuhGDJ6BVL4JAeqMYft3W0QJo2B1-nBw1uAO1f6SMvwB_gqKXc6RIg",
                                        "endpoint": "https://identityagent.de/userinfo"
                                    }
                                },
                                "iss": "https://id.test.denic.de",
                                "iat": 1558991772
                            }
                    ).encode(), 200, 'application/json'),
                    (json.dumps(
                            {
                                "id4me.identity": "idtest1.domainid.community",
                                "email": "idtest1@domainid.community",
                                "exp": 1558992237,
                                "address": {
                                    "region": "",
                                    "country": "Neverland",
                                    "street_address": "",
                                    "formatted": "",
                                    "postal_code": "",
                                    "locality": ""
                                },
                                "nbf": 1558991937,
                                "sub": "7kn6ENyaLST+IKpqthXSJq217qyMU4y8PwCSeNsxP+9L5xkcNMKEHS4DsE/w+nXQ",
                                "name": "Test User 1",
                                "birthdate": "2019-03-11",
                                "aud": "bpwlftz52dw4u",
                                "iat": 1558991937,
                                "updated_at": 1553446512,
                                "iss": "https://identityagent.de",
                                "id4me.identifier": "idtest1.domainid.community"
                            }
                    ).encode(), 200, 'application/json')
                ]

            user_info = client.get_user_info(ctx)
            assert user_info is not None, 'User info token empty'
            print ('** User info: {}'.format(user_info))
            assert 'iss' in user_info, 'No iss in user info'
            assert 'sub' in user_info, 'No sub in user info'
            assert str(OIDCClaim.email) in user_info, 'No email in user info'
            assert str(OIDCClaim.address) in user_info, 'No address in user info'
            assert str(OIDCClaim.birthdate) in user_info, 'No birthdate in user info'
            assert str(OIDCClaim.name) in user_info, 'No name in user info'

            # testing without distributed claims (skip IDs where Agent = Authority
            user_info = client.get_user_info(ctx, 0)
            assert user_info is not None, 'User info token empty'
            print ('** User info: {}'.format(user_info))
            assert 'iss' in user_info, 'No iss in user info'
            assert 'sub' in user_info, 'No sub in user info'
            assert str(OIDCClaim.email) not in user_info, 'Agent claim not expected in user info'
            assert str(OIDCClaim.address) not in user_info, 'Agent claim not expected in user info'
            assert str(OIDCClaim.birthdate) not in user_info, 'Agent claim not expected in user info'
            assert str(OIDCClaim.name) not in user_info, 'Agent claim not expected in user info'

            # testing with user info delivered as plain JSON
            user_info = client.get_user_info(ctx)
            assert user_info is not None, 'User info token empty'
            print ('** User info: {}'.format(user_info))
            assert 'iss' in user_info, 'No iss in user info'
            assert 'sub' in user_info, 'No sub in user info'
            assert str(OIDCClaim.email) in user_info, 'No email in user info'
            assert str(OIDCClaim.address) in user_info, 'No address in user info'
            assert str(OIDCClaim.birthdate) in user_info, 'No birthdate in user info'
            assert str(OIDCClaim.name) in user_info, 'No name in user info'

    @mock.patch('time.time')
    def test_get_user_info__error_cases(self, time_mock):
        time_mock.return_value = 1558991940

        auth_store = {'id.test.denic.de': json.dumps(self._denic_registration)}

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(side_effect=lambda x: self._openid_configs[x])
        client._get_public_keys_set = MagicMock(side_effect=lambda x: self._entity_keys[x])

        ctx = mock.create_autospec(ID4meContext)
        ctx.id = 'idtest1.domainid.community'
        ctx.issuer = 'https://id.test.denic.de'
        ctx.iau = 'id.test.denic.de'
        ctx.nonce = '996f37f0-41ac-439f-a147-961656e43697'
        ctx.sub = '7kn6ENyaLST+IKpqthXSJq217qyMU4y8PwCSeNsxP+9L5xkcNMKEHS4DsE/w+nXQ'
        ctx.iss = 'https://id.test.denic.de'
        ctx.access_token = 'aa'

        # case: no access token in context
        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            ctx.access_token = None
            try:
                http_request_mock.side_effect = \
                    [
                        ('eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImF1ZCI6ImJwd2xmdHo1MmR3NHUiLCJfY2xhaW1fbmFtZXMiOnsiYWRkcmVzcyI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiIsImJpcnRoZGF0ZSI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiIsIm5hbWUiOiIwYTg4N2IwYS0yM2VkLTRhNmQtYTcwZi0wOTAxOTlkMGU3N2IiLCJlbWFpbCI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiJ9LCJfY2xhaW1fc291cmNlcyI6eyIwYTg4N2IwYS0yM2VkLTRhNmQtYTcwZi0wOTAxOTlkMGU3N2IiOnsiYWNjZXNzX3Rva2VuIjoiZXlKcmFXUWlPaUpHVDNaNUlpd2lZV3huSWpvaVVsTXlOVFlpZlEuZXlKemRXSWlPaUkzYTI0MlJVNTVZVXhUVkN0SlMzQnhkR2hZVTBweE1qRTNjWGxOVlRSNU9GQjNRMU5sVG5ONFVDczVURFY0YTJOT1RVdEZTRk0wUkhORlhDOTNLMjVZVVNJc0ltbGtORzFsTG1sa1pXNTBhV1pwWlhJaU9pSnBaSFJsYzNReExtUnZiV0ZwYm1sa0xtTnZiVzExYm1sMGVTSXNJbWxrWlc1MGFXWnBaWElpT2lKcFpIUmxjM1F4TG1SdmJXRnBibWxrTG1OdmJXMTFibWwwZVNJc0ltbGtORzFsSWpvaWFXUjBaWE4wTVM1a2IyMWhhVzVwWkM1amIyMXRkVzVwZEhraUxDSmpiRzBpT2xzaVlXUmtjbVZ6Y3lJc0ltSnBjblJvWkdGMFpTSXNJbTVoYldVaUxDSmxiV0ZwYkNKZExDSnpZMjl3WlNJNld5SnZjR1Z1YVdRaVhTd2lhWE56SWpvaWFIUjBjSE02WEM5Y0wybGtMblJsYzNRdVpHVnVhV011WkdVaUxDSmxlSEFpT2pFMU5UZzVPVEl5T1RVc0ltbGhkQ0k2TVRVMU9EazVNVFk1TlN3aWFuUnBJam9pUzFST1gyOVJXVmQyVjBVaUxDSmpiR2xsYm5SZmFXUWlPaUppY0hkc1puUjZOVEprZHpSMUluMC5pdGRLYlc4QkptUVU3ZkVFV3hXM2tnRTVJSFFlYl83UFVjUWhwLVBJTGdxV0Q1eG5OTDgxWktqVThRZnpqczVYVk5KWVRiQlNoQXFBbWlRa3VrQ1ZWMGttV3lZV0xMNXdZYkw3VnN0ODRoOW9vZzBHZGx0LVJYREVHRGdJZUtRNHVWTXZ5a29ZZGJqMDZydmY3dTdoNENVc25tcHVKVU9HMzBkNVpxMG1YNEZhRFUwMVp0eFFNaFVIcGlkbTA3VFRvZGJLb0FrMkJBRHFqRFpwcmNfRGhsMDBiT2FSODJpOFlLWlpZVDB4X2podU1BUGM5cFhJUy1LZnFJTVdkem15S3d1SVVpVmVfMThHVjFhMHE5Y1pUaXU4eF94c2dvM1VHdWhHREo2QlZMNEpBZXFNWWZ0M1cwUUpvMkIxLW5CdzF1QU8xZjZTTXZ3Ql9ncUtYYzZSSWciLCJlbmRwb2ludCI6Imh0dHBzOlwvXC9pZGVudGl0eWFnZW50LmRlXC91c2VyaW5mbyJ9fSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJpYXQiOjE1NTg5OTE3NzJ9.Ui1msk7Sy3l2FRZjsBIaHFeJKJYFnRESgyiW3kadv_vnhWYMAt1LtulKn-PaOndfXQbvrA-mTDb_-cvaT8V91BblSndaPt8b1DN-yRPpsHBevGHsmuJ1IB2zKI6z_7YupH9BdHQh133w7M--hSExWVw18fBnpwCApAm21GfAlDeS9tYstpgUFNRqlOxdi0_rwDToztucdDWImy5VvP2ZJZ_ShISjnlbAz7UBgWmS-Qlq48a1xJZhc6wNBROU66ITmFOseDvu-YJKEuJhVRwlLwb6PfQiZdpWOGbpvOAcYhJo-_Wa1IKXS9xx8zZQK0RJwY9IgtHNXutmGfC1w1JeGA'.encode(), 200, "applicaiont/json+jwt")
                    ]
                caughtexception = None
                try:
                    client.get_user_info(ctx, max_recoursion=0)
                except Exception as ex:
                    caughtexception = ex

                self.assertIsNotNone(caughtexception, "No expected exception occured")
                self.assertIsInstance(caughtexception, ID4meUserInfoRequestException, "Error of unexpected type: {}".format(type(caughtexception)))
                self.assertEqual(caughtexception.message, "No access token is session. Call id_token() first.")
            finally:
                ctx.access_token = 'aa'

        # case: error fetching data
        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.side_effect = \
                [
                    ('error'.encode(), 500, "text/plain")
                ]
            caughtexception = None
            try:
                client.get_user_info(ctx, max_recoursion=0)
            except Exception as ex:
                caughtexception = ex

            self.assertIsNotNone(caughtexception, "No expected exception occured")
            self.assertIsInstance(caughtexception, ID4meUserInfoRequestException,
                                  "Error of unexpected type: {}".format(type(caughtexception)))
            self.assertEqual(caughtexception.message, "Failed to get user info from https://id.test.denic.de/userinfo "
                                                      "(Failed to read from https://id.test.denic.de/userinfo. "
                                                      "HTTP code: 500, content: b'error')")

        # case: wrog issuer in the Agent reponse (no https in iss)
        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.side_effect = \
                [
                    ('eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImF1ZCI6ImJwd2xmdHo1MmR3NHUiLCJfY2xhaW1fbmFtZXMiOnsiYWRkcmVzcyI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiIsImJpcnRoZGF0ZSI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiIsIm5hbWUiOiIwYTg4N2IwYS0yM2VkLTRhNmQtYTcwZi0wOTAxOTlkMGU3N2IiLCJlbWFpbCI6IjBhODg3YjBhLTIzZWQtNGE2ZC1hNzBmLTA5MDE5OWQwZTc3YiJ9LCJfY2xhaW1fc291cmNlcyI6eyIwYTg4N2IwYS0yM2VkLTRhNmQtYTcwZi0wOTAxOTlkMGU3N2IiOnsiYWNjZXNzX3Rva2VuIjoiZXlKcmFXUWlPaUpHVDNaNUlpd2lZV3huSWpvaVVsTXlOVFlpZlEuZXlKemRXSWlPaUkzYTI0MlJVNTVZVXhUVkN0SlMzQnhkR2hZVTBweE1qRTNjWGxOVlRSNU9GQjNRMU5sVG5ONFVDczVURFY0YTJOT1RVdEZTRk0wUkhORlhDOTNLMjVZVVNJc0ltbGtORzFsTG1sa1pXNTBhV1pwWlhJaU9pSnBaSFJsYzNReExtUnZiV0ZwYm1sa0xtTnZiVzExYm1sMGVTSXNJbWxrWlc1MGFXWnBaWElpT2lKcFpIUmxjM1F4TG1SdmJXRnBibWxrTG1OdmJXMTFibWwwZVNJc0ltbGtORzFsSWpvaWFXUjBaWE4wTVM1a2IyMWhhVzVwWkM1amIyMXRkVzVwZEhraUxDSmpiRzBpT2xzaVlXUmtjbVZ6Y3lJc0ltSnBjblJvWkdGMFpTSXNJbTVoYldVaUxDSmxiV0ZwYkNKZExDSnpZMjl3WlNJNld5SnZjR1Z1YVdRaVhTd2lhWE56SWpvaWFIUjBjSE02WEM5Y0wybGtMblJsYzNRdVpHVnVhV011WkdVaUxDSmxlSEFpT2pFMU5UZzVPVEl5T1RVc0ltbGhkQ0k2TVRVMU9EazVNVFk1TlN3aWFuUnBJam9pUzFST1gyOVJXVmQyVjBVaUxDSmpiR2xsYm5SZmFXUWlPaUppY0hkc1puUjZOVEprZHpSMUluMC5pdGRLYlc4QkptUVU3ZkVFV3hXM2tnRTVJSFFlYl83UFVjUWhwLVBJTGdxV0Q1eG5OTDgxWktqVThRZnpqczVYVk5KWVRiQlNoQXFBbWlRa3VrQ1ZWMGttV3lZV0xMNXdZYkw3VnN0ODRoOW9vZzBHZGx0LVJYREVHRGdJZUtRNHVWTXZ5a29ZZGJqMDZydmY3dTdoNENVc25tcHVKVU9HMzBkNVpxMG1YNEZhRFUwMVp0eFFNaFVIcGlkbTA3VFRvZGJLb0FrMkJBRHFqRFpwcmNfRGhsMDBiT2FSODJpOFlLWlpZVDB4X2podU1BUGM5cFhJUy1LZnFJTVdkem15S3d1SVVpVmVfMThHVjFhMHE5Y1pUaXU4eF94c2dvM1VHdWhHREo2QlZMNEpBZXFNWWZ0M1cwUUpvMkIxLW5CdzF1QU8xZjZTTXZ3Ql9ncUtYYzZSSWciLCJlbmRwb2ludCI6Imh0dHBzOlwvXC9pZGVudGl0eWFnZW50LmRlXC91c2VyaW5mbyJ9fSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJpYXQiOjE1NTg5OTE3NzJ9.Ui1msk7Sy3l2FRZjsBIaHFeJKJYFnRESgyiW3kadv_vnhWYMAt1LtulKn-PaOndfXQbvrA-mTDb_-cvaT8V91BblSndaPt8b1DN-yRPpsHBevGHsmuJ1IB2zKI6z_7YupH9BdHQh133w7M--hSExWVw18fBnpwCApAm21GfAlDeS9tYstpgUFNRqlOxdi0_rwDToztucdDWImy5VvP2ZJZ_ShISjnlbAz7UBgWmS-Qlq48a1xJZhc6wNBROU66ITmFOseDvu-YJKEuJhVRwlLwb6PfQiZdpWOGbpvOAcYhJo-_Wa1IKXS9xx8zZQK0RJwY9IgtHNXutmGfC1w1JeGA'.encode(), 200, 'application/jwt;charset=UTF-8'),
                    ('eyJ0eXAiOiJKV1QiLCJraWQiOiIwZDU4MTE0My1hYTU2LTQxMzAtYTM3MS04Njc3NmUxY2QzMzgiLCJhbGciOiJSUzI1NiJ9.eyJpZDRtZS5pZGVudGl0eSI6ImlkdGVzdDEuZG9tYWluaWQuY29tbXVuaXR5IiwiZW1haWwiOiJpZHRlc3QxQGRvbWFpbmlkLmNvbW11bml0eSIsImV4cCI6MTU1ODk5MjIzNywiYWRkcmVzcyI6eyJyZWdpb24iOiIiLCJjb3VudHJ5IjoiTmV2ZXJsYW5kIiwic3RyZWV0X2FkZHJlc3MiOiIiLCJmb3JtYXR0ZWQiOiIiLCJwb3N0YWxfY29kZSI6IiIsImxvY2FsaXR5IjoiIn0sIm5iZiI6MTU1ODk5MTkzNywic3ViIjoiN2tuNkVOeWFMU1QrSUtwcXRoWFNKcTIxN3F5TVU0eThQd0NTZU5zeFArOUw1eGtjTk1LRUhTNERzRS93K25YUSIsIm5hbWUiOiJUZXN0IFVzZXIgMSIsImJpcnRoZGF0ZSI6IjIwMTktMDMtMTEiLCJhdWQiOiJicHdsZnR6NTJkdzR1IiwiaWF0IjoxNTU4OTkxOTM3LCJ1cGRhdGVkX2F0IjoxNTUzNDQ2NTEyLCJpc3MiOiJhZ2VudG5vc2NoZW1haW51cmwuZGUiLCJpZDRtZS5pZGVudGlmaWVyIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkifQ.rPksHFQtMBmVYpVyg3oVVecGfNOeScVjSAgZGqWwuvd92cJRWoo6zgQNqY5Bg2Dbzmi3Wh7X-YoLXcY_sotl5gJs3eeFJBTGXGpnHZKZ1lEWsuZUcfOrxblhjr54xhvv-gm7-Zdf-Cq9by1M5iRmc9cNIEjE0KP1eOZM9okKP9UcK-5HuYDz0r5h4TxQkmyv-Z_A-ELL2yFjpIoxw-I-o31E-i430kS9iAgl9zh4a0trI2WYabtRZdC8DgXyLZGF6S0P7JIGuoqmFsf9Ieoov7m-khrX6w9eebArMkXx-Z-2xQXsaivEr1jWNefo2yD9vDTM-_qXZR92O2a-4zfvDg'.encode(), 200, 'application/json+jwt'),
                ]
            caughtexception = None
            try:
                client.get_user_info(ctx)
            except Exception as ex:
                caughtexception = ex

            self.assertIsNotNone(caughtexception, "No expected exception occured")
            self.assertIsInstance(caughtexception, ID4meUserInfoRequestException,
                                  "Error of unexpected type: {}".format(type(caughtexception)))
            self.assertEqual(caughtexception.message, "Failed to get distributed user info from https://identityagent.de/userinfo ('agentnoschemainurl.de')")

    @mock.patch('time.time')
    def test__get_distributed_claims(self, time_mock):
        time_mock.return_value = 1558991940

        auth_store = {'id.test.denic.de': json.dumps(self._denic_registration)}

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(side_effect=lambda x: self._openid_configs[x])
        client._get_public_keys_set = MagicMock(side_effect=lambda x: self._entity_keys[x])

        ctx = mock.create_autospec(ID4meContext)
        ctx.id = 'idtest1.domainid.community'
        ctx.issuer = 'https://id.test.denic.de'
        ctx.iau = 'id.test.denic.de'
        ctx.nonce = '996f37f0-41ac-439f-a147-961656e43697'
        ctx.sub = '7kn6ENyaLST+IKpqthXSJq217qyMU4y8PwCSeNsxP+9L5xkcNMKEHS4DsE/w+nXQ'
        ctx.iss = 'https://id.test.denic.de'
        ctx.access_token = 'eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImlkNG1lLmlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiYWRkcmVzcyIsImJpcnRoZGF0ZSIsIm5hbWUiLCJlbWFpbCJdLCJzY29wZSI6WyJvcGVuaWQiXSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJleHAiOjE1NTg5OTIyOTUsImlhdCI6MTU1ODk5MTY5NSwianRpIjoiS1ROX29RWVd2V0UiLCJjbGllbnRfaWQiOiJicHdsZnR6NTJkdzR1In0.itdKbW8BJmQU7fEEWxW3kgE5IHQeb_7PUcQhp-PILgqWD5xnNL81ZKjU8Qfzjs5XVNJYTbBShAqAmiQkukCVV0kmWyYWLL5wYbL7Vst84h9oog0Gdlt-RXDEGDgIeKQ4uVMvykoYdbj06rvf7u7h4CUsnmpuJUOG30d5Zq0mX4FaDU01ZtxQMhUHpidm07TTodbKoAk2BADqjDZprc_Dhl00bOaR82i8YKZZYT0x_jhuMAPc9pXIS-KfqIMWdzmyKwuIUiVe_18GV1a0q9cZTiu8x_xsgo3UGuhGDJ6BVL4JAeqMYft3W0QJo2B1-nBw1uAO1f6SMvwB_gqKXc6RIg'

        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.return_value = 'eyJ0eXAiOiJKV1QiLCJraWQiOiJrZXlJZCIsImFsZyI6IlJTMjU2In0.eyJpZDRtZS5pZGVudGl0eSI6ImlkdGVzdDEuZG9tYWluaWQuY29tbXVuaXR5IiwiZW1haWwiOiJpZHRlc3QxQGRvbWFpbmlkLmNvbW11bml0eSIsImV4cCI6MTU1ODk5MjIzNywiYWRkcmVzcyI6eyJyZWdpb24iOiIiLCJjb3VudHJ5IjoiTmV2ZXJsYW5kIiwic3RyZWV0X2FkZHJlc3MiOiIiLCJmb3JtYXR0ZWQiOiIiLCJwb3N0YWxfY29kZSI6IiIsImxvY2FsaXR5IjoiIn0sIm5iZiI6MTU1ODk5MTkzNywic3ViIjoiN2tuNkVOeWFMU1QrSUtwcXRoWFNKcTIxN3F5TVU0eThQd0NTZU5zeFArOUw1eGtjTk1LRUhTNERzRS93K25YUSIsIm5hbWUiOiJUZXN0IFVzZXIgMSIsImJpcnRoZGF0ZSI6IjIwMTktMDMtMTEiLCJhdWQiOiJicHdsZnR6NTJkdzR1IiwiaWF0IjoxNTU4OTkxOTM3LCJ1cGRhdGVkX2F0IjoxNTUzNDQ2NTEyLCJpc3MiOiJodHRwczovL2lkZW50aXR5YWdlbnQuZGUiLCJpZDRtZS5pZGVudGlmaWVyIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkifQ.Pl4-gLFqkTeJUZe3hR8CSm2Et-OWTYKYJy4416z1U482DTWCN9QAXbRt1zB92I5bm2R_pDwOe73_cb69lsuG14JfazHXJvutQ_j8mlUngyJ4Cgye_iploy1SfeBYr31D8WUugamlRfe0Q-dx7cC68P58FFQ8TItlnMP8ALuN-R_-cBUns09Ld88QeupEUoWaYqTKw_m1w67kP858IXuCRvRfGGymKWOACWDn-yBgLFzyXA3-vmc9-BtzDlJeJ75gPltoYqlEC0RhHX3AfTtZwT7hmAR7LNocESCY4fv5KWJluudVKogEYTmeZjM9s_-1K703Q1DNsqYJqRE2xlczjA'.encode(), 200, 'application/json+jwt'

            res = dict()
            client._get_distributed_claims(ctx, self._denic_registration, 'https://identityagent.de/userinfo', 'code', res,
                                           leeway=datetime.timedelta(minutes=5), max_recoursion=0)
            assert res == {
                "id4me.identity": "idtest1.domainid.community",
                "email": "idtest1@domainid.community",
                "exp": 1558992237,
                "address": {
                    "region": "",
                    "country": "Neverland",
                    "street_address": "",
                    "formatted": "",
                    "postal_code": "",
                    "locality": ""
                },
                "nbf": 1558991937,
                "sub": "7kn6ENyaLST+IKpqthXSJq217qyMU4y8PwCSeNsxP+9L5xkcNMKEHS4DsE/w+nXQ",
                "name": "Test User 1",
                "birthdate": "2019-03-11",
                "aud": "bpwlftz52dw4u",
                "iat": 1558991937,
                "updated_at": 1553446512,
                "iss": "https://identityagent.de",
                "id4me.identifier": "idtest1.domainid.community"
            }


    @mock.patch('time.time')
    def test__get_distributed_claims__error_cases(self, time_mock):
        time_mock.return_value = 1558991940

        auth_store = {'id.test.denic.de': json.dumps(self._denic_registration)}

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            generate_private_keys=False)

        client._get_identity_authority = MagicMock(return_value='id.test.denic.de')
        client._get_openid_configuration = MagicMock(side_effect=lambda x: self._openid_configs[x])
        client._get_public_keys_set = MagicMock(side_effect=lambda x: self._entity_keys[x])

        ctx = mock.create_autospec(ID4meContext)
        ctx.id = 'idtest1.domainid.community'
        ctx.issuer = 'https://id.test.denic.de'
        ctx.iau = 'id.test.denic.de'
        ctx.nonce = '996f37f0-41ac-439f-a147-961656e43697'
        ctx.sub = '7kn6ENyaLST+IKpqthXSJq217qyMU4y8PwCSeNsxP+9L5xkcNMKEHS4DsE/w+nXQ'
        ctx.iss = 'https://id.test.denic.de'
        ctx.access_token = 'eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI3a242RU55YUxTVCtJS3BxdGhYU0pxMjE3cXlNVTR5OFB3Q1NlTnN4UCs5TDV4a2NOTUtFSFM0RHNFXC93K25YUSIsImlkNG1lLmlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkZW50aWZpZXIiOiJpZHRlc3QxLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lIjoiaWR0ZXN0MS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiYWRkcmVzcyIsImJpcnRoZGF0ZSIsIm5hbWUiLCJlbWFpbCJdLCJzY29wZSI6WyJvcGVuaWQiXSwiaXNzIjoiaHR0cHM6XC9cL2lkLnRlc3QuZGVuaWMuZGUiLCJleHAiOjE1NTg5OTIyOTUsImlhdCI6MTU1ODk5MTY5NSwianRpIjoiS1ROX29RWVd2V0UiLCJjbGllbnRfaWQiOiJicHdsZnR6NTJkdzR1In0.itdKbW8BJmQU7fEEWxW3kgE5IHQeb_7PUcQhp-PILgqWD5xnNL81ZKjU8Qfzjs5XVNJYTbBShAqAmiQkukCVV0kmWyYWLL5wYbL7Vst84h9oog0Gdlt-RXDEGDgIeKQ4uVMvykoYdbj06rvf7u7h4CUsnmpuJUOG30d5Zq0mX4FaDU01ZtxQMhUHpidm07TTodbKoAk2BADqjDZprc_Dhl00bOaR82i8YKZZYT0x_jhuMAPc9pXIS-KfqIMWdzmyKwuIUiVe_18GV1a0q9cZTiu8x_xsgo3UGuhGDJ6BVL4JAeqMYft3W0QJo2B1-nBw1uAO1f6SMvwB_gqKXc6RIg'

        # case: error fetching data
        with mock.patch('id4me_rp_client.network._http_request_raw') as http_request_mock:
            http_request_mock.return_value = 'dist errror'.encode(), 500

            res = dict()
            try:
                client._get_distributed_claims(ctx, self._denic_registration, 'https://identityagent.de/userinfo', 'code', res,
                                               leeway=datetime.timedelta(minutes=5), max_recoursion=0)
                assert False, "No expected exception: case: error fetching data"
            except ID4meUserInfoRequestException:
                pass