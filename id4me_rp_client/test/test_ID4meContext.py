__author__ = "Pawel Kowalik"
__copyright__ = "Copyright 2018, 1&1 IONOS SE"
__credits__ = []
__license__ = "MIT"
__maintainer__ = "Pawel Kowalik"
__email__ = "pawel-kow@users.noreply.github.com"
__status__ = "Beta"

from unittest2 import TestCase

from id4me_rp_client import *


class TestID4meContext(TestCase):

    def test_serialize_context(self):
        ctx = ID4meContext(id4me='id200.connect.domains',
                           identity_authority='id.denic.de',
                           issuer='https://id.denic.de')
        ctx.access_token = 'ACTOKEN-ABCDEFGHIJK'
        ctx.refresh_token = 'RTOKEN--ABCDEFGHIJK'
        ctx.iss = 'https://id.denic.de'
        ctx.sub = 'SUB-ABCDEFGHIJK'
        ctx.nonce = 'NONCE-ABCDEFGHIJK'

        serialized = ctx.to_json()
        assert len(serialized) > 0, 'context not serialized'
        assert serialized == '{"id": "id200.connect.domains", "iau": "id.denic.de", "issuer": "https://id.denic.de", "access_token": "ACTOKEN-ABCDEFGHIJK", "refresh_token": "RTOKEN--ABCDEFGHIJK", "iss": "https://id.denic.de", "sub": "SUB-ABCDEFGHIJK", "nonce": "NONCE-ABCDEFGHIJK"}', 'context invalid'

        ctxdes = ID4meContext.from_json(serialized)

        assert ctxdes is not None, 'Context was not deserialized'
        assert ctxdes.id == ctx.id, 'Context is not the same after deserialization (id)'
        assert ctxdes.iau == ctx.iau, 'Context is not the same after deserialization (iau)'
        assert ctxdes.issuer == ctx.issuer, 'Context is not the same after deserialization (issuer)'
        assert ctxdes.access_token == ctx.access_token, 'Context is not the same after deserialization (access_token)'
        assert ctxdes.refresh_token == ctx.refresh_token, 'Context is not the same after deserialization (refresh_token)'
        assert ctxdes.iss == ctx.iss, 'Context is not the same after deserialization (iss)'
        assert ctxdes.sub == ctx.sub, 'Context is not the same after deserialization (sub)'
        assert ctxdes.nonce == ctx.nonce, 'Context is not the same after deserialization (nonce)'
