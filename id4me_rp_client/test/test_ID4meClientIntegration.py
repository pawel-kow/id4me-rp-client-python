__author__ = "Pawel Kowalik"
__copyright__ = "Copyright 2018, 1&1 IONOS SE"
__credits__ = []
__license__ = "MIT"
__maintainer__ = "Pawel Kowalik"
__email__ = "pawel-kow@users.noreply.github.com"
__status__ = "Beta"

import json
import random
import webbrowser

import unittest2
from test_ID4meClientTestCommon import TestID4meClientTestCommon
from unittest2 import TestCase

from id4me_rp_client import *
from id4me_rp_client.test.test_http_server import OneCallTestHttpServer


@unittest2.skip("Integration tests")
class TestID4meClientTestIntegration(TestCase):
    testloginconfigs = [
        # test without encryption and identityagent.de // Pass: WU9ZOZGC0DgzS9blLECd
        {'requireencryption': False, 'domain': 'idtest1.domainid.community'},
        # test with optional encryption and identityagent.de // Pass: WU9ZOZGC0DgzS9blLECd
        {'requireencryption': None, 'domain': 'idtest1.domainid.community'},
        # test with MojeID (encryption disabled due to https://github.com/OpenIDC/pyoidc/issues/607 // Pass: R9NSXL9JtWLKYmEH1uKr
        {'requireencryption': False, 'domain': 'doe-john.mojeid.cz', 'nodistributedclaims': True},
        # test with agent: beta.id4me.ionos.com // Pass: WU9ZOZGC0DgzS9blLECd
        {'requireencryption': None, 'domain': 'testionos.domainid.community'},
        # test with authority mailbox.org // Pass: WU9ZOZGC0DgzS9blLECd // turned off due to lack of OIDC configuration
        #{'requireencryption': False, 'domain': 'doe.jane@mailbox.org', 'nodistributedclaims': True,
        # 'noclaimsparameter': True, },
        # test with Kopano // User: user1, Pass: user1
        # {'requireencryption': False, 'domain': 'eisenmann.io', 'nodistributedclaims': True, 'noaddress': True,
        # 'nobirthday': True,},

        # TEMP: uncomment this test one encryption is present in all cases
        # {'requireencryption': True, 'domain': 'testid.connect.domains'},
        #        { 'requireencryption': True}
    ]

    def test_get_claims(self):
        for i in self.testloginconfigs:
            with self.subTest(i=i):
                self._test_get_claims(i)

    def _test_get_claims(self, params):
        test_id = params['domain'] if 'domain' in params else 'id200.connect.domains'
        test_state = str(random.randint(1000000, 10000000))

        auth_store = dict()

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval),
            params=params,
            generate_private_keys='requireencryption' not in params
                                  or params['requireencryption'] is None
                                  or params['requireencryption'] == True)

        ctx = client.get_rp_context(id4me=test_id)

        link = client.get_consent_url(
            ctx, state=test_state,
            prompt=OIDCPrompt.login,
            usenonce=True,
            claimsrequest=ID4meClaimsRequest(
                id_token_claims={
                    OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='Test reason'),
                    OIDCClaim.email_verified: None,
                },
                userinfo_claims={
                    OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='Test other confusing reason'),
                    OIDCClaim.address: ID4meClaimRequestProperties(essential=True, reason='Yet another reason'),
                    OIDCClaim.name: ID4meClaimRequestProperties(essential=True, reason='Yet another reason'),
                    OIDCClaim.birthdate: ID4meClaimRequestProperties(essential=True)
                }) if 'noclaimsparameter' not in params or not params['noclaimsparameter'] else None,
            scopes=[OIDCScope.email, OIDCScope.address, OIDCScope.profile] if 'noclaimsparameter' in params and params[
                'noclaimsparameter'] else None
        )
        assert link is not None, "Login URL is empty"

        webbrowser.open(link, autoraise=True)

        testserv = OneCallTestHttpServer(path_to_handle='/validate', serverhost=TestID4meClientTestCommon.test_host,
                                         serverport=TestID4meClientTestCommon.test_port)
        testserv.start_local_http_and_handle_one_request()

        assert testserv.parsed_path.path == '/validate', 'Wrong end-point called'
        assert testserv.pased_params is not None, 'No params passed back'
        assert testserv.pased_params['code'] is not None, 'Code not passed back'
        assert testserv.pased_params['code'][0] is not None, 'Code not passed back'
        assert testserv.pased_params['state'] is not None, 'State not passed back'
        assert testserv.pased_params['state'][0] is not None, 'State not passed back'
        assert testserv.pased_params['state'][0] == test_state, \
            'Different test_state coming back {} {}'.format(testserv.pased_params['state'][0], test_state)

        id_token = client.get_idtoken(ctx, code=testserv.pased_params['code'][0])
        assert id_token is not None, 'ID token empty'
        print('** ID token: {}'.format(id_token))
        assert 'iss' in id_token, 'No iss in id token'
        assert 'sub' in id_token, 'No sub in id token'

        user_info = client.get_user_info(ctx)
        assert user_info is not None, 'User info token empty'
        print('** User info: {}'.format(user_info))
        assert 'iss' in user_info, 'No iss in user info'
        assert 'sub' in user_info, 'No sub in user info'
        assert str(OIDCClaim.email) in user_info, 'No email in user info'
        if 'noaddress' not in params or not params['noaddress']:
            assert str(OIDCClaim.address) in user_info, 'No address in user info'
        if 'noclaimsparameter' not in params or not params['noclaimsparameter']:
            if 'nobirthday' not in params or not params['nobirthday']:
                assert str(OIDCClaim.birthdate) in user_info, 'No birthdate in user info'
        assert str(OIDCClaim.name) in user_info, 'No name in user info'

        # testing without distributed claims (skip IDs where Agent = Authority
        if 'nodistributedclaims' not in params or not params['nodistributedclaims']:
            user_info = client.get_user_info(ctx, 0)
            assert user_info is not None, 'User info token empty'
            print('** User info: {}'.format(user_info))
            assert 'iss' in user_info, 'No iss in user info'
            assert 'sub' in user_info, 'No sub in user info'
            assert str(OIDCClaim.email) not in user_info, 'Agent claim not expected in user info'
            assert str(OIDCClaim.address) not in user_info, 'Agent claim not expected in user info'
            assert str(OIDCClaim.birthdate) not in user_info, 'Agent claim not expected in user info'
            assert str(OIDCClaim.name) not in user_info, 'Agent claim not expected in user info'

    def test_get_custom_claims(self):
        test_id = 'idtest1.domainid.community'  # Pass: WU9ZOZGC0DgzS9blLECd
        test_state = str(random.randint(1000000, 10000000))

        auth_store = dict()

        client = TestID4meClientTestCommon._get_test_client(
            get_client_registration=lambda authname: auth_store[authname],
            save_client_registration=lambda authname, authval: auth_store.__setitem__(authname, authval))

        ctx = client.get_rp_context(id4me=test_id)

        link = client.get_consent_url(
            ctx, state=test_state,
            prompt=OIDCPrompt.login,
            usenonce=True,
            claimsrequest=ID4meClaimsRequest(
                userinfo_claims={
                    OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='Test other confusing reason'),
                    'id4me.custom': ID4meClaimRequestProperties(essential=True, reason='Custom claim reason'),
                    'id4me.nonexisting': ID4meClaimRequestProperties(essential=True, reason='Custom claim2 reason')
                })
        )
        assert link is not None, "Login URL is empty"

        webbrowser.open(link, autoraise=True)

        testserv = OneCallTestHttpServer(path_to_handle='/validate', serverhost=TestID4meClientTestCommon.test_host,
                                         serverport=TestID4meClientTestCommon.test_port)
        testserv.start_local_http_and_handle_one_request()

        assert testserv.parsed_path.path == '/validate', 'Wrong end-point called'
        assert testserv.pased_params is not None, 'No params passed back'
        assert testserv.pased_params['code'] is not None, 'Code not passed back'
        assert testserv.pased_params['code'][0] is not None, 'Code not passed back'
        assert testserv.pased_params['state'] is not None, 'State not passed back'
        assert testserv.pased_params['state'][0] is not None, 'State not passed back'
        assert testserv.pased_params['state'][0] == test_state, \
            'Different test_state coming back {} {}'.format(testserv.pased_params['state'][0], test_state)

        id_token = client.get_idtoken(ctx, code=testserv.pased_params['code'][0])
        assert id_token is not None, 'ID token empty'
        print('** ID token: {}'.format(id_token))
        assert 'iss' in id_token, 'No iss in id token'
        assert 'sub' in id_token, 'No sub in id token'

        user_info = client.get_user_info(ctx)
        assert user_info is not None, 'User info token empty'
        print('** User info: {}'.format(user_info))
        assert 'iss' in user_info, 'No iss in user info'
        assert 'sub' in user_info, 'No sub in user info'
        assert str(OIDCClaim.email) in user_info, 'No email in user info'
        assert str('id4me.custom') in user_info, 'No custom claim in user info'
        assert user_info['id4me.custom'] == 'Custom test', 'Custom claim with wring value'
        assert str('id4me.nonexisting') not in user_info, 'Not existing custom claim in user info'

    testconfigs_reuse_auth_registration = [
        # this one works with Denic auth, one needs to log-in first and select "remember me"
        {'domain': 'idtest1.domainid.community'},
        # TEMP: this one will fail due to lack of new auth if same browser used, to pass prompt=login would be required
        # {'domain': 'id100.connect.domains'},
    ]

    def test_reuse_auth_registration(self):
        for i in self.testconfigs_reuse_auth_registration:
            with self.subTest(i=i):
                self._test_reuse_auth_registration(i)

    def _test_reuse_auth_registration(self, params):
        test_id = params['domain'] if 'domain' in params else 'id200.connect.domains'
        test_state = str(random.randint(1000000, 10000000))

        # noinspection PyPep8
        priv_key = '{"keys":[{"d":"LiY9nb5v4NW8-iBtU3Z_Bx-PNFzX57qeaEqh2AZuuvFLZpBNvKhELJ0_Re308LweyCYVJHaP6MUrVs9i2PwGTtNxxuZ835K09T_9wc5OcNIjzI5WxLc8j1Wwd65v1QlndmXh4MS9rPjPC3us4modamFcjf5I0Lg9o-0miCt77SYROl_ZuHCX8QVCVrySU4UkU9xpZONpkDn25CpwnrftRhO1hP60HIiXWA6vQvYYHpRX9f5wuqIshcoMzXccSHf_TMY9UEAsxpvIqO98eRCxWNF75RTBynddqoSWVBnP_JwCUWOG3ZtwS7EGYd5HncNKdramrh0XK-JOzS6xb7UyzQ","dp":"I6knDqeSJTdbYw-OfXM2KmqgUZ-z6QaVqpLDPyNeTfJJTwklH1i9T5i_6t849fOKBOjrYhZ-Poz3t1tVJ7n77_colyr-i7o3irMoVnMI-m8EalwVGV61jJ7BlAruhnvrINNrGauUJ9E_2kkcFaJtlQy3LPN2r8e0oX2qH9NANkk","dq":"DfKB5JouG30jiVnFJljMwMKTJw47o4bSJ5BGqZIxfNjVgtQir2-_Ekzh4F_kVMxFIDhSc-gKBFtDw1SMkbfZUZl8gdhZe9ejvhXuh08NuTKDbdayuWNCv4gGd7kwTIvjjI545uaLsc5fql7-D9Ai6gF40lfzZZp9If0yUve8CAM","e":"AQAB","kid":"9664a935-c49d-4914-963a-110aba5d6202","kty":"RSA","n":"saZoYnYQBKfWQY0Uy0stB3tpRVXCJce0YQhRPPpfYizQhueo-2I_UVrdenZhzyWKFg6LfR2z_pKcDPj5A3TepXXSWo4X8HnOKu3V9E1pplwfzE5NEGADd4-Fvfjs6RZtX8JLIhj3teBVO4LXBryHGOW-s24_DIqdItoZbpCvRwZaHskZG0ruWIX27hSCNQyxygLNRmbWRBYo0sHloOFPV2Yl3aM5N2VOTEfhkJr4346GHOGDmrDgirC3DdjpEqBmk1cQXP2T3J4EmMpZOsn-RiuRsjvBHIyRr9mooKYL2ekAmr8CD5PVeSslntf_F-_w4ATv4Ld2MLsXS7mrl41DBQ","p":"5FJM0z-LeWKNtz7DZsheYKhViieNxgor2JhnsCJZd8L1IPlmwJ8tueyUjG7wbFrVsdfKp4Mo15CDXAMdMBjmvSa6oY1_hJIjiWvRbAbJ0MgMo60nc6Pv4H7N807Lg9AFhVDUaQIL47ldOYvp9cJBnNGWZoGKop-SllDsB4mN_m8","q":"xy-TgNAeqeF8mLPE37KE99-jtcVaMR3wZsfytumwZW-lm8oVgmUcnt_u4157_ni3OidPgdcpItNvGi5L4h9MOmWJI7Hc3aSZnj7ZTFjeQ4OFg7eCmUxENtMXseGK1eRyfzE6e-y7PGtp3eVbz-yD_yED81LwTKSbx6zJD3S0D8s","qi":"tKkmN7wcmG63hAQAoq9oD0KYXjv6V5bnrr5NBHHWtO_fqHpofqhHizYemmuQnvad8I7kg6h1iN_abhxMaE5gtUi-kzdRZy1hx_KU3w5XO2F6h60WKfQEuXtIkPFaGjtLPhzGTDDaisOsOYltKQOIuUkjaO3ZBatzGSi9BLLeqT8"}]}'
        authstore = {
            'id.test.denic.de': '{"grant_types": ["authorization_code"], "jwks": {"keys": [{"kty": "RSA", "e": "AQAB", "kid": "9664a935-c49d-4914-963a-110aba5d6202", "n": "saZoYnYQBKfWQY0Uy0stB3tpRVXCJce0YQhRPPpfYizQhueo-2I_UVrdenZhzyWKFg6LfR2z_pKcDPj5A3TepXXSWo4X8HnOKu3V9E1pplwfzE5NEGADd4-Fvfjs6RZtX8JLIhj3teBVO4LXBryHGOW-s24_DIqdItoZbpCvRwZaHskZG0ruWIX27hSCNQyxygLNRmbWRBYo0sHloOFPV2Yl3aM5N2VOTEfhkJr4346GHOGDmrDgirC3DdjpEqBmk1cQXP2T3J4EmMpZOsn-RiuRsjvBHIyRr9mooKYL2ekAmr8CD5PVeSslntf_F-_w4ATv4Ld2MLsXS7mrl41DBQ"}]}, "subject_type": "public", "application_type": "native", "logo_uri": "http://localhost:8090/logo.png", "userinfo_encrypted_response_enc": "A128CBC-HS256", "registration_client_uri": "https://id.test.denic.de/clients/4eft7lh4oqpxg", "redirect_uris": ["http://localhost:8090/validate"], "userinfo_encrypted_response_alg": "RSA-OAEP-256", "registration_access_token": "RvonMm52sXwuzU7n4w7rUXs11-UnviuOsNoESV5T07o.YSxpLHI", "token_endpoint_auth_method": "client_secret_basic", "userinfo_signed_response_alg": "RS256", "client_id": "4eft7lh4oqpxg", "id_token_encrypted_response_alg": "RSA-OAEP-256", "id_token_encrypted_response_enc": "A128CBC-HS256", "client_secret_expires_at": 0, "tos_uri": "http://localhost:8090/documents", "client_id_issued_at": 1552317743, "client_secret": "IFbeuw-BN6Y5VuK4MzYz02nC9wmHNSFAEcP565URekc", "tls_client_certificate_bound_access_tokens": false, "client_name": "Test", "response_types": ["code"], "policy_uri": "http://localhost:8090/about", "id_token_signed_response_alg": "RS256"}'}
        client = TestID4meClientTestCommon._get_test_client(
            private_jwks_json=priv_key,
            get_client_registration=lambda authname: authstore[authname],
            save_client_registration=lambda authname, authval: authstore.__setitem__(authname, authval))
        ctx = client.get_rp_context(id4me=test_id)
        assert json.loads(authstore['id.test.denic.de'])['client_id'] == '4eft7lh4oqpxg', "Client ID not matching"

        link = client.get_consent_url(
            ctx, state=test_state,
            usenonce=True,
            # prompt=OIDCPrompt.login, # uncomment if Authority returns different ID
            claimsrequest=ID4meClaimsRequest(
                id_token_claims={
                    OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='Test reason'),
                    OIDCClaim.email_verified: None,
                },
                userinfo_claims={
                    OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='Test other confusing reason'),
                    OIDCClaim.address: ID4meClaimRequestProperties(essential=True, reason='Yet another reason'),
                    OIDCClaim.birthdate: ID4meClaimRequestProperties(essential=True)
                })
        )
        assert link is not None, "Login URL is empty"

        webbrowser.open(link, autoraise=True)

        testserv = OneCallTestHttpServer(path_to_handle='/validate', serverhost=TestID4meClientTestCommon.test_host,
                                         serverport=TestID4meClientTestCommon.test_port)
        testserv.start_local_http_and_handle_one_request()

        assert testserv.parsed_path.path == '/validate', 'Wrong end-point called'
        assert testserv.pased_params is not None, 'No params passed back'
        assert testserv.pased_params['code'][0] is not None, 'Code not passed back'
        assert testserv.pased_params['state'][0] == test_state, \
            'Different test_state coming back {} {}'.format(testserv.pased_params['state'][0], test_state)

        try:
            client.get_user_info(ctx)
            assert False, 'No exception when user info called before token obtained'
        except ID4meUserInfoRequestException:
            pass

        client.get_idtoken(ctx, code=testserv.pased_params['code'][0])
        ui = client.get_user_info(ctx)
        assert ui is not None, 'No user info received'
        print(ui)
