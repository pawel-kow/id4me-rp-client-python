## CHANGELOG:
| version | date       | changes |
| ------- | -----------| ------ |
| 0.0.24  | 2019-11-26 | NEW FEATURE: Support "none" as "alg" of id_token<br>NEW FEATURE: allow override of authority lookup<br>NEW FEATURE: userinfo_signing_required now can be configured in client code<br>NEW FEATURE: RS384 and RS512 added as supported signature options<br>REFACTORING: cleaned up the code of dynamic client registration<br>BUGFIX: using 'none' algorithms only if supported |
| 0.0.23  | 2019-10-04 | NEW FEATURE: plain JSON user info support added<br>NEW FEATURE: use of scope parameter instead of claims if not supported by IdP<br>NEW: example code included |
| 0.0.22  | 2019-07-29 | BUGFIX: id4me_rp_client.helper not exported to the release library |
| 0.0.21  | 2019-07-29 | BUGFIX: YXDOMAIN case not properly handled<br>BUGFIX: avoid trying to resolve empty domain names<br>BUGFIX: added better handling when state is empty<br>LOGGING: added logging of all exceptions (debug level) |
| 0.0.20  | 2019-05-23 | NEW FEATURE: E-mail address hashing as per spec change proposed in https://gitlab.com/ID4me/documentation/merge_requests/7<br>MINOR CHANGE: finally deprecated `preferred_client_id` from registration<br>NEW FEATURE: timeout configurable via NetworkConfig |
| 0.0.19  | 2019-03-24 | TEST: added Kopano to the integration test<br>BUGFIX: leeway to re-register set to 5 minutes istead of 2 hours<br>TEST: added password to mojeid test account |
| 0.0.18  | 2019-03-23 | NEW FEATURE: added support for E-mail like identifiers (just replace @ with .)<br>NEW FEATURE: requesting claims with scope<br>WORKAROUND: accepting token_type as 'Bearer' and 'bearer'<br>BUGFIX: 'tos_uri' assigned properly |
| 0.0.17  | 2019-03-19 | SECURITY FIX: Limited timeouts and size of downloaded data (DOS prevention)<br>SECURITY FIX: Limited recoursion level of distributed claims (DOS prevention) |
| 0.0.16  | 2019-03-11 | MAJOR CHANGE: removed back-compatibility with old _openid record format |
| 0.0.15  | 2019-02-27 | - NEW FEATURE: Automatically re-register expired client registration <br> - explicit parameter to enable/block automatic client registration |
| 0.0.14  | 2019-02-25 | No functional changes. Example code in README fixed |
| 0.0.13  | 2019-02-25 | No functional changes. TEST & EXAMPLE for custom claims added |
| 0.0.12  | 2019-02-21 | BUGFIX: Exception when no encryption used but private key missing |
| 0.0.11  | 2019-02-21 | BUGFIX, error when serializing ID4meContext |
| 0.0.10  | 2019-02-18 | API BREAKING CHANGE: client configuration loading callback moved to client object in order to remove secret data from the ID4meContext which can be in some frameworks sent over cookies |
